<!DOCTYPE html>

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

</head>
<body>
<table style="border-collapse: separate;" width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#eeeeee" align="center">
    <tbody>
    <tr>
        <td style="background: url(http://clever-e.com/web/design/img2/promo-img.png) repeat 0 0;padding-top:0;padding-right:0;padding-bottom:80px;padding-left:0;margin:0;" width="100%" bgcolor="#eeeeee">
            <table style="border-top-left-radius:10px;border-top-right-radius:10px;" width="565" cellspacing="0" cellpadding="15" border="0" align="center">
                <tbody>
                <tr>
                    <td style="padding-top:30px;padding-right:15px;padding-bottom:30px;padding-left:15px;color: #ffffff;" width="100%" align="center">
                        <a href="http://clever-e.com/" target="_blank" rel="noopener">
                            <img alt="Clever-e" src="http://clever-e.com/web/design/img2/toplogo.png" height="27" width="198" border="0">
                        </a>
                    </td>
                </tr>
                </tbody>
            </table>
            <table style="box-shadow:0 1px 1px 0 rgba(0, 0, 0, 0.1);border: 1px solid #cccccc;border-top-left-radius:10px;border-top-right-radius:10px;border-bottom-left-radius:10px;border-bottom-right-radius:10px;" width="565" cellspacing="0" cellpadding="15" border="0" align="center">
                <tbody>
                <tr>
                    <td style="padding-top:34px;padding-left:31px;padding-right:31px;margin:0;border-top-left-radius:10px;border-top-right-radius:10px;" bgcolor="#ffffff" align="left">
                        <span style="color:#00afec;font-weight:normal;font-size:21px;font-family:Arial,Helvetica,sans-serif;">Почти готово...</span>
                    </td>
                </tr>
                <td style="padding-top:5px;padding-left:31px;padding-right:31px;padding-bottom:20px;" bgcolor="#ffffff" align="left">
                    <span style="color:#262626;font-weight:normal;font-size:15px;font-family:Arial,Helvetica,sans-serif;line-height:22px;">Нажмите на кнопку ниже, чтобы активировать вашу учетную запись.</span>
                </td>
                <tr>
                    <td width="100%" bgcolor="#ffffff" align="center">
                        <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                            <tbody>
                            <tr>
                                <td align="center">
                                    <table style="border-top-left-radius:4px;border-top-right-radius:4px;border-bottom-left-radius:4px;border-bottom-right-radius:4px;" width="62%" cellspacing="0" cellpadding="0" border="0" bgcolor="#00a0d9" align="center">
                                        <tbody>
                                        <tr>
                                            <td style="padding:0;border-top-left-radius:4px;border-top-right-radius:4px;border-bottom-left-radius:4px;border-bottom-right-radius:4px;border-color:transparent;border-color:#1790da;border-style:solid;border-top-width:1px;border-right-width:1px;border-bottom-width:1px;border-left-width:1px;text-shadow: 0px -1px 0 rgba(0, 0, 0, 0.2);text-align: center;background: #00a0d9;background: -moz-linear-gradient(top, #00adea 0%, #00a0d9 100%);background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#00adea), color-stop(100%,#00a0d9));background: -o-linear-gradient(top, #00adea 0%,#00a0d9 100%);" height="55" align="center">
                                                <a href="{{url('/verifyemail/'.$confirmation_code)}}" style="display:block;height:55px;line-height:55px;color:#ffffff;font-weight:normal;font-size:24px;font-family:Arial,Helvetica,sans-serif;text-decoration:none;" target="_blank" rel="noopener">
                                                    Активировать
                                                    <span style="font-size:16px;font-family:Arial,Helvetica,sans-serif;text-decoration:none;"></span>
                                                </a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="padding-top:5px;padding-left:32px;padding-right:50px;padding-bottom:30px;" bgcolor="#ffffff" align="center">
                                <span style="color:#a4a4a4;font-weight:normal;font-size:12px;font-family:Helvetica,sans-serif;line-height:19px;">
                                     Нажав на кнопку „Активировать”, я соглашаюсь с условиями и положениями
                                        <a href="" style="color: #a4a4a4;" target="_blank" rel="noopener">Пользовательского соглашения</a>
                                        ,
                                        <a href="" style="color: #a4a4a4;" target="_blank" rel="noopener" data-snippet-id="3b41f2a5-5217-fca7-ec45-19eef973c4a9">Политики против спама</a>
                                        , и
                                        <a href="" style="color: #a4a4a4;" target="_blank" rel="noopener">Политики конфиденциальности</a>
                                        .
                                        </span>
                    </td>
                </tr>
                <tr>
                    <td style="border-bottom-left-radius:10px;border-bottom-right-radius:10px;padding:4px;" bgcolor="#ffffff" align="center">
                        <table style="width:100%;" cellspacing="0" cellpadding="0" border="0" align="center">
                            <tbody>
                            <tr>
                                <td style="background:#ffffff;padding:0;margin:0;" height="1" align="center"></td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
</body>
</html>
