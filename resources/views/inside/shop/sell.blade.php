@extends('inside.index')

@section('title', 'Продажа курса ')

@section('modal')
@endsection


@section('content')


    <div class="container">
        <div class="row margin-row">
            <div class="col-md-12  col-sm-12 col-xs-12 ">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 user-name">
                        Продажа курса "{{$book->name}}"
                    </div>
                </div>
            </div>
            <form id ="create-form" role="form" method="POST" action="/shop/sell/id{{$book->id}}">
                {{ csrf_field() }}
                <div class="col-md-12  col-sm-12 col-xs-12 ">

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                Цена курса:
                                <div class="input-group">
                                    @if ($book->price > 0)
                                        <input id="course_price" class="form-control" placeholder="Цена" name="price" type="number" min="0"  value="{{$book->price}}">
                                        <span class="input-group-addon">
                                        <input class="free" aria-label="..." name="free"  type="checkbox">
                                    Бесплатный
                                    </span>
                                    @else
                                        <input id="course_price" class="form-control" placeholder="Цена" name="price" type="number" min="0" disabled>
                                        <span class="input-group-addon">
                                        <input class="free" aria-label="..." name="free"  type="checkbox" checked>
                                    Бесплатный
                                    </span>
                                    @endif
                                </div>
                                <div class="help-block hidden help-error">

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 add-marginb-30">
                            <button id="cancel_course" action="/id{{Auth::id()}}" type="button" class="btn-default btn btn-left" ><b>Отмена</b></button>
                            <button id="add_course" type="submit" class="btn btn-success btn-right"><b>Выставить на продажу</b></button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection







