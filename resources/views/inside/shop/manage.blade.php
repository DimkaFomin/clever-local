@extends('inside.index')

@section('title', 'Продажа курсов')

@section('modal')
    <!-- send xhr data -->
    <div class="modal fade" id="xhrConfirmation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myModalLabel">Отправка данных на доступ к курсу</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-2 col-sm-2 col-xs-2">Пользователь: </div><div class="col-md-10 col-sm-10 col-xs-10"><input id="user" data-id="" class="col-md-12 col-sm-12 col-xs-12" type="text" disabled value=""></div>
                        <div class="col-md-12 col-sm-12 col-xs-12"></div>
                        <div class="col-md-2 col-sm-2 col-xs-2">Курс: </div><div class="col-md-10 col-sm-10 col-xs-10"><input id="course" data-id="" class="col-md-12 col-sm-12 col-xs-12" type="text" disabled value=""></div>
                        <div class="col-md-12 col-sm-12 col-xs-12"><hr></div>
                        <div class="col-md-12 col-sm-12 col-xs-12"><h4>Данные на вход</h4></div>
                        <div class="col-md-2 col-sm-2 col-xs-2">Логин: </div><div class="col-md-10 col-sm-10 col-xs-10"><input id="login" data-login="" class="col-md-12 col-sm-12 col-xs-12" type="text" value=""></div>
                        <div class="col-md-12 col-sm-12 col-xs-12"></div>
                        <div class="col-md-2 col-sm-2 col-xs-2">Пароль: </div><div class="col-md-10 col-sm-10 col-xs-10"><input id="password" data-password="" class="col-md-12 col-sm-12 col-xs-12" type="text" value=""></div>
                        <div class="col-md-12 col-sm-12 col-xs-12"></div>
                        <div class="col-md-2 col-sm-2 col-xs-2">Инструкция: </div><div class="col-md-10 col-sm-10 col-xs-10"><textarea id="comment" data-id="" class="col-md-12 col-sm-12 col-xs-12" type="text"></textarea></div>
                        <div class="error hidden"><div class="col-md-2 col-sm-2 col-xs-2"></div><div class="col-md-10 col-sm-10 col-xs-10" style="color: red">Введите <b>логин и пароль</b> и/или <b>инструкцию</b></div></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-right" id="xhrNo" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove-circle"></span> Отмена
                    </button>
                    <button type="button" class="btn btn-primary btn-left" id="xhrOk">
                        <span class="glyphicon glyphicon-envelope"></span> Отправить
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- end send xhr data -->
@endsection


@section('content')
    <div class="container">
        <div class="row margin-row">
            <div class="col-md-12  col-sm-12 col-xs-12 ">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 user-name">
                    Продажа курсов
                    </div>
                </div>
            </div>
         @foreach($courses as $c)
            <div class="row margin-row">
                <div class="col-md-12  col-sm-12 col-xs-12 block course-head-container ">
                    <div class="row">

                        <div class="col-md-12 col-sm-12 col-xs-12 ">
                            Продажи курсов на сторонем ресурсе
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12 ">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th class="col-md-6 col-xs-6">Курс</th>
                                    <th class="col-md-4 col-xs-4">Покупатель</th>
                                    <th class="col-md-2 col-xs-2"></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($courses as $course)
                                    @if($course->students->count() > 0 )
                                        @foreach($course->students as $student)

                                            @if($student->pay == 1 && $student->xhr == 1 && $student->xhr_check != 1)
                                                <tr>
                                                    <td  class="col-md-6 col-xs-6 editor-container"><a href="/constructor/id{{$course->id}}">{{$course->name}}</a></td>
                                                    <td  class="col-md-4 col-xs-4">{{$student->user->first}} {{$student->user->second}}</td>
                                                    <td  class="col-md-2 col-xs-2">
                                                        @if($student->xhr_check == 0)
                                                        <button class="btn btn-success sendXhrClass"  id="sendXhr" data-toggle="modal" data-target="#xhrConfirmation"
                                                                                           data-user_id="{{$student->user->id}}"
                                                                                           data-user_name="{{$student->user->first}} {{$student->user->second}}"
                                                                                           data-course_id="{{$course->id}}"
                                                                                           data-course_name="{{$course->name}}"
                                                                                           data-link="/shop/sendxhr">Отправить данные</button> </td>
                                                        @endif
                                                        @if($student->xhr_check == 2)
                                                            <button class="btn btn-danger sendXhrClass"  id="sendXhr" data-toggle="modal" data-target="#xhrConfirmation"
                                                                    data-user_id="{{$student->user->id}}"
                                                                    data-user_name="{{$student->user->first}} {{$student->user->second}}"
                                                                    data-course_id="{{$course->id}}"
                                                                    data-course_name="{{$course->name}}"
                                                                    data-link="/shop/sendxhr">Отправить повторно</button> </td>
                                                        @endif
                                                        @if($student->xhr_check == 3)
                                                            <button class="btn btn-default "  disabled >Ожидает проверки</button> </td>
                                                        @endif
                                                </tr>
                                            @endif
                                        @endforeach
                                    @endif
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
                @break
        @endforeach
        </div>
        <div class="row margin-row">
            @foreach($courses as $course)
                @if($course->is_moderated == 2)
                <div class="col-md-12  col-sm-12 col-xs-12 block course-head-container ">
                    <div class="row">

                        <div class="col-md-12 col-sm-12 col-xs-12 ">
                            Курсы, ожидающие проверки
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12 ">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th class="col-md-6 col-xs-6">Курс</th>
                                    <th class="col-md-2 col-xs-2">Стоимость</th>
                                    <th class="col-md-2 col-xs-2"></th>
                                    <th class="col-md-2 col-xs-2"></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($courses as $course)
                                    @if($course->is_moderated == 2)
                                    <tr>
                                        <td  class="col-md-6 col-xs-6 editor-container"><a href="/constructor/id{{$course->id}}">{{$course->name}}</a></td>
                                        <td  class="col-md-2 col-xs-2">{{$course->price}}</td>
                                        <td  class="col-md-2 col-xs-2"></td>
                                        <td  class="col-md-2 col-xs-2"><button class="btn btn-danger remove-moderate">Отменить заявку</button> </td>
                                    </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
                    @break
                @endif
            @endforeach
        </div>
        <div class="row margin-row">
            <div class="col-md-12  col-sm-12 col-xs-12 block course-head-container ">
                <div class="row">

                    <div class="col-md-12 col-sm-12 col-xs-12 ">
                        Курсы, выставленные на продажу
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 ">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th class="col-md-6 col-xs-6">Курс</th>
                                <th class="col-md-2 col-xs-2">Стоимость</th>
                                <th class="col-md-2 col-xs-2">Продаж</th>
                                <th class="col-md-2 col-xs-2"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($courses as $course)
                                @if($course ->is_moderated == 1)
                                    <tr>
                                        <td  class="col-md-6 col-xs-6 editor-container"><a href="/constructor/id{{$course->id}}">{{$course->name}}</a></td>
                                        <td  class="col-md-2 col-xs-2">{{$course->price}}</td>
                                        <td  class="col-md-2 col-xs-2"> {{ $course->students->count()}}</td>
                                        <td  class="col-md-2 col-xs-2"><button class="btn btn-danger remove-sell">Снять с продажи</button> </td>
                                    </tr>
                                @endif
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

@endsection
