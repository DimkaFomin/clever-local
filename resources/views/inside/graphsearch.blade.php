@extends('inside.index')


@section('content')


    {{--<div class="container" style = "padding: 0 15px 0 15px;" name ="top">--}}
        {{--<div>{{$user_info->login}}</div>--}}
        {{--<div>Ваша реферальная сслыка: http://clever-e.com/refer{{$md5}}</div>--}}

    {{--</div>--}}
    {{--{{ asset('js/home/index.js') }}--}}

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>
    <script src="{{ asset('js/arbor-v0.92/lib/arbor.js') }}"></script>
    <script src="{{ asset('js/arbor-v0.92/demos/halfviz/src/renderer.js') }}"></script>
    <script src="{{ asset('js/arbor-v0.92/demos/_/graphics.js') }}"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.pack.js"></script>



    {{--<script src="{{ asset('js/arbor-v0.92/lib/arbor.js') }}"></script>--}}


        <div id="parent" class="container" style = "padding: 0 15px 0 15px;" name ="top">
            <br>

            <style type="text/css">
                /*graphical search*/

                .search-div{
                    height: 600px;
                    width: 100%;
                }
                .min{
                    width: 100%;
                }


                /*end graphical search*/

                @media (max-width: 360px)  {
                    .search-div{
                        height: 600px;
                        width: 600px;

                    }
                    .min{
                        width: 600px;
                    }

                }
            </style>

            <div id="parent-search" class="search-div">
                <canvas id="viewport" style="border: 1px #26b37c solid;"></canvas>
            </div>

            <script>
                var canvas = document.getElementById("viewport");
                var parent = document.getElementById("parent-search");
                canvas.style.width = '100%';
                canvas.style.height = '600px';
                canvas.width = parent.offsetWidth;
                canvas.height = parent.offsetHeight;
            </script>


                    Найдено {{$count}} элемента(-ов)
                @foreach($courses as $course)
                    <div class="panel panel-default min">
                        <div class="panel-heading" role="tab" id="heading_{{$loop->index}}" onclick="">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_{{$loop->index}}" aria-expanded="true" aria-controls="collapse_{{$loop->index}}">

                                    <i class="more-less glyphicon glyphicon-plus"></i>
                                    {{$course->name}}
                                </a>
                            </h4>

                        </div>

                        <div id="collapse_{{$loop->index}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_{{$loop->index}}">
                            <div class="panel-body">
                                <div style = "max-height: 480px;">
                                    <div class = "col-md-2"></div>
                                    <div class = "col-md-8" class = "material_block" data-desciption_id = "{{$loop->index}}">
                                        {!! $course->description !!}
                                        <br>
                                        @if($course->source == 'other')
                                            <a class="btn btn-info pull-right" href="/other_course/card/id{{$course->id}}" data-id="{{$course->id}}"><i class="icon-shopping-cart"></i>Изучать</a>
                                        @else
                                            <a class="btn btn-info pull-right" href="/course/card/id{{$course->id}}" data-id="{{$course->id}}"><i class="icon-shopping-cart"></i>Изучать</a>
                                        @endif

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                @endforeach


            @if($show_button == 1)
                <div class="min" style="padding-bottom: 50px;">
                    <a href="/search/se_{{$element_id}}/page_{{$next_page}}"><button class="btn btn-default show-more-courses" style="width: 100%">Показать ещё</button></a>
                </div>

            @endif





                <script>

                    (function ($) {
                        var Renderer = function (canvas) {

                            var canvas = $(canvas).get(0);
                            var ctx = canvas.getContext("2d");
                            var particleSystem;
                            var that = {
                                init: function (system) {
                                    particleSystem = system;
                                    particleSystem.screenSize(canvas.width, canvas.height);
                                    particleSystem.screenPadding(150);
                                    that.initMouseHandling()
                                },
                                redraw: function () {
                                    ctx.fillStyle = "#f2fcfb";

                                    ctx.fillRect(0, 0, canvas.width, canvas.height);
                                    particleSystem.eachEdge(function (edge, pt1, pt2) {
                                        ctx.strokeStyle = edge.data.linkcolor;
                                        ctx.lineWidth = 2;
                                        ctx.beginPath();
                                        ctx.moveTo(pt1.x, pt1.y);
                                        ctx.lineTo(pt2.x, pt2.y);
                                        ctx.stroke();
                                    });
                                    // круги
                                    particleSystem.eachNode(function (node, pt) {
                                        // ctx.beginPath();
                                        // //var w = ctx.measureText(""+node.data.name).width + 10;
                                        // ctx.arc(pt.x, pt.y, 50, 0, 2 * Math.PI);
                                        // ctx.fillStyle = node.data.nodecolor;
                                        // ctx.fill();
                                        // ctx.font = "13px Arial yellow";
                                        // ctx.fillText(node.data.name, pt.x - ctx.measureText(""+node.data.name).width/2, pt.y);


                                        var gfx = arbor.Graphics(canvas);
                                        //var w = ctx.measureText(""+node.data.name).width - 35;
                                        var w = 100;
                                        //ctx.oval(pt.x-w/2, pt.y-w/2, w, w);
                                        // ctx.fillStyle = node.data.nodecolor;
                                        // ctx.fill();
                                        var text = node.data.name.split(' ');

                                        gfx.oval(pt.x-w/2, pt.y-w/2, w, w-5, {fill:node.data.nodecolor, alpha:node.data.alpha});

                                        if(text.length > 1){
                                            for(var i=0; i<text.length; i=i+1)
                                            {
                                                //alert(text[i])
                                                gfx.text(text[i], pt.x, pt.y+(i*14) - 5, {color:"black", align:"center", font:"Arial", size:12})
                                            }
                                        }
                                        else{
                                            gfx.text(node.data.name, pt.x, pt.y + 7, {color:"black", align:"center", font:"Arial", size:12})
                                        }
                                     });
                                    //квадраты
                                    // particleSystem.eachNode( //теперь каждую вершину
                                    //     function(node, pt){  //получаем вершину и точку где она
                                    //         var w = 20;   //ширина квадрата
                                    //         ctx.fillStyle = "#00c7bb"; //с его цветом понятно
                                    //         ctx.fillRect(pt.x-w/2, pt.y-w/2, w,w); //рисуем
                                    //         ctx.fillStyle = "black"; //цвет для шрифта
                                    //         ctx.font = 'italic 15px sans-serif'; //шрифт
                                    //         ctx.fillText (node.data.name, pt.x+8, pt.y+8); //пишем имя у каждой точки
                                    //     });
                                },
                                initMouseHandling: function () {
                                    var dragged = null;
                                    var hlt_nodes = [];
                                    var handler = {
                                        clicked: function (e) {
                                            var pos = $(canvas).offset();
                                            _mouseP = arbor.Point(e.pageX - pos.left, e.pageY - pos.top);
                                            dragged = particleSystem.nearest(_mouseP);
                                            if (dragged && dragged.node !== null) {
                                                dragged.node.fixed = true;
                                                window.location.href = dragged.node.data.link;
                                            }
                                            // $(canvas).bind('mousemove', handler.dragged);
                                            // $(window).bind('mouseup', handler.dropped);
                                            return false;
                                        },
                                        dragged: function (e) {

                                            var pos = $(canvas).offset();
                                            var s = arbor.Point(e.pageX - pos.left, e.pageY - pos.top);
                                            if (dragged && dragged.node !== null) {
                                                var p = particleSystem.fromScreen(s);
                                                dragged.node.p = p
                                            }
                                            return false;
                                        },
                                        dropped: function (e) {

                                            if (dragged === null || dragged.node === undefined) return;
                                            if (dragged.node !== null) dragged.node.fixed = false;
                                            dragged.node.tempMass = 10000;
                                            dragged = null;
                                            $(canvas).unbind('mousemove', handler.dragged);
                                            $(window).unbind('mouseup', handler.dropped);
                                            _mouseP = null;
                                            return false;
                                        }

                                        // move:function (e) {
                                        //     var pos = $(canvas).offset();
                                        //     _mouseP = arbor.Point(e.pageX - pos.left, e.pageY - pos.top);
                                        //     n_node = particleSystem.nearest(_mouseP);
                                        //     alert(n_node);
                                        //     $(canvas).bind('mouseover', handler.over);
                                        //     return false;
                                        // },
                                        // over:function (e) {
                                        //     // alert('la');
                                        // }

                                    };
                                    var handler_move = {

                                        moved: function (e) {
                                            var pos = $(canvas).offset();
                                            _mouseP = arbor.Point(e.pageX - pos.left, e.pageY - pos.top)
                                            nearest = particleSystem.nearest(_mouseP);


                                            if (!nearest.node) return false

                                            if (nearest.node.data.shape != 'dot') {

                                                selected = (nearest.distance < 10) ? nearest : null
                                                if (selected) {
                                                    //alert(selected.node.data.name);
                                                    // snode = FullGraph[selected.node.name][1]
                                                    // sys.tweenNode(snode,0.5,{color:"blue"})
                                                    // hlt_nodes.push(snode)
                                                } else {
                                                    // Tween Back for each node in hlt_nodes.
                                                    hlt_nodes.forEach(function (hlt_node) {
                                                        sys.tweenNode(hlt_node, 0.1, {color: CLR.ganetinode})
                                                    });
                                                }
                                                //TODO - Node list should be generated from the graph JS object.
                                            } else if ($.inArray(nearest.node.name, GanetiNodes) >= 0) {
                                                if (nearest.node.name != _section) {
                                                    _section = nearest.node.name
                                                    that.switchSection(_section)
                                                }
                                                dom.removeClass('linkable')
                                                window.status = ''
                                            }


                                            return false
                                        }
                                        };

                                    $(canvas).mousedown(handler.clicked);
                                    $(canvas).mousemove(handler_move.moved);
                                    //$(canvas).mouseover(handler_move.move);


                                }
                            }
                            return that;
                        }


                        $(document).ready(function () {
                            var sys = arbor.ParticleSystem(10000, 100, 0.75);
                            sys.parameters({gravity:true});
                            sys.renderer = Renderer("#viewport");
                            sys.addNode('Node central', {name: "{{$center[0]->category_name}}", nodecolor: "white", id: "{{$center[0]->id}}", id_parent: "{{$center[0]->id_parent}}", link:"/search/se_{{$center[0]->id}}/page_1"});
                            @if(isset($child) && count($child)>0)
                                    @foreach($child as $petal)
                                            @if($petal->category_name == "")
                                                sys.addNode('Node central', {name: "Ничего не найдено("});
                                                @break
                                            @else
                                                sys.addNode('Node_{{$loop->index}}', {name: "{{$petal->category_name}}", link:'/search/se_{{$petal->id}}/page_1', id: "{{$petal->id}}", id_parent: "{{$petal->id_parent}}", nodecolor:'#02c6ba'});
                                                // if (i >= 1){
                                                //sys.addEdge('Node central', 'Node_{{$loop->index}}', {linkcolor: "#888888"});

                                                // }
                                            @endif
                                    @endforeach
                            @endif

                            @if(isset($get_back))
                                sys.addNode('Node_back', {name: "{{$get_back[0]->category_name}}", link:'/search/se_{{$get_back[0]->id}}/page_1', nodecolor: 'aqua', id: "{{$get_back[0]->id}}", id_parent: "{{$get_back[0]->id_parent}}"});
                            //sys.addEdge('Node central', 'Node_back', {linkcolor: "#888888"});
                            @endif

                        });

                    })(this.jQuery);



                </script>




                {{--<div id="la">{{$man->id}}</div>--}}

         </div>

@endsection