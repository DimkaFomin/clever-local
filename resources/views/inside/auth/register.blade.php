@extends('inside.index')

@section('title', 'Регистрация на портале')

@section('modal')
@endsection


@section('content')
<div class="container">
    <div class="row margin-row">
        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 col-sm-offset-3" id="enter_form">
            <div class="form-title col-md-12 col-sm-12 col-xs-12">
                <h4>Регистрация на сайте</h4>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 form-content">
                <div class="add-margin-30">
                    <form id="register-form" class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('first') ? ' has-error' : '' }}">
                            <div class="col-md-12">
                                <input id="first" placeholder="Имя" type="text" class="form-control" name="first" value="{{ old('first') }}" required autofocus>
                                @if ($errors->has('first'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('first') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('second') ? ' has-error' : '' }}">
                            <div class="col-md-12">
                                <input id="second" placeholder="Фамилия" type="text" class="form-control" name="second" value="{{ old('second') }}" required >
                                @if ($errors->has('second'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('second') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <div class="col-md-12">
                                <input id="login" placeholder="E-Mail" type="email" class="form-control" name="login" value="{{ old('email') }}" required>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <div class="col-md-12">
                                <input id="password" placeholder="Пароль" type="password" class="form-control" name="password" required>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                                <input id="password-confirm" placeholder="Подтверждение пароля" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group help-block">
                            <div class="col-md-12">
                                <div class="checkbox">
                                    <label>
                                        <input id="checkbox1" type="checkbox" name="remember" {{ old('remember') ? 'checked' : ''}} required> С <a href ="#">порядком обработки персональных данных</a> согласен
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12 col-xs-offset-3 col-sm-offset-3 col-md-offset-3 ">
                                <button type="submit" class="btn btn-login">
                                    Регистрация
                                </button>
                            </div>
                        </div>

                        <div class="form-group help-block">
                            <div class="row">
                                <div class="col-md-6 col-sm-7 col-xs-7 col-lg-5">
                                    Уже есть учетная запись?
                                </div>
                                <div class="col-md-6 col-sm-3 col-xs-5 col-lg-7">
                                    <a class="login" href="{{ url('/login') }}">
                                        Авторизуйтесь
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="form-group help-block">
                            <div class="row">
                                <div class="col-md-6 col-sm-7 col-xs-7 col-lg-5">
                                    Забыли пароль?
                                </div>
                                <div class="col-md-6 col-sm-3 col-xs-5 col-lg-7">
                                    <a class="reset-pass" href="#resetpass"><!-- {{ url('/password/reset') }} -->
                                        Восстановить доступ
                                    </a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
