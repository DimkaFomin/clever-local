
@php
    $unread = '';
    if (Auth::id() == $message->user_id) {
        if (is_null($minLastRead) or $minLastRead < $message->created_at)
            $unread = 'unread';
    }
@endphp

<div class="media msg-list {{ $unread }}">
    <a class="pull-left" href="/id{{$message->user_id}}">
        @if (is_null($users[$message->user_id]['type']))
            <img class="media-object" alt="{{$users[$message->user_id]['first'] }} {{ $users[$message->user_id]['second'] }}" src="/img/no_avatar.png">
        @else
            <img class="media-object" alt="{{$users[$message->user_id]['first'] }} {{ $users[$message->user_id]['second'] }}" src="/storage/avatars/{{ $message->user_id }}.{{ $users[$message->user_id]['type'] }}">
        @endif
    </a>
    <div class="media-body">
        <small class="pull-right text-muted">{{ $message->created_at->diffForHumans() }}</small>
        <h5 class="media-heading">
            <a href="/id{{$message->user_id}}">
                {{$users[$message->user_id]['first'] }} {{ $users[$message->user_id]['second'] }}
            </a>
        </h5>
        <small class="text col-lg-10 col-md-10 col-sm-12 col-sm-12">
            {!! $message->body !!}
        </small>
    </div>
</div>
