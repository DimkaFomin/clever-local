@extends('inside.index')
@section('title', ''.$user[$user_id]->first.'  '.$user[$user_id]->second.'  '.$user[$user_id]->third)

@section('modal')
@endsection


@section('content')
    <div class="container">
        <div class="row margin-row ">
            <div class="col-md-3 col-sm-4 ">
                <div class="row bordered">
                    <div class="col-md-12 col-sm-12 col-xs-12 user-photo">
                        @if ($user[$user_id]->id == Auth::id())
                        <form enctype="multipart/form-data" id="user-avatar-upload-form" action="/id{{$user[$user_id]->id}}" method="POST">
                            <input type="file" name="avatar" id="upload-avatar">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <a class="user-avatar-upload" href="/id{{$user[$user_id]->id}}" type="file" name="avatar">
                        @endif
                                <span class="helper"></span>
                                @if (!is_null($user[$user_id]->photo_id))
                                    <img class="user-avatar" src="/uploads/avatars/{{ $user[$user_id]->id }}.{{$user[$user_id]->photo->type}}">
                                @else
                                    <img class="user-avatar" src="/img/no_avatar.png">
                                @endif
                        @if ($user[$user_id]->id == Auth::id())
                            </a>
                        </form>
                        @endif
                    </div>
                    @if ($user[$user_id]->subscribe_to->count() > 0)
                        <div class="col-md-12 col-sm-12 col-xs-12  box followbox">
                            <div class="col-md-12">
                                <h2>Подписки
                                    <span class="fcount">{{$user[$user_id]->subscribe_to->count()}}</span>
                                </h2>
                            </div>
                            <div class="col-md-12">

                                <ul>
                                    @foreach ( $user[$user_id]->subscribe_to->shuffle() as $index=>$subscribe)

                                        <li>
                                            <a href="/id{{ $subscribe->sub_id }}">
                                                @if (!is_null($user[$subscribe->sub_id-1]->photo_id))
                                                    <div class="user-avatar-sm">
                                                        <img class="user-avatar-pic" src="/uploads/avatars/{{ $subscribe->sub_id  }}.{{$user[$subscribe->sub_id-1]->photo->type}}">
                                                    </div>
                                                @else
                                                    <div class="user-no-avatar-sm">
                                                        <img class="user-avatar-pic" src="/img/no_avatar.png">
                                                    </div>
                                                @endif
                                                <p>{{ $user[$subscribe->sub_id-1]->first }}</p>
                                            </a>
                                        </li>

                                        @if ($index == 5)
                                            @break
                                        @endif
                                    @endforeach

                                </ul>

                            </div>
                        </div>
                    @endif
                    @if ($user[$user_id]->subscribe->count() > 0)
                        <div class="col-md-12 col-sm-12 col-xs-12  box followbox">
                            <div class="col-md-12">
                                <h2>Подписчики
                                    <span class="fcount">{{$user[$user_id]->subscribe->count()}}</span>
                                </h2>
                            </div>
                            <div class="col-md-12">
                                <ul>
                                    @foreach ( $user[$user_id]->subscribe->shuffle() as $index=>$subscribe)
                                        <li>
                                            <a href="/id{{ $subscribe->user_id }}">
                                                @if (!is_null($user[$subscribe->user_id-1]->photo_id))
                                                    <div class = "user-avatar-sm">
                                                        <img class="user-avatar-pic" src="/uploads/avatars/{{ $subscribe->user_id  }}.{{$user[$subscribe->user_id-1]->photo->type}}">
                                                    </div>
                                                @else
                                                    <div class = "user-no-avatar-sm">
                                                        <img class="user-avatar-pic" src="/img/no_avatar.png">
                                                    </div>
                                                @endif
                                                    <p>{{ $user[$subscribe->user_id-1]->first }}</p>
                                            </a>
                                        </li>

                                        @if ($index == 5)
                                            @break
                                        @endif
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif
                </div>
            </div>

            <div class="col-md-9 col-sm-8 user-info-container">
                <div class="row">
                    <div class="col-md-12  col-sm-12 col-xs-12 ">
                        <div class="col-md-12 col-sm-12 col-xs-12 user-name">
                            {{$user[$user_id]->first}} {{$user[$user_id]->second}}
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 big-box">
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <div class=" row">
                                    <div class="col-md-6 col-sm-4 col-xs-6">
                                        Место учебы
                                    </div>
                                    <div class="col-md-6 col-sm-8 col-xs-6">
                                        @if ($user[$user_id]->edu_place_id != NULL)
                                            {{$user[$user_id]->edu_place_id}}
                                        @else
                                            Не указано
                                        @endif
                                    </div>
                                </div>

                                <div class=" row">
                                    <div class="col-md-6 col-sm-4 col-xs-6">
                                        Специальность
                                    </div>
                                    <div class="col-md-6 col-sm-8 col-xs-6">
                                        @if ($user[$user_id]->edu_grade_id != NULL)
                                            {{$user[$user_id]->edu_grade_id}}
                                        @else
                                            Не указана
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <div class=" row">
                                    <div class="col-md-6 col-sm-4 col-xs-6">
                                        Место работы
                                    </div>
                                    <div class="col-md-6 col-sm-8 col-xs-6">
                                        @if ($user[$user_id]->work_place_id != NULL)
                                            {{$user[$user_id]->work_place_id}}
                                        @else
                                            Не указано
                                        @endif
                                    </div>
                                </div>
                                <div class=" row">
                                    <div class="col-md-6 col-sm-4 col-xs-6">
                                        Должность
                                    </div>
                                    <div class="col-md-6 col-sm-8 col-xs-6">
                                        @if ($user[$user_id]->job_id != NULL)
                                            {{$user[$user_id]->job_id}}
                                        @else
                                            Не указана
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="collapse-user-info" data-toggle="collapse" data-target="#full-info">
                                    Развернуть
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div id="full-info" class="collapse full-info-padding-10">
                                    <div class=" row">
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class=" row">
                                                <div class="col-md-6 col-sm-4 col-xs-6">
                                                    Телефон
                                                </div>
                                                <div class="col-md-6 col-sm-8 col-xs-6">
                                                    @if ($user[$user_id]->phone != NULL)
                                                        {{$user[$user_id]->phone}}
                                                    @else
                                                        Нет номера
                                                    @endif
                                                </div>
                                            </div>
                                            @if ($user[$user_id]->id == Auth::user()->id )
                                            <div class=" row">
                                                <div class="col-md-6 col-sm-4 col-xs-6">
                                                    E-mail
                                                </div>
                                                <div class="col-md-6 col-sm-8 col-xs-6">
                                                        {{$user[$user_id]->login}}
                                                </div>
                                            </div>
                                            @endif
                                            <div class=" row">
                                                <div class="col-md-6 col-sm-4 col-xs-6">
                                                    Skype
                                                </div>
                                                <div class="col-md-6 col-sm-8 col-xs-6">
                                                    @if ($user[$user_id]->skype != NULL)
                                                        {{$user[$user_id]->skype}}
                                                    @else
                                                        Не указан
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <div class=" row">
                                                <div class="col-md-6 col-sm-4 col-xs-6">
                                                    VK
                                                </div>
                                                <div class="col-md-6 col-sm-8 col-xs-6">
                                                    @if ($user[$user_id]->vk != NULL)
                                                        {{$user[$user_id]->vk}}
                                                    @else
                                                        Не указан
                                                    @endif
                                                </div>
                                            </div>
                                            <div class=" row">
                                                <div class="col-md-6 col-sm-4 col-xs-6">
                                                    Facebook
                                                </div>
                                                <div class="col-md-6 col-sm-8 col-xs-6">
                                                    @if ($user[$user_id]->fb != NULL)
                                                        {{$user[$user_id]->fb}}
                                                    @else
                                                        Не указан
                                                    @endif
                                                </div>
                                            </div>
                                            <div class=" row">
                                                <div class="col-md-6 col-sm-4 col-xs-6">
                                                    Сайт
                                                </div>
                                                <div class="col-md-6 col-sm-8 col-xs-6">
                                                    @if ($user[$user_id]->site != NULL)
                                                        {{$user[$user_id]->site}}
                                                    @else
                                                        Не указан
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class=" row">
                                        <div class="col-md-6  col-sm-12 full-info-padding-10">
                                            <div class=" row">
                                                <div class="col-md-6 col-sm-4 col-xs-6">
                                                    Дата рождения
                                                </div>
                                                <div class="col-md-6 col-sm-8 col-xs-6">
                                                    @if ($user[$user_id]->birthday != NULL)
                                                        {{$user[$user_id]->birthday}}
                                                    @else
                                                        Не указан
                                                    @endif
                                                </div>
                                            </div>
                                            <div class=" row">
                                                <div class="col-md-6 col-sm-4 col-xs-6">
                                                    Город
                                                </div>
                                                <div class="col-md-6 col-sm-8 col-xs-6">
                                                    @if ($user[$user_id]->country_id == NULL)
                                                        Не указан
                                                    @elseif ($user[$user_id]->country_id != NULL and $user[$user_id]->region_id == NULL)
                                                        {{$user[$user_id]->country_id}}
                                                    @elseif ($user[$user_id]->country_id != NULL and $user[$user_id]->region_id != NULL and $user[$user_id]->city_id == NULL)
                                                        {{$user[$user_id]->region_id}}
                                                    @else
                                                        {{$user[$user_id]->city_id}}
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                        </div>
                                    </div>
                                    <!--
                                    <div class=" row">
                                        <div class="col-md-12 full-info-padding-10">
                                            <div class=" row">
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    О себе
                                                </div>
                                            </div>
                                            <div class=" row">
                                                <div class="col-md-12  col-sm-12 col-xs-12">
                                                    13
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="col-md-12 col-sm-12 col-xs-12 big-box user-course">
                            <ul class="nav nav-pills">
                                <li class="active student">
                                    <a data-toggle="tab" href="#stud">Обучаюсь</a>
                                </li>
                                <li class="teacher">
                                    <a data-toggle="tab" href="#teach">Преподаю</a>
                                </li>
                            </ul>
                            <div class="tab-content clearfix">
                                <div id="stud" class="tab-pane fade in active">
                                    <div class="col-md-4">

                                    </div>
                                    <div class="col-md-4">

                                    </div>
                                    <div class="col-md-4">

                                    </div>
                                </div>
                                <div id="teach" class="tab-pane fade">
                                    <div class="">
                                    @if ($user[$user_id]->id == Auth::id())
                                    <div class="col-md-4 add-marginb-10">
                                        <div class="block span3 create-new-container">
                                        <div class="create-new ">
                                            <img class="img-responsive center-block" src="/img/add_course_icon.png">
                                            <b>Создать курс</b>
                                        </div>
                                        </div>
                                    </div>
                                    @endif
                                    @foreach ( $book as $index=>$book)
                                        <div class="col-md-4 add-marginb-10">
                                            <div class="block span3">
                                                <div class="product">
                                                    @if (!is_null($book->photo_id ))
                                                        <img src="/uploads/course/avatar/{{ $book->id }}.{{$book->photo->type}}">
                                                    @else
                                                        <img src="http://placehold.it/295x190/333333/FFFFFF">
                                                    @endif

                                                    <div class="buttons" style="cursor: pointer;">
                                                    </div>
                                                </div>

                                                <div class="info" style="height: 210px;">
                                                    <h4>{{$book->name}}</h4>
                                                    <span class="description">

                                                    </span>

                                                </div>

                                            </div>
                                        </div>
                                    @endforeach

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    </div>
                </div>
            </div>
        </div>

    <!--
        <div class="row margin-row ">
            <div class="col-md-12 ">
                6
            </div>
        </div>
        -->
    </div>



    <script>
        jQuery(document).ready(function(){
            upload_avatar();
        });
    </script>
@endsection