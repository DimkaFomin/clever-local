@section('side_menu')

    <div class="side-menu">
            <div class="side-menu-container">
                <ul class="nav navbar-nav">
                    @include(config('laravel-menu.views.bootstrap-items'), ['items' => $MyNavBar->roots()])

                </ul>

            </div>

    </div>
@endsection