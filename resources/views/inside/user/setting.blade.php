@extends('inside.index')

@section('title', 'Настройки пользователя')

@section('modal')
@endsection


@section('content')


    <div class="container">
        <div class="row margin-row">
            <div class="col-md-12  col-sm-12 col-xs-12 ">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 user-name ">
                        {{--<img src ="/img/on_progress.png">--}}
                        Редактирование профиля
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 add-marginb-30">
                    <form id ="config-user-form" role="form" method="POST" action="/id{{Auth::id()}}/setting">
                        {{ csrf_field() }}
                        <div class="col-md-12  col-sm-12 col-xs-12 ">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <strong>Основная информация</strong>
                                </div>
                                <div class="col-md-1 col-sm-1 col-xs-12 col-md-offset-11 col-sm-offset-11">
                                    <div class="form-group">
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        Фамилия:
                                        <input id="second_name" class="form-control" placeholder="Введите фамилию" name="second_name" required autofocus type="text" value="{{$user->second}}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        Имя:
                                        <input id="first_name" class="form-control" placeholder="Введите имя" name="first_name" required type="text" value="{{$user->first}}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        Отчество:
                                        <input id="third_name" class="form-control" placeholder="Введите отчество" name="third_name"  type="text" value="{{$user->third}}">
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        Пол:
                                    <select id="sex_name" class="selectpicker form-control" name="sex_name">
                                        <option data-id="0">Не указан</option>
                                        <option data-id="1" @if($user->sex == 1) selected @endif>Мужской</option>
                                        <option data-id="2" @if($user->sex == 2) selected @endif>Женский</option>
                                    </select>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        Дата рождения:
                                        <input id="birthday_name" class="form-control" placeholder="Введите дату рождения" name="birthday_name"  type="text" value="{{$user->birthday}}">
                                    </div>
                                </div>
                                <div class="col-md-1 col-sm-1 col-xs-12 col-md-offset-11 col-sm-offset-11">
                                    <div class="form-group">
                                      </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        Страна:
                                        <select class="select_country selectpicker form-control" data-live-search="true" name="country_name" id="country_name" data-link="/id{{Auth::id()}}/setting/region">
                                            <option data-id = '0'>Выберите страну</option>
                                            @foreach($countries as $country)
                                                <option data-id = "{{$country->id}}"
                                                        @if($country->id == $user->country_id)
                                                        selected
                                                        @endif
                                                >{{$country->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group  @if (is_nan($user->country_id) or is_null($user->country_id))hidden @endif">
                                        Регион:
                                        <select class="select_region selectpicker form-control" name="region_name" data-link="/id{{Auth::id()}}/setting/city"  data-live-search="true">
                                            <option data-id = '0'>Выберите регион</option>
                                            @if (!is_nan($user->region_id))
                                            @foreach($regions as $region)
                                                <option data-id = "{{$region->id}}"
                                                        @if($region->id == $user->region_id)
                                                        selected
                                                        @endif
                                                >{{$region->name}}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group  @if (is_nan($user->region_id) or is_null($user->region_id))hidden @endif">
                                        Город:
                                        <select class="select_city selectpicker form-control" name="city_name"  data-live-search="true">
                                            <option data-id = '0'>Выберите город</option>
                                            @if (!is_nan($user->region_id))
                                                @foreach($cities as $city)
                                                    <option data-id = "{{$city->id}}"
                                                            @if($city->id == $user->city_id)
                                                            selected
                                                            @endif
                                                    >{{$city->name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <strong>Образование и работа</strong>
                                </div>
                                <div class="col-md-1 col-sm-1 col-xs-12 col-md-offset-11 col-sm-offset-11">
                                    <div class="form-group">
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        Место учебы:
                                        <input id="edu_place_name" class="form-control" placeholder="В разработке" name="edu_place_name" type="text" value="{{$user->edu_place_id}}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        Специальность:
                                        <input id="edu_grade_name" class="form-control" placeholder="В разработке" name="edu_grade_name"  type="text" value="{{$user->edu_grade_id}}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-1 col-sm-1 col-xs-12 col-md-offset-11 col-sm-offset-11">
                                    <div class="form-group">
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        Место работы:
                                        <input id="work_place_name" class="form-control" placeholder="Введите место работы" name="work_place_name" type="text" value="{{$user->work_place_id}}">
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        Должность:
                                        <input id="job_name" class="form-control" placeholder="Введите должность" name="job_name"  type="text" value="{{$user->job_id}}">
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <strong>Контактная информация</strong>
                                </div>
                                <div class="col-md-1 col-sm-1 col-xs-12 col-md-offset-11 col-sm-offset-11">
                                    <div class="form-group">
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        E-mail:
                                        <input id="login_name" class="form-control" placeholder="Введите имя" name="login_name" required type="text" value="{{$user->login}}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        Телефон:
                                        <input id="phone_name" class="form-control" placeholder="Введите номер телефона" name="phone_name"  autofocus type="text" value="{{$user->phone}}">
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        Skype:
                                        <input id="skype_name" class="form-control" placeholder="Введите адрес Skype" name="skype_name"  type="text" value="{{$user->skype}}">
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        Вконтакте:
                                        <input id="vk_name" class="form-control" placeholder="Введите адрес страницы Вконтакте" name="vk_name"  type="text" value="{{$user->vk}}">
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        Facebook:
                                        <input id="fb_name" class="form-control" placeholder="Введите адрес страницы Facebook" name="fb_name"  type="text" value="{{$user->fb}}">
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        Личный сайт:
                                        <input id="site_name" class="form-control" placeholder="Введите адрес личного сайта" name="site_name"  type="text" value="{{$user->site}}">
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12 add-marginb-30">
                                    <button action="/id{{Auth::id()}}" type="button" class="btn-default btn btn-left goto_edit_course" ><b>Отмена</b></button>
                                    <button id="apply_config_course" type="submit" class="btn btn-success btn-right "><b>Сохранить</b></button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $(document).on("keyup", "#phone_name", function(e) {
                var r = this.value.replace(/\D/g, "");
                r = r.replace(/^0/, "");
                if (e.keyCode == 8) {
                    if(/^(\d)$/.test(r)){
                        r = r.replace(/^(\d).*/, "");
                        this.value = r;
                    }else if (/^(\d)(\d{0,3})$/.test(r)) {
                        r = r.replace(/^(\d)(\d{0,3}).*/, "+7 ($2");
                        this.value = r;
                    }else if (/^(\d)(\d{0,3})(\d{0,3})$/.test(r)) {
                        r = r.replace(/^(\d)(\d{0,3})(\d{0,3}).*/, "+7 ($2) $3");
                        this.value = r;
                    }
                    else if (/^(\d)(\d{0,3})(\d{0,3})(\d{0,2})$/.test(r)) {
                        r = r.replace(/^(\d)(\d{0,3})(\d{0,3})(\d{0,2})(\d{0,2}).*/, "+7 ($2) $3-$4");
                        this.value = r;
                    }
                }
                if (r.length > 6) {
                    // 11+ digits.
                    r = r.replace(/^(\d)(\d{0,3})(\d{0,3})(\d{0,2})(\d{0,2}).*/, "+7 ($2) $3-$4-$5");
                    this.value = r;
                }
                else if (r.length > 5) {
                    // 6..10 digits.
                    r = r.replace(/^(\d)(\d{0,3})(\d{0,3}).*/, "+7 ($2) $3 -");
                    this.value = r;
                }
                else if (r.length > 2) {
                    // 3..5 digits. Add (0XX..)
                    r = r.replace(/^(\d)(\d{0,3})/, "+7 ($2) ");
                    this.value = r;
                }
                else {
                    r = r.replace(/^(\d)/, "+7 ($1");
                    this.value = r;
                }
            });
            $('.selectpicker').selectpicker({
                size: 7
            });
            $('body').on('hide.bs.select', 'select[name="country_name"]', function (e) {
                var country_id = $(this).find(':selected').data('id');
                var link = $(this).data('link');
                if (country_id > 0) {
                    get_region(country_id, link);
                }
                else{
                    $('option', 'select[name="region_name"]').not(':eq(0)').remove();
                    $('select[name="region_name"]').selectpicker('refresh');
                    $('select[name="region_name"]').parent().parent().addClass('hidden');
                }
            });
            $('body').on('hide.bs.select', 'select[name="region_name"]', function (e) {
                var link = $(this).data('link');
                var region_id = $(this).find(':selected').data('id');
                console.log(region_id);
                if (region_id > 0) {
                    get_city(region_id, link);
                }
                else{
                    $('option', 'select[name="city_name"]').not(':eq(0)').remove();
                    $('select[name="city_name"]').selectpicker('refresh');
                    $('select[name="city_name"]').parent().parent().addClass('hidden');
                }
            });
        });

        function get_region (country_id, link){
            var _token = $("input[name='_token']").val();
                $.post(link, {
                    _token: _token,
                    country_id: country_id
                }, function (response) {
                    $('option', 'select[name="region_name"]').not(':eq(0)').remove();
                    $.each(response, function (key) {
                        $('select[name="region_name"]')
                            .append($("<option></option>")
                                .attr("data-id", response[key].id)
                                .text(response[key].name));
                    });
                    $('select[name="region_name"]').selectpicker('refresh');
                    $('select[name="region_name"]').parent().parent().removeClass('hidden');
                });

        }

        function  get_city(region_id, link) {
        var _token = $("input[name='_token']").val();
            $.post(link, {
                _token: _token,
                region_id: region_id
            }, function (response) {
                $('option', 'select[name="city_name"]').not(':eq(0)').remove();
                $.each(response, function (key) {
                    $('select[name="city_name"]')
                        .append($("<option></option>")
                            .attr("data-id", response[key].id)
                            .text(response[key].name));
                });
                $('select[name="city_name"]').selectpicker('refresh');
                $('select[name="city_name"]').parent().parent().removeClass('hidden');
            });
        }
    </script>

@endsection







