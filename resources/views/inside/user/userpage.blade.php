@extends('inside.index')

@section('title', ''.$user->first.'  '.$user->third.'  '.$user->second)


@section('modal')
    <div class="modal fade" id="addPhone" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog  modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myModalLabel">Привязать телефон</h4>
                </div>
                <div class="modal-body">
                    <div class="input-group" id="new_phone">
                    Номер телефона:
                        <input placeholder="+7 (___) ___-__-__"  id="email" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" >

                    </div>
                    <div class="input-group" id="confirm_phone" >
                        Код подтверждения
                        <input placeholder="хххх"  id="password" type="text" class="form-control" name="password" >

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-right" id="getPhoneNo" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove-circle"></span> Отмена
                    </button>
                    <button type="button" class="btn btn-primary btn-left" id="addConfirm" data-course_id="">
                        <span class="glyphicon glyphicon-plus"></span> Привязать
                    </button>
                    <button type="button" class="btn btn-primary btn-left" id="addPhoneOk" data-course_id="">
                        <span class="glyphicon glyphicon-ok"></span> Подтвердить
                    </button>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
        $("#confirm_phone").hide();
        $("#addPhoneOk").hide();
        var maxLen = 17;
        var lastLogin ="";
        $(document).on("keyup", "#email", function(e) {
            var login = $('#email').val();
            var key = e.keyCode || e.charCode;
            var check = login.replace(/[0-9]/g, '').replace(/[\(\)\-\+]/g, "").replace(/[_\s]/g, '');
            if (login.length > 18 || check != ""){
                $("#email").val(lastLogin);
            }else {
                console.log(check == "");
                console.log(login.length);
                if (key == 8 || key == 46) {
                    if (check == "" && (login.length == 12 || login.length == 15))
                        login = login.substr(0, login.length - 1);
                    else if (check == "" && login.length == 8)
                        login = login.substr(0, login.length - 2);
                    else if (login.length <= 4 && check == "")
                        login = "";
                    maxLen = login.length;
                } else if (check == "" && login.length < 17) {
                    if (login.length == 1) {
                        var temp = "+7 (";
                        if (login[0] == '7' || login[0] == '+' || login[0] == '8') {
                            login = temp;
                        } else {
                            login = temp.concat(login);
                        }
                    } else if (login.length == 7) {
                        var temp = ") ";
                        login = login.concat(temp);
                    } else if (login.length == 12 || login.length == 15) {
                        var temp = "-";
                        login = login.concat(temp);
                    }
                } else {

                }
                $("#email").val(login);
                lastLogin = login;
                if (login.length > maxLen)
                    maxLen = login.length;
            }
        });
            $(document).on('click','#addConfirm', function () {
                $("#addConfirm").prop( "disabled", true );
                $('#email').prop( "disabled", true );
                var login = $('#email').val();
                var check = login.replace(/[0-9]/g, '').replace(/[\(\)\-\+]/g, "").replace(/[_\s]/g, '');
                login = login.replace(/\D/g, '');
                if (check != "") {
                    alert("Не верный формат телефона");
                    $("#addConfirm").prop("disabled", false);
                    $('#email').prop("disabled", false);
                } else {
                    $("#new_phone").hide();
                    $("#addConfirm").hide();
                    $("#confirm_phone").show();
                    $("#addPhoneOk").show();
                    $.post("id"+{{Auth::id()}}+"/getconfirm", {email: login})
                        .done(function (response) {
                        });
                }
            });
            $(document).on('click','#addPhoneOk', function () {
                var password = $('#password').val();
                var login = $('#email').val();
                login = login.replace(/\D/g, '');
                var check = password.replace(/[0-9]/g, '').replace(/[\(\)\-\+]/g, "").replace(/[_\s]/g, '');
                if (password.length != 4 || check != "") {
                    alert("Код подтвержедния состоит из 4-х цифр");
                } else {
                    $.post("id"+{{Auth::id()}}+"/applyconfirm", {email: login, password: password})
                        .done(function (response) {
                            if(response.success) {
                                alert("Номер успешно привязан!");
                                $('#addPhone').modal('toggle');
                            }else if (response == 0){
                                $('#password').val("Неверный код подтверждения");
                            }
                        });
                }
            });
        });

    </script>
@endsection


@section('content')
    <div class="container">
        <div class="row margin-row ">
            <div class="col-md-3 col-sm-4 ">
                <div class="row bordered">


                    <div class="col-md-12 col-sm-12 col-xs-12 user-photo">

                        @if (!Auth::guest())
                            @if ($user->id == Auth::id())
                                <form enctype="multipart/form-data" id="user-avatar-upload-form" action="/id{{$user->id}}" method="POST">
                                    <input type="file" name="avatar" id="upload-avatar">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            @endif
                        @endif

                        <span class="helper"></span>

                        @if (!is_null($user->photo_id))
                            @foreach($photos as $photo)
                                @if($photo->id == $user->photo_id)
                                <img class="user-avatar" src="{{Storage::url('avatars/')}}{{$user->id}}.{{$photo->type}}">
                                @endif
                            @endforeach
                        @else
                            <img class="user-avatar" src="/img/no_avatar.png">
                        @endif


                        @if (!Auth::guest())
                            @if ($user->id == Auth::id())
                                    <a class="user-avatar-upload" href="/id{{$user->id}}" type="file" name="avatar">
                                        <div class="middle">
                                            <div class="text">Обновить</div>
                                        </div>
                                    </a>
                                </form>
                            @endif
                        @endif

                    </div>
                    @if (!Auth::guest())
                        @if ($user->id == Auth::id() )
                            {{--&& $user->phone == ""--}}
                            <div class="col-md-12 col-sm-12 col-xs-12 box">
                                <a href="#" data-toggle="modal" data-target="#addPhone"  class="btn-success btn btn-left btn-block"><b>Привязять телефон</b></a>
                            </div>
                        @endif
                    @endif


                @if (!Auth::guest())
                        @if ($user->id != Auth::id())
                            <div class="col-md-12 col-sm-12 col-xs-12  box  follow-btn" data-id="{{$user->id}}" data-auth="{{Auth::id()}}"data-name="{{Auth::user()->first}}" data-img="{{Auth::user()->photo_id}}">
                                <div class="col-md-12 ">
                                    @if($subscribes->count() > 0)
                                        @foreach ( $subscribes as $subscribe)
                                            @if($subscribe->id == Auth::id())
                                                <button action="/id{{$user->id}}/unfollow" type="button" class="btn-default btn btn-left btn-block unfollow"><b>Отписаться</b></button>
                                                @break
                                            @endif
                                            @if($subscribe == end($subscribes)  )
                                                <button action="/id{{$user->id}}/follow" type="button" class="btn-success btn btn-left btn-block follow" ><b>Подписаться</b></button>
                                            @endif
                                        @endforeach
                                    @else
                                        <button action="/id{{$user->id}}/follow" type="button" class="btn-success btn btn-left btn-block follow"><b>Подписаться</b></button>
                                    @endif
                                </div>
                            </div>
                        @endif
                    @endif

                    @if ($user->subscribe_to->count() > 0)
                        <div class="col-md-12 col-sm-12 col-xs-12  box followbox">
                            <div class="col-md-12">
                                <h2>Подписки
                                    <span class="fcount">{{$user->subscribe_to->count()}}</span>
                                </h2>
                            </div>
                            <div class="col-md-12">

                                <ul>
                                    @foreach ( $subscribe_tos as $subscribe)
                                        <li>
                                            <a href="/id{{ $subscribe->id }}">
                                                @if (!is_null($subscribe->photo_id))
                                                    <div class="user-avatar-sm">
                                                        @foreach($photos as $photo)@if($photo->id == $subscribe->photo_id)
                                                        <img class="user-avatar-pic" src="{{Storage::url('avatars/small/')}}{{ $subscribe->id  }}.{{$photo->type}}">
                                                        @endif @endforeach
                                                    </div>
                                                @else
                                                    <div class="user-no-avatar-sm">
                                                        <img class="user-avatar-pic" src="/img/no_avatar_small.png">
                                                    </div>
                                                @endif
                                                <p>{{$subscribe->first }}</p>
                                            </a>
                                        </li>

                                    @endforeach

                                </ul>

                            </div>
                        </div>
                    @endif
                    @if ($user->subscribe->count() > 0)
                        <div class="col-md-12 col-sm-12 col-xs-12  box followbox">
                            <div class="col-md-12">
                                <h2>Подписчики
                                    <span class="fcount">{{$user->subscribe->count()}}</span>
                                </h2>
                            </div>
                            <div class="col-md-12">
                                <ul>
                                    @foreach ( $subscribes as $subscribe)
                                        <li>
                                            <a href="/id{{ $subscribe->id }}">
                                                @if (!is_null($subscribe->photo_id))
                                                    <div class = "user-avatar-sm">
                                                        @foreach($photos as $photo)@if($photo->id == $subscribe->photo_id)
                                                        <img class="user-avatar-pic" src="{{Storage::url('avatars/small/')}}{{ $subscribe->id  }}.{{$photo->type}}">
                                                            @endif @endforeach
                                                    </div>
                                                @else
                                                    <div class = "user-no-avatar-sm">
                                                        <img class="user-avatar-pic" src="/img/no_avatar_small.png">
                                                    </div>
                                                @endif
                                                <p>{{$subscribe->first }}</p>
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif
                </div>
            </div>

            <div class="col-md-9 col-sm-8 user-info-container">
                <div class="row">
                    <div class="col-md-12  col-sm-12 col-xs-12 ">
                        <div class="col-md-12 col-sm-12 col-xs-12 user-name">
                            {{$user->first}} {{$user->third}} {{$user->second}}
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 big-box">
                            {{--<div class="col-md-6 col-sm-12 col-xs-12">--}}
                                {{--<div class=" row">--}}
                                    {{--<div class="col-md-6 col-sm-4 col-xs-6">--}}
                                        {{--Место учебы--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-6 col-sm-8 col-xs-6">--}}
                                        {{--@if ($user->edu_place_id != NULL)--}}
                                            {{--{{$user->edu_place_id}}--}}
                                        {{--@else--}}
                                            {{--Не указано--}}
                                        {{--@endif--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                                {{--<div class=" row">--}}
                                    {{--<div class="col-md-6 col-sm-4 col-xs-6">--}}
                                        {{--Специальность--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-6 col-sm-8 col-xs-6">--}}
                                        {{--@if ($user->edu_grade_id != NULL)--}}
                                            {{--{{$user->edu_grade_id}}--}}
                                        {{--@else--}}
                                            {{--Не указана--}}
                                        {{--@endif--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <div class=" row">
                                    <div class="col-md-6 col-sm-4 col-xs-6">
                                        Место работы
                                    </div>
                                    <div class="col-md-6 col-sm-8 col-xs-6">
                                        @if ($user->work_place_id != NULL)
                                            {{$user->work_place_id}}
                                        @else
                                            Не указано
                                        @endif
                                    </div>
                                </div>
                                <div class=" row">
                                    <div class="col-md-6 col-sm-4 col-xs-6">
                                        Должность
                                    </div>
                                    <div class="col-md-6 col-sm-8 col-xs-6">
                                        @if ($user->job_id != NULL)
                                            {{$user->job_id}}
                                        @else
                                            Не указана
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="collapse-user-info" data-toggle="collapse" data-target="#full-info">
                                    Развернуть
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div id="full-info" class="collapse full-info-padding-10">
                                    <div class=" row">
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class=" row">
                                                <div class="col-md-6 col-sm-4 col-xs-6">
                                                    Телефон
                                                </div>
                                                <div class="col-md-6 col-sm-8 col-xs-6">
                                                    @if ($user->phone != NULL)
                                                        {{$user->phone}}
                                                    @else
                                                        Нет номера
                                                    @endif
                                                </div>
                                            </div>
                                            @if (!Auth::guest())
                                                @if ($user->id == Auth::id())
                                                    <div class=" row">
                                                        <div class="col-md-6 col-sm-4 col-xs-6">
                                                            E-mail
                                                        </div>
                                                        <div class="col-md-6 col-sm-8 col-xs-6">
                                                            {{$user->login}}
                                                        </div>
                                                    </div>
                                                @endif
                                            @endif
                                            <div class=" row">
                                                <div class="col-md-6 col-sm-4 col-xs-6">
                                                    Skype
                                                </div>
                                                <div class="col-md-6 col-sm-8 col-xs-6">
                                                    @if ($user->skype != NULL)
                                                        {{$user->skype}}
                                                    @else
                                                        Не указан
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <div class=" row">
                                                <div class="col-md-6 col-sm-4 col-xs-6">
                                                    VK
                                                </div>
                                                <div class="col-md-6 col-sm-8 col-xs-6">
                                                    @if ($user->vk != NULL)
                                                        {{$user->vk}}
                                                    @else
                                                        Не указан
                                                    @endif
                                                </div>
                                            </div>
                                            <div class=" row">
                                                <div class="col-md-6 col-sm-4 col-xs-6">
                                                    Facebook
                                                </div>
                                                <div class="col-md-6 col-sm-8 col-xs-6">
                                                    @if ($user->fb != NULL)
                                                        {{$user->fb}}
                                                    @else
                                                        Не указан
                                                    @endif
                                                </div>
                                            </div>
                                            <div class=" row">
                                                <div class="col-md-6 col-sm-4 col-xs-6">
                                                    Сайт
                                                </div>
                                                <div class="col-md-6 col-sm-8 col-xs-6">
                                                    @if ($user->site != NULL)
                                                        {{$user->site}}
                                                    @else
                                                        Не указан
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class=" row">
                                        <div class="col-md-6  col-sm-12 full-info-padding-10">
                                            <div class=" row">
                                                <div class="col-md-6 col-sm-4 col-xs-6">
                                                    Дата рождения
                                                </div>
                                                <div class="col-md-6 col-sm-8 col-xs-6">
                                                    @if ($user->birthday != NULL)
                                                        {{$user->birthday}}
                                                    @else
                                                        Не указан
                                                    @endif
                                                </div>
                                            </div>
                                            <div class=" row">
                                                <div class="col-md-6 col-sm-4 col-xs-6">
                                                    Пол
                                                </div>
                                                <div class="col-md-6 col-sm-8 col-xs-6">
                                                    @if ($user->sex != NULL)
                                                        @if ($user->sex == 1)
                                                            Мужской
                                                        @else
                                                            Женский
                                                        @endif
                                                    @else
                                                        Не указан
                                                    @endif
                                                </div>
                                            </div>
                                            <div class=" row">
                                                <div class="col-md-6 col-sm-4 col-xs-6">
                                                    Место проживания
                                                </div>
                                                <div class="col-md-6 col-sm-8 col-xs-6">
                                                    @if ($user->country_id == NULL)
                                                        Не указан
                                                    @elseif ($user->country_id != NULL and $user->region_id == NULL)
                                                        {{$user->country->name}}
                                                    @elseif ($user->country_id != NULL and $user->region_id != NULL and $user->city_id == NULL)
                                                        {{$user->region->name}}
                                                    @else
                                                        {{$user->city->name}}
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                        </div>
                                    </div>
                                    <!--
                                    <div class=" row">
                                        <div class="col-md-12 full-info-padding-10">
                                            <div class=" row">
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    О себе
                                                </div>
                                            </div>
                                            <div class=" row">
                                                <div class="col-md-12  col-sm-12 col-xs-12">
                                                    13
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="col-md-12 col-sm-12 col-xs-12 big-box user-course">
                            <ul class="nav nav-pills">
                                <li class="active student">
                                    <a data-toggle="tab" href="#stud">Обучаюсь</a>
                                </li>
                                <li class="teacher">
                                    <a data-toggle="tab" href="#teach">Преподаю</a>
                                </li>
                            </ul>
                            <div class="tab-content clearfix" id="course_container">
                                <div id="stud" class="tab-pane fade in active">
                                    @foreach ( $students as $index=>$student)
                                        <div class="col-md-4 add-marginb-15 add-paddinglr-7 go_course">
                                            <div class="block span3">
                                                <div class="product">
                                                    @if (!is_null($student->course->photo_id ))
                                                        @foreach($photos as $photo)
                                                            @if($photo->id == $student->course->photo_id)
                                                                <img class = "" src="{{Storage::url('course_avatars/')}}{{ $student->course_id }}.{{$photo->type}}" >
                                                            @endif
                                                        @endforeach
                                                    @else
                                                        <img src="/img/course/no_avatar.png">
                                                    @endif

                                                    <div class="buttons pointer" >
                                                    </div>
                                                </div>

                                                <div class="info h160">
                                                    <h4>{{$student->course->name}}</h4>
                                                </div>
                                                <div class ="btn-container h130">


                                                    <button type="button" name="" class="btn btn-primary btn-block go_material" action="/course/id{{ $student->course_id }}/materials">
                                                        <span class="glyphicon glyphicon-list-alt"></span> Перейти к курсу
                                                    </button>

                                                    <button type="button" name="" class="btn btn-success btn-block go_my_results" action="/course/id{{ $student->course_id }}/my_results">
                                                        <span class="glyphicon glyphicon-list-alt"></span> Мой прогресс
                                                    </button>

                                                </div>

                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <div id="teach" class="tab-pane fade">
                                    <div class="row">
                                        @if ($user->id == Auth::id())
                                            <div class="col-md-4 add-marginb-15 add-paddinglr-7">
                                                <div class="block span3 create-new-container">
                                                    <div class="create-new ">
                                                        <img class="img-responsive center-block" src="/img/add_course_icon.png">
                                                        <b>Создать курс</b>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4 add-marginb-15 add-paddinglr-7">
                                                <div class="block span3 create-new-meeting-container">
                                                    <div class="create-new ">
                                                        <img class="img-responsive center-block" src="/img/add_course_icon.png">
                                                        <b>Создать мероприятие</b>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                        @foreach ( $courses as $index=>$course)
                                            <div class="col-md-4 add-marginb-15 add-paddinglr-7 go_course">
                                                <div class="block span3">
                                                    <div class="product">
                                                        @if (!is_null($course->photo_id ))
                                                            @foreach($photos as $photo)
                                                                @if($photo->id == $course->photo_id)
                                                                    <img class = "" src="{{Storage::url('course_avatars/')}}{{ $course->id }}.{{$photo->type}}" >
                                                                @endif
                                                            @endforeach
                                                        @else
                                                            <img src="/img/course/no_avatar.png">
                                                        @endif

                                                        <div class="buttons pointer" >
                                                        </div>
                                                    </div>

                                                    <div class="info h160">
                                                        <h4>{{str_limit($course->name, 77, '...')}}</h4>
                                                    </div>
                                                    @if($user->id == Auth::id())
                                                    <div class ="btn-container h130">


                                                        @if ($course->is_moderated == 0)
                                                            <button type="button" name="go_sell" action="/course/edit/id{{$course->id}}/moderate" class="btn btn-danger btn-block go_sell" data-course_id="{{$course->id}}">
                                                                <span class="glyphicon glyphicon-ruble"></span> Отправить на модерацию
                                                            </button>
                                                        @elseif ($course->is_moderated == 2)
                                                            <button data-toggle="tooltip" data-placement="top" title="Курс не прошел модерацию" type="button" class="btn btn-info btn-block go_shop" data-course_id="{{$course->id}}" disabled>
                                                                <span  class="glyphicon glyphicon-ruble"></span> Проходит модерацию
                                                            </button>
                                                        @else
                                                            <button data-toggle="tooltip" data-placement="top" title="Переход к списку студентов по данному курсу" type="button" class="btn btn-block btn-success go_student_results" action="/course/id{{ $course->id }}/my_students">
                                                                <span></span> Мои студенты
                                                            </button>
                                                        @endif


                                                        <button type="button" name="go_course" class="btn btn-primary btn-block go_course" action="/constructor/id{{$course->id}}">
                                                            <span class="glyphicon glyphicon-pencil"></span> Редактор курса
                                                        </button>
                                                        <button type="button" name="go_card" class="btn btn-primary btn-block go_card" action="/course/card/id{{$course->id}}">
                                                            <span class="glyphicon glyphicon-list-alt"></span> Карточка курса
                                                        </button>

                                                    </div>
                                                        @endif

                                                </div>

                                            </div>
                                        @endforeach
                                            <script>
                                                $('body').on('click', ".go_sell", function(){
                                                    var elem = $(this);
                                                    $.get( $(this).attr('action'), function(  ) {

                                                     }).done(function(data){
                                                        elem.removeClass('btn-info');
                                                        elem.addClass('disabled');
                                                        elem.addClass('btn-info');
                                                        elem.empty();
                                                        elem.append('<span  class="glyphicon glyphicon-ruble"></span> Проходит модерацию');
                                                     if (data == 0) alert ("Курс отправлен на модерацию");
                                                     if (data == 2) alert ("Курс ожидает модерации");
                                                     if (data == 1) alert ("Курс уже модерирован");
                                                     });
                                                });
                                            </script>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--
        <div class="row margin-row ">
            <div class="col-md-12 ">
                6
            </div>
        </div>
        -->
    </div>



    <script>
        jQuery(document).ready(function(){
            upload_avatar();
        });


    </script>
@endsection
