@extends('inside.index')

@section('title', 'Карточка курса '.$course->name)

@section('modal')
@endsection


@section('content')

    <div class="wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12" style = "padding: 0;">
                    <header id="header">

                        <div class="slider">
                            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                <!-- Wrapper for slides -->
                                <div class="carousel-inner" role="listbox">
                                    <div class="item active profile" style = "min-height: 250px; background-color: rgb(0, 177, 166); padding-right: 15px;">
                                        <p class="site-name" >{{$course->name}}</p>
                                        <a class = "site-name " style = "font-size: 12px;"  href="/id{{ $course->user_id }}"><b>Автор курса: {{$course->user->first}} {{$course->user->second}}</b> </a>
                                    </div>

                                </div>


                                   </div>
                        </div>
                        <nav class="navbar navbar-default" style = "margin-bottom: 0; border-radius: 0;">
                            <div class="navbar-header">

                                <a class="col-xs-12 navbar-brand-course" href="#" style = "background-color: #fff;">
                                    @if (!is_null($course->photo_id))
                                        <div class = "user-avatar-sm">
                                            <img class="course-avatar-pic" src="{{Storage::url('course_avatars/')}}{{ $course->id  }}.{{$course->photo->type}}">
                                        </div>
                                    @else
                                        <div class = "user-no-avatar-sm">
                                            <img class="course-avatar-pic" src="/img/course/no_avatar.png">
                                        </div>
                                    @endif


                                </a>

                                <div style = "font-size: 16pt; position: absolute; right: 10px; top: 5px;">
                                @if ($course->price > 0)
                                        <div class = "btn btn-success" style = "background-color: rgb(0, 177, 166);"><b> Купить за {{$course->price}}</b> р. </div>
                                @else
                                        @if ($course->user_id != Auth::id())
                                        <div class = "btn btn-success buy-course" style = "background-color: rgb(0, 177, 166);" >
                                            <a href="/course/buy/id{{$course->id}}" style="text-decoration: none; color: white;" data-id="{{$course->id}}"><b>Добавить к себе</b></a> </div>
                                        @endif
                                @endif
                                </div>
                            </div>
                        </nav>
                    </header>



                    <nav class="navbar navbar-default" style = "margin-bottom: 10px; border-radius: 0; font-size: 1.2em;">
                        <div class="navbar-header" style = "padding: 20px;">
                            <span class="">{!! $course->description !!}</span><br>
                        </div>
                    </nav>



                    <nav class="navbar navbar-default" style = "margin-bottom: 0; border-radius: 0;">
                        <div class="navbar-header" style = "padding: 20px;">
                        <div class="col-md-12" style = "padding: 0;">
                            <ol class="ordered-list">


                                @foreach($materials as $material)
                                        <li class = "li-lessons"> {{$material->name}} </li>
                                        @if ($loop->index < count($materials) - 1)
                                        <hr style  ="margin: 10px 0;">
                                        @endif
                                @endforeach
                            </ol>
                        </div>
                        </div>
                    </nav>
                </div>
            </div>
        </div>


    </div>
    <br>
@endsection