@extends('inside.courses.catalog')

@section('content_courses')
    <div class="container">
        @forelse ($courses as $course)
            @if(!is_null($course->source))
                {{--курс новый--}}
                <div class="col-md-4 col-sm-6">
                    <div class="block span3 to_course" style="margin-bottom: 20px;">
                        <div class="product">
                            @if($course->photo_id == '')
                                <img style = "height: 190px; object-fit: contain; background-color: #fff;" class="user-avatar-pic" src="/img/course/geekbrains.jpg">
                            @else
                                <img style = "height: 190px; object-fit: contain; background-color: #fff;" class="user-avatar-pic" src="{{Storage::url('other_course_avatars/')}}{{ $course->id-1  }}.{{$course->photo->type}}">
                            @endif
                        </div>
                        <a href ="/other_course/card/id{{$course->id}}" class="to_card">
                            <div class="info" style="height: 290px;">
                                <h4>{{$course->name}}</h4>
                                <span class="description" style = "font-size: 10pt;">
                                    @php
                                        if(strlen($course->name) < 130) {
                                            if (strlen($course->description) > 120){
                                            echo mb_substr($course->description,0,120, 'UTF-8').'...';
                                            }
                                            else
                                                echo $course->description;
                                        }
                                        else echo '...';
                                    @endphp

                                </span>

                            </div>
                        </a>

                        <a href="http://www.{{ $course->source  }}" class = "source"  style="padding-left: 25px">
                            Площадка: {{ $course->source  }}
                        </a>


                        <div class="details">
                        <span class="time" style="font-size: 18pt;"><span class="price">
                            @if($course->price > 0)
                                    {{$course->price}} р.
                                    <a class="btn btn-info pull-right" href="/other_course/card/id{{$course->id}}"><i class="icon-shopping-cart" data-id="{{$course->id}}"></i>Подробно</a>
                            @else
                                Бесплатно
                                    <a class="btn btn-info pull-right" href="/other_course/card/id{{$course->id}}"><i class="icon-shopping-cart" data-id="{{$course->id}}"></i>Подробно</a>
                            @endif
                        </span>
                        </div>
                    </div>
                </div>
                                {{--курс новый--}}
                        @else
                <div class="col-md-4 col-sm-6">
                    <div class="block span3 to_course" style="margin-bottom: 20px;">
                        <div class="product">
                                    {{--курс старый--}}
                            @if (!is_null($course->photo_id))
                                <div class = "user-no-avatar-sm" style = "height: 190px; object-fit: contain; background-color: #fff;">
                                    <img style = "height: 190px; object-fit: contain; background-color: #fff;" class="user-avatar-pic" src="{{Storage::url('course_avatars/')}}{{ $course->id  }}.{{$course->photo->type}}">
                                </div>
                            @else
                                <div class = "user-no-avatar-sm" style = "height: 190px; object-fit: contain; background-color: #fff;">
                                    <img style = "height: 190px; object-fit: contain; background-color: #fff;" class="user-avatar-pic" src="/img/course/no_avatar.png">
                                </div>
                            @endif

                        </div>
                        <a href ="/course/card/id{{$course->id}}" class="to_card">
                            <div class="info" style="height: 290px;">
                                <h4>{{$course->name}}</h4>
                                <span class="description" style = "font-size: 10pt;">
                                    @php
                                        if(strlen($course->name) < 130) {
                                            if (strlen($course->description) > 120){
                                            echo mb_substr($course->description,0,120, 'UTF-8').'...';
                                            }
                                            else
                                                echo $course->description;
                                        }
                                        else echo '...';
                                    @endphp

                                    </span>
                            </div>
                        </a>
                        <a href="/" class = "source"  style="padding-left: 25px">
                            Площадка: Clever-e.com
                        </a>

                        <div class="details">
                        <span class="time" style="font-size: 18pt;"><span class="price">
                                @if ($course->price > 0)
                                    {{$course->price}} р.
                                    </span></span>
                            @if ($course->user_id != Auth::id())
                                <span class="rating pull-right buy-course">
                                        <a class="btn btn-info pull-right" href="#"><i class="icon-shopping-cart" data-id="{{$course->id}}"></i>Купить</a>
                                    </span>
                            @endif

                            @else
                                Бесплатно
                                @if ($course->user_id != Auth::id())
                                    <span class="rating pull-right buy-course">
                                        <a class="btn btn-info pull-right" href="/course/buy/id{{$course->id}}" data-id="{{$course->id}}"><i class="icon-shopping-cart"></i>Изучать</a>
                                    </span>
                                    @endif
                                    @endif
                                    </span></span>
                        </div>
                    </div>
                </div>
                            {{--курс старый--}}
            @endif


        @empty
            Курсов нет
        @endforelse

            @if ($courses_var_count <= 15)
                <script>
                    $('.show-more-courses').hide();
                </script>
            @endif
    </div>
@endsection
