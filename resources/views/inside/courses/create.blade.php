@extends('inside.index')

@section('title', 'Создание курса')

@section('modal')
    <!-- xhr modla-->
    <div class="modal fade" id="xhrModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myModalLabel">Кроссерверные материалы</h4>
                </div>
                <div class="modal-body">
                    <h5>Выбирая данный пункт вам будет необходимо предоставлять доступ к материалам на сторонем ресурсе каждому стеденту в <b>ИНДИВИДУАЛЬНОМ</b> порядке.</h5>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-left" id="xhrOK" data-remove_id="" data-lesson_id="" data-dismiss="modal">
                        <span class="glyphicon glyphicon-ok"></span> ОК
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- end xhr modla -->
@endsection

@section('content')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css"/>


    <div class="container has-create-api">
        <div class="row margin-row">
            <div class="col-md-12  col-sm-12 col-xs-12 ">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 user-name">
                        Создание курса
                    </div>
                </div>
            </div>
            <form id ="create-form" role="form" method="POST" action="/course/create">
                {{ csrf_field() }}

            <div class="col-md-12  col-sm-12 col-xs-12 ">
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="form-group">
                            Название курса:
                            <input id="course_name" class="form-control" placeholder="Введите название курса" name="course_name" required autofocus type="text">
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="form-group">
                            Тип курса:
                            <select id="type" class="selectpicker form-control" name="type_name" title="Выберите тип">
                                <option data-id="0"> Он-лайн курс</option>
                                <option data-id="1"> Офф-лайн курс</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            Краткое описание:
                            <textarea type="text" rows="3" class="form-control" id="discription" placeholder="Краткое описание" value=""></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            Полное описание:
                            <textarea type="text" rows="3" class="form-control" id="full_description" placeholder="Полное описание" value=""></textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            Минимальная цена при продаже курса:
                        <div class="input-group">
                            <input id="course_price" class="form-control" placeholder="Цена" name="price" type="number" min="0">
                            <span class="input-group-addon">
                                <input class="free" aria-label="..." name="free"  type="checkbox">
                                    Бесплатный
                            </span>
                        </div>
                            <div class="help-block hidden help-error">

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" id="xhr_course">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <div class="input-group">
                                <input class="xhr_course" aria-label="..." name="xhr_course"  type="checkbox">
                                     Материалы курса находятся на стороннем ресурсе
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row hidden">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            Ссылка на материалы:
                            <input id="xhr_link" class="form-control" placeholder="Введите ссылку на материалы курса" name="xhr_link" type="text">
                            <div class="help-block-xhr hidden help-error">

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" id="buttons">
                    <div class="col-md-12 col-sm-12 col-xs-12 add-marginb-30">
                        <button id="cancel_course" action="/id{{Auth::id()}}" type="button" class="btn-default btn btn-left" ><b>Отмена</b></button>
                        <button id="add_course" type="submit" class="btn btn-success btn-right"><b>Создать курс</b></button>
                    </div>
                </div>

                <div id = "offline">

                    <div class="row">
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                Длительность курса:
                                <input id="course_length_hours" class="form-control" placeholder="Длительность в часах" name="course_name"  type="text">
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                Количество занятий:
                                <input id="lessons_count" class="form-control" placeholder="Введите количество занятий" name="course_name"   type="text">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                Максимальное количество студентов:
                                <input id="max_number_students" class="form-control" placeholder="Максимальное число слушателей" name="course_name"   type="text">
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                Дата начала курса:
                                <div class="input-group" id="datetimepicker1">
                                    <input type="text" class="form-control"/>
                                    <span class="input-group-addon">
                                  <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                Автор:
                                <input id="author" class="form-control" placeholder="Введите имя или название организации" name="course_name"   type="text">
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                Ссылка на автора:
                                <input id="author_link" class="form-control" placeholder="Введите ссылку на автора" name="course_name"   type="text">
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="col-md-6 col-sm-6 col-xs-6" style="padding-left: 0px">
                                Укажите сложность:
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <input class="dif_check" aria-label="..." name="difficult_checkbox"  type="checkbox">
                                Не указывать
                            </div>
                            <select id="difficulty" class="selectpicker form-control" name="difficulty_name" placeholder="Сложность" title="Выберите сложность">
                                <option data-id="1"> Низкая </option>
                                <option data-id="2"> Средняя </option>
                                <option data-id="3"> Высокая </option>
                            </select>

                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                Место проведения:
                                <input id="address" class="form-control" placeholder="Введите адрес, где будут проходить занятия" name="course_name"   type="text">
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            Необходимые знания:
                            <textarea type="text" rows="3" class="form-control" id="knoledge_requires" placeholder="Введите компетенции, которыми должен обладать студент" value=""></textarea>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            Полученные навыки:
                            <textarea type="text" rows="3" class="form-control" id="skills" placeholder="Укажите список навыков, полученных послу прохождения курса" value=""></textarea>
                        </div>
                    </div>

                    <br/>
                </div>





            </div>
            </form>
        </div>
    </div>

    <script>

        jQuery(function(){
            jQuery('#datetimepicker1').datetimepicker();
        });

        /* render alert xhr */
        $(document).ready(function () {
            var el = $("#offline");
            el.css('visibility', 'hidden');
            el.slideUp();


            var xhr = $("#xhr_course");
            xhr.css('visibility', 'hidden');
            xhr.slideToggle();

            $(".xhr_course").click(function (e) {
                if ($('.xhr_course').is(':checked')) {
                    $('#xhrModal').modal('show');
                    $('#xhr_link').parent().parent().parent().removeClass('hidden');
                }
                else
                    $('#xhr_link').parent().parent().parent().addClass('hidden');
            });

        /* end render alert xhr */

            $('.selectpicker').selectpicker({
                size: 7
            });

        });

    </script>

@endsection