@extends('inside.index')

@section('title', 'Редактирование курса')

@section('modal')
    <div class="modal fade" id="books_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Мои учебные материалы</h4>
                </div>
                <div class="modal-body">

                    <style>
                        .dropdown-book{
                            width: 70%;
                            border-radius: 0;
                            border: 1px solid #fff;
                        }

                        .to-book-editor{
                            width: 25%;
                            border-radius: 0;
                            border: 1px solid #fff;
                        }
                    </style>



                    @foreach($books as $book)

                    <button type="button" class="btn dropdown-book" data-toggle="collapse" data-target="#book_id{{$book->id}}">
                        {{$book->name}}
                    </button>

                        <button type="button" class="btn  btn-info to-book-editor" data-dismiss="modal" action="/book/edit/cid_{{$course->id}}/id{{$book->id}}">
                            Редактировать
                        </button>



                    <div id="book_id{{$book->id}}" class="collapse out">
                        @foreach($book->lessons as $lesson)
                        <div class = "col-md-9"  style = "width: 70%; margin-top: 10px;">
                            {{$lesson->name}} <!--<button action = /book/edit/cid_133/id11>-->
                        </div>

                        <div class = "col-md-3 btn btn-success add-lesson-to-course" data-lesson_id = "{{$lesson->id}}"  style = "width: 25%; margin-top: 10px; border: none; border-radius: 0;">
                        Копировать в курс
                        </div>


                        @endforeach

                            &nbsp;
                    </div>
                    @endforeach
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary create-book" action="/book/create/cid_{{$course->id}}" data-dismiss="modal" data-course_id="{{$course->id}}">Создать новый материал</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                   <!-- <button type="button" class="btn btn-primary">Сохранить изменения</button> -->
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="test_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Мои тестовые материалы</h4>
                </div>
                <div class="modal-body">

                    <style>
                        .to-test-editor, .add-test-to-course{
                            width: 25%;
                            border-radius: 0;
                            border: 1px solid #fff;
                        }
                    </style>



                    @foreach($quizes as $quiz)
                        <div class="row">
                        <div class="col-md-6" >
                            {{$quiz->name}}
                        </div>

                            <button type="button" class="col-md-3 btn  btn-info to-test-editor" data-dismiss="modal" action="/quiz/edit/cid_{{$course->id}}/id{{$quiz->id}}">
                                Редактировать
                            </button>

                            <button type="button" class="col-md-3 btn  btn-success add-test-to-course" data-quiz_id = "{{$quiz->id}}">
                                Копировать в курс
                            </button>

                        </div>
                    @endforeach

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary create-test" action="/quiz/create/cid_{{$course->id}}" data-dismiss="modal" data-course_id="{{$course->id}}">Создать новый тест</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                    <!-- <button type="button" class="btn btn-primary">Сохранить изменения</button> -->
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="task_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Добавить задание к курсу</h4>
                </div>
                <div class="modal-body">

                    <style>
                        .to-test-editor, .add-test-to-course{
                            width: 25%;
                            border-radius: 0;
                            border: 1px solid #fff;
                        }
                    </style>



                    @foreach($tasks as $task)
                        <div class="row">
                            <div class="col-md-6" >
                                {{$task->name}}
                            </div>

                            <button type="button" class="col-md-3 btn  btn-info to-test-editor" data-dismiss="modal" action="/task/edit/cid_{{$course->id}}/id{{$task->id}}">
                                Редактировать
                            </button>

                            <button type="button" class="col-md-3 btn  btn-success add-task-to-course" data-task_id = "{{$task->id}}">
                                Копировать в курс
                            </button>

                        </div>
                    @endforeach

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary create-task" action="/task/create/cid_{{$course->id}}" data-dismiss="modal" data-course_id="{{$course->id}}">Создать новую задачу</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                    <!-- <button type="button" class="btn btn-primary">Сохранить изменения</button> -->
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="text_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Добавить простой текст</h4>
                </div>
                <div class="modal-body">
                    Заголовок:
                    <input id = "plain-text-header" style = "width: 100%; resize: none;">
                    Текст:
                    <textarea id = "plain-text" style = "height: 300px; width: 100%; resize: none;"></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary add-text-to-course"  data-dismiss="modal">Сохранить</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                    <!-- <button type="button" class="btn btn-primary">Сохранить изменения</button> -->
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="video_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Добавить видео с youtube</h4>
                </div>
                <div class="modal-body">
                    Заголовок:
                    <input id = "video-header" style = "width: 100%; resize: none;">
                    Ссылка на видео:
                    <input id = "video-link" style = "width: 100%; resize: none;"></input>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary add-video-to-course" data-dismiss="modal">Сохранить</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                    <!-- <button type="button" class="btn btn-primary">Сохранить изменения</button> -->
                </div>
            </div>
        </div>
    </div>

    <!-- delete course -->
    <div class="modal fade" id="deleteConfirmationCourse" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog  modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myModalLabel">Удалить курс?</h4>
                </div>
                <div class="modal-body">
                    <h5>Удалить курс: </h5>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-right" id="deleteNo" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove-circle"></span> Отмена
                    </button>
                    <button type="button" class="btn btn-primary btn-left" id="deleteCourseOk" data-course_id=""data-user_id="{{Auth::id()}}" data-dismiss="modal">
                        <span class="glyphicon glyphicon-trash"></span> Удалить
                    </button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="preview_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Предпросмотр материала</h4>
                </div>
                <div class="modal-body preview-content">


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                    <!-- <button type="button" class="btn btn-primary">Сохранить изменения</button> -->
                </div>
            </div>
        </div>
    </div>

    <script>


        /*del course*/
        $("body").on("click", ".delete-container a", function (e) {
            var elem = $(this);
            var course_id = elem.data('remove_id_course');
            var course_name = elem.data('course_name');
            $("#deleteConfirmationCourse #deleteCourseOk").data("course_id", course_id);
            $("#deleteConfirmationCourse .modal-title").text("Удалить курс?");
            $("#deleteConfirmationCourse .modal-body h5").text("Удалить курс: " + course_name + " и все его содержание?");
            // e.preventDefault();
        });

        $("body").on("click", "#deleteCourseOk", function (e) {
            var course_id = $(this).data("course_id");
            var user_id = $(this).data("user_id");
            var _token = $("input[name='_token']").val();
            $.get("/constructor/delete_course/id" + course_id, function (response) {
                $('#panel').html(response.content);
                history.pushState(null, null, '../../id'+user_id);
                document.title = $(".user-name").text() + " Clever-e";
                upload_avatar();
            });
            e.preventDefault();
        });
        /*end del course*/
    </script>
    <!-- end delete course -->

    <script>

        function addMaterial(id, name, content) {

            var html =
                '<div class = "course_block col-md-12" data-material_id = "' + id + '">' +
                '<div class = "glyphicon glyphicon-arrow-up arrow-up"></div>' +
                '<div class = "glyphicon glyphicon-arrow-down arrow-down"></div>' +
                '<div class = "glyphicon glyphicon-remove-circle remove-block" data-material_id = "' + id + '"></div>' +
                '<div class = "glyphicon glyphicon-cog pencil"></div>' +
                '<div class = "block-content"><div class="material-name">' + name + '</div>' +
                '<div class = "preview-material" data-material_id =  "' + id + '" style =  "cursor: pointer;"  data-toggle="modal" data-target="#preview_modal">Просмотреть материал</div></div>' +
                '<div class = "block-content-edit" style = "display: none;">' +
                '<input class = "block-content-edit-input" value ="' + name + '"></div></div>';

            $(".course_blocks").append(html);
            get_material_priority();
        }

        function renderTest(data) {

            var html = '';

            data = data.questions;

            data.forEach(function(question, i, data) {
                i++;
                html +=
                    '<div class = "question_material_preview"><b>'+i +')'+question["name"]+'</b></div>';

                if (question["variant"])
                question["variant"].forEach(function(variant, j){
                    j++;

                    if (variant['is_true'])
                    html +=
                        '<div class = "variant_material_preview">++'+j+')'+variant["name"]+'</div>';
                    else html +=
                        '<div class = "variant_material_preview">--'+j+')'+variant["name"]+'</div>';
                });

            });



            return html;
        }

        $(".add-test-to-course").on('click', function(){

            elem = $(this);

            $.get( "/constructor/id{{$course->id}}/add_quiz_id"+$(this).data('quiz_id'), {
            }).done(function(data){
                elem.css('background-color', 'grey');
                elem.html('Добавлено');

                addMaterial(data['id'], data['name'], data['content']);

            });
        });

        $(".add-task-to-course").on('click', function(){

            elem = $(this);

            $.get( "/constructor/id{{$course->id}}/add_task_id"+$(this).data('task_id'), {
            }).done(function(data){
                elem.css('background-color', 'grey');
                elem.html('Добавлено');

                addMaterial(data['id'], data['name'], data['content']);

            });
        });

        $(".add-lesson-to-course").on('click', function(){

            elem = $(this);

            $.get( "/constructor/id{{$course->id}}/add_lesson_id"+$(this).data('lesson_id'), {
            }).done(function(data){
                elem.css('background-color', 'grey');
                elem.html('Добавлено');

                addMaterial(data['id'], data['name'], data['content']);


            });
        });


        $(".add-text-to-course").on('click', function(){
            $.get( "/constructor/id{{$course->id}}/add_text", {
                text: $('#plain-text').val(),
                name: $('#plain-text-header').val(),
                priority: '1'

            }).done(function(data){

                addMaterial(data['id'], data['name'], data['content']);

            });
        });

        $(".add-video-to-course").on('click', function(){

            function getId(url) {
                var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
                var match = url.match(regExp);

                if (match && match[2].length == 11) {
                    return match[2];
                } else {
                    return 'error';
                }
            }

            var videoId = getId($('#video-link').val());

            var iframeMarkup = '<div class = "videowrapper"><iframe class = "video" src="//www.youtube.com/embed/'
                + videoId + '" frameborder="0" allowfullscreen></iframe></div>';


            if (videoId !== 'error') {
                $.get("/constructor/id{{$course->id}}/add_video", {
                    text: iframeMarkup,
                    name: $('#video-header').val(),
                    priority: '1'

                }).done(function (data) {
                    addMaterial(data['id'], data['name'], data['content']);

                });
            } else alert('Неверная ссылка');
        });

        $('body').on('click', '.preview-material', function(){
            $.get( "/constructor/get_material/id"+$(this).data('material_id'), {

            }).done(function(data){
                if (data['type'] == 4){
                    $('.preview-content').html(renderTest(JSON.parse(data['json'])));
                } else {
                    //data = JSON.parse(data);
                    $('.preview-content').html(data['json']);
                }
                MathJax.Hub.Queue(["Typeset",MathJax.Hub]);

            });
        });

    </script>
@endsection

@section('modal')
@endsection


@section('content')

    <div class = "container  has-editor-api">
    <div class="row margin-row">
        <div class="col-md-12  col-sm-12 col-xs-12 ">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 block course-head-container">
                    <div class="col-md-11 col-sm-11 col-xs-11 left-container">
                        <div class="row">
                            <div class="col-md-2 col-sm-3 col-xs-12 course-avatar-container">
                                <form enctype="multipart/form-data" id="course-avatar-upload-form"
                                      action="/course/edit/id{{$course->id}}" method="POST">
                                    <input type="file" name="avatar" id="upload-avatar">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="course_id" value="{{$course->id}}">
                                    <a class="course-avatar-upload" href="/course/edit/id{{$course->id}}" type="file"
                                       name="avatar">
                                        <span class="helper"></span>
                                        @if (!is_null($course->photo_id))
                                            <img class="course-avatar"
                                                 src="{{Storage::url('course_avatars/small/')}}{{ $course->id }}.{{$course->photo->type}}" style = "max-width: 130px; height: 140px; object-fit: contain;">
                                        @else
                                            <img class="course-avatar" src="/img/course/no_avatar.png">
                                        @endif
                                        <div class="middle">
                                            <div class="text">Обновить</div>
                                        </div>
                                    </a>
                                </form>
                            </div>
                            <div class="col-md-10 col-sm-9 col-xs-12 ">
                                <div class="row">
                                    <div class="col-md-12  col-sm-12 col-xs-12 course-name">
                                        {{str_limit($course->name, 150, '...')}}
                                    </div>
                                    <div class="col-md-12  col-sm-12 col-xs-12 ">
                                        @if (is_null($course->discription) or $course->discription == "")
                                            Описание отсутсвует.
                                        @else
                                            {!!  str_limit($course->discription, 250, '...')!!}
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-12 right-container">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-3 btn-container edit-container">
                                <a href="/constructor/config/id{{$course->id}}">
                                    <div class="edit_bar_element_edit pointer " data-toggle="popover"
                                         data-trigger="hover" data-placement="bottom"
                                         data-content="Настройки курса">
                                        <span class="glyphicon glyphicon-cog"></span>

                                    </div>
                                    <div class="edit_bar_element_text pointer">
                                        Настройки
                                    </div>
                                </a>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-3 btn-container moderate-container">



                                <div class="edit_bar_element_moderate pointer" data-toggle="popover"
                                     data-trigger="hover" data-placement="bottom"
                                     data-content="Отправить на модерацию">
                                    <span class="glyphicon glyphicon-ruble"></span>
                                </div>

                                <script>
                                    $(".edit_bar_element_moderate").on('click', function(){
                                        $.get( "/course/edit/id{{$course->id}}/moderate", function(  ) {

                                        }).done(function(data){
                                            if (data == 0) alert ("Курс отправлен на модерацию");
                                            if (data == 2) alert ("Курс ожидает модерации");
                                            if (data == 1) alert ("Курс уже модерирован");
                                        });
                                    });
                                </script>

                                <div class="edit_bar_element_text pointer">
                                    Продать
                                </div>


                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-3 btn-container delete-container">
                                <a href="#" data-toggle="modal" data-target="#deleteConfirmationCourse" data-course_name= "{{$course->name}}" data-course_name= "{{$course->name}}" data-remove_id_course="{{$course->id}}">
                                    <div class="edit_bar_element_delete pointer" data-toggle="popover"
                                         data-trigger="hover" data-placement="bottom" data-content="Удалить курс">
                                        <span class=" 	glyphicon glyphicon-trash"></span>
                                    </div>
                                    <div class="edit_bar_element_text pointer">
                                        Удалить
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-3 btn-container close-container">
                                <a href="/course/id{{$course->id}}/materials">
                                    <div class="edit_bar_element_close pointer" data-toggle="popover"
                                         data-trigger="hover" data-placement="bottom"
                                         data-content="Просмотр">
                                        <span class=" 	glyphicon glyphicon-eye-open"></span>

                                    </div>
                                    <div class="edit_bar_element_text pointer">
                                        Просмотр
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

        <div class="row margin-row">

            <style>
                .add_button {
                    cursor: pointer;
                    height: 45px;
                    background-color: #fff;
                    box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
                    padding: 0;
                    margin-bottom: 5px;
                }

                .add_button_icon{
                    font-size: 20pt; background-color: #02C6BA; height: 45px; padding: 9px; color: #fff; width: 45px;
                }

                .add_button_title{
                   position: absolute; font-size: 12pt; z-index: 1000; top: 10px; left: 55px;
                }

            </style>

            <div class = "col-md-3" style = "padding: 0;">
                Добавить в курс:<br>

            <div class = "add_book add_button col-md-11" data-toggle="modal" data-target="#books_modal">

            <div class = "glyphicon glyphicon-book add_button_icon">
            </div>

                <div class = "add_button_title" >Текстовый материал</div>

            </div>

                <div class = "add_test add_button col-md-11" data-toggle="modal" data-target="#test_modal">
                    <div class = "glyphicon glyphicon-list-alt add_button_icon">
                    </div>

                    <div class = "add_button_title">Тестирование</div>

                </div>

                <div class = "add_task add_button col-md-11" data-toggle="modal" data-target="#task_modal">

                    <div class = "glyphicon glyphicon-list-alt add_button_icon">
                    </div>

                    <div class = "add_button_title">Задание к курсу</div>
                </div>

                <div class = "add_book add_button col-md-11" data-toggle="modal" data-target="#text_modal">

                    <div class = "glyphicon glyphicon-align-justify  add_button_icon">
                    </div>
                    <div class = "add_button_title" style = "margin-top: -6px; line-height: 17px;">Простой текст без форматирования</div>
                </div>


            <div class = "add_video add_button col-md-11" data-toggle="modal" data-target="#video_modal">
                <div class = "glyphicon glyphicon-film add_button_icon">
                </div>

                <div class = "add_button_title">Видео с YouTube.com</div>

            </div>

                <!--

            <div class = "add_audio add_button col-md-11">
                <div class = "glyphicon glyphicon-volume-up add_button_icon">
                </div>

                <div class = "add_button_title">Аудио</div>

            </div>

            <div class = "add_prezi add_button col-md-11">
                <div class = "glyphicon glyphicon-blackboard add_button_icon">
                </div>

                <div class = "add_button_title">Презентацию</div>

            </div>  -->


                <br>
            </div>


        <style>
            .course_block{
                background-color: #fff;
                width: 100%;
                height: 100px;
                padding: 0;
                margin-bottom: 10px;
                box-shadow: 0 1px 2px rgba(0, 0, 0, .05);
            }

            .course_blocks{
                padding: 0;

            }

            .arrow-up, .arrow-down, .remove-block, .pencil{
                height: 50px;
                width: 50px;
                background-color: #02C6BA;
                font-size: 30px;
                line-height: 48px;
                color: #fff;
                padding-left: 8px;
                position: absolute;
                cursor: pointer;
                border: 1px solid #fff;
            }

            .arrow-down{
                top: 50px;
            }

            .remove-block{
                right: 0;
            }

            .pencil{
                top: 50px;
                right: 0px;
            }

            .block-content, .block-content-edit{
                padding: 5px 55px;
            }




        </style>

            Материалы курса:
        <div class = "course_blocks col-md-9">

            @foreach($materials as $material)
            <div class = "course_block col-md-12" data-material_id = "{{$material->id}}" id = 'material_block_{{$material->id}}'>
                <div class = "glyphicon glyphicon-arrow-up arrow-up">
                </div>

                <div class = "glyphicon glyphicon-arrow-down arrow-down">
                </div>

                <div class = "glyphicon glyphicon-remove-circle remove-block" data-material_id = "{{$material->id}}">
                </div>

                <div class = "glyphicon glyphicon-cog pencil">
                </div>



                <div class = "block-content">
                    <div class = "material-name">{{$material->name}}</div>

                     <div class = "preview-material" data-material_id =  "{{$material->id}}" style =  "cursor: pointer;"  data-toggle="modal" data-target="#preview_modal">Просмотреть материал</div>
                </div>

                <div class = "block-content-edit" style = "display: none;">
                    <input class = "block-content-edit-input" value ="{{$material->name}}">
                </div>

            </div>
            @endforeach
        </div>
        </div>

        <script>

            $('body').on('click', '.glyphicon-cog', function(){
                $(this).removeClass('glyphicon-cog');
                $(this).addClass('glyphicon-floppy-disk');
                $(this).parent().children('.block-content').hide();
                $(this).parent().children('.block-content-edit').show();
            });

            $('body').on('click', '.glyphicon-floppy-disk', function(){
                $(this).removeClass('glyphicon-floppy-disk');
                $(this).addClass('glyphicon-cog');
                $(this).parent().children('.block-content-edit').hide();
                $(this).parent().children('.block-content').show();

                new_name = $(this).parent().children('.block-content-edit').children('.block-content-edit-input').val();
                $(this).parent().children('.block-content').children('.material-name').html(new_name);


                $.get("/constructor/rename_material", {
                    course_id: '{{$course->id}}',
                    new_name: new_name,
                    material_id: $(this).parent().data('material_id')
                }).done(function (data) {

                });


            });


            $('body').on('click', '.remove-block', function(){
                elem = $(this);
                $.get( "/constructor/remove_material/cid_{{$course->id}}/id"+$(this).data('material_id'), {

                }).done(function(data){
                    elem.parent().remove();
                    get_material_priority();

                });
            });

            $('body').on('click', '.arrow-up', function(){
                var pdiv = $(this).parent('div');
                var target = pdiv.prev();


                pdiv.swap({
                    target: target.attr('id'),
                    opacity: "0.9",
                    speed: 300,
                    callback: function() {
                        pdiv.insertBefore(pdiv.prev());
                        pdiv.attr('style', '');
                        target.attr('style', '');
                        get_material_priority();
                        return false
                    }
                });
                return false
            });


            $('body').on('click', '.arrow-down', function(){
                var pdiv = $(this).parent('div');

                var target = pdiv.next()

                pdiv.swap({
                    target: target.attr('id'),
                    opacity: "0.9",
                    speed: 300,
                    callback: function() {
                        pdiv.insertAfter(pdiv.next());
                        pdiv.attr('style', '');
                        target.attr('style', '');
                        get_material_priority();
                        return false
                    }
                });
            });

            </script>

        <!-- Modal -->

    </div>



    <script>
        jQuery(document).ready(function () {
            $('.lesson-content').addClass('hide_block');
            $('[data-toggle="popover"]').popover({
                container: '#panel'
            });
            upload_course_avatar();

            CKEDITOR.replace('mytextarea');
            CKEDITOR.config.resize_enabled = false;
            CKEDITOR.config.height = window.innerHeight-254*2;
            CKEDITOR.config.width = '100%';

            if(window.innerWidth >990) {
                $('.menu-lesson').css('height', window.innerHeight - 254 * 1.32);
                $('.menu-lesson').css('max-height', window.innerHeight - 254 * 1.32);
                $('.accordion').css('max-height', window.innerHeight - 254 * 1.48);
            }


        });
        $('body').on('hover', '.right-container', function (e) {
            if ($('.popover-content').length > 1) {
                $('.popover').remove();
            }

        });

        function get_material_priority() {
            var priority_array = [];
            var material_array = [];
            $( ".course_block" ).each(function( index ) {
                //priority_array.push({material_id:$(this).data('material_id'), priority:index})
                material_array.push($(this).data('material_id'));

            });
            var _token = $("input[name='_token']").val();
            $.post('/constructor/save_priority', {
                _token: _token,
                material_id: material_array
            },function(response){
                if (response != 0)
                    alert("Порядок урок сохранен");
            });
        }

    </script>

@endsection





