<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    @include('inside.layers.header')

    <title>Панель управления Clever-e</title>

    <!-- Bootstrap core CSS -->
    <link href="../../dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="dashboard.css" rel="stylesheet">


</head>

<body>

<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Панель администратора</a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="id{{Auth::id()}}"><i
                                class="fa fa-sign-in"></i> Вернуться на сайт</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="container-fluid" style = "margin-top: 50px;">
<div class = "container">
    <div class = "row">

        <h3>Присланные курсы на модерацию</h3>
        <table class="table">
            <thead>
            <tr>
                <th>Пользователь</th>
                <th>Курс</th>
                <th>Действия</th>
            </tr>
            </thead>

            <tbody>

            @foreach($moderation as $item)
                <tr>
                    <td><a href = "/id{{$item->user->id}}">{{$item->user->first}} {{$item->user->second}}</a></td>
                    <td><a href = "/course/id{{$item->course->id}}/materials">{{$item->course->name}}</a></td>
                    <td>
                        <button class = "btn btn-success accept-course" data-event_id = "{{$item->id}}">Принять</button>
                        <button class = "btn btn-danger reject-course" data-event_id = "{{$item->id}}">Отклонить</button>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <h3>Курсы на стороних ресурсах</h3>
        <table class="table">
            <thead>
            <tr>
                <th>Преподаватель</th>
                <th>Курс</th>
                <th>Студент</th>
                <th>Логин</th>
                <th>Пароль</th>
                <th>Инструкция</th>
                <th>Действия</th>
            </tr>
            </thead>

            <tbody>


            @foreach($xhr_check as $item)
                <tr>
                    <td><a href = "/id{{$item->course->user->id}}">{{$item->course->user->first}} {{$item->course->user->second}}</a></td>
                    <td><a href = "http://{{$item->course->material_link}}" target="_blank">{{$item->course->name}}</a></td>
                    <td><a href = "/id{{$item->user->id}}">{{$item->user->first}} {{$item->user->second}}</a></td>
                    <td>{{$item->login}}</td>
                    <td>{{$item->password}}</td>
                    <td>{{$item->comment}}</td>
                    <td>
                        <button class = "btn btn-success accept-xhr" data-xhr_id = "{{$item->id}}" data-student_id = "{{$item->user->id}}" data-course_id = "{{$item->course->id}}">Принять</button>
                        <button class = "btn btn-danger reject-xhr" data-xhr_id = "{{$item->id}}"data-student_id = "{{$item->user->id}}" data-course_id = "{{$item->course->id}}">Отклонить</button>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <h3>Вопросы от зарегистрированных пользователей</h3>
        <table class="table">
            <thead>
            <tr>
                <th>Пользователь</th>
                <th>Вопрос</th>
                <th>Действия</th>
            </tr>
            </thead>

            <tbody>

            @foreach($questions as $item)
                <tr>
                    <td>{{$item->user->first}} {{$item->user->second}}</td>
                    <td>{{$item->text}}</td>
                    <td>
                        <button class = "btn btn-success">Ответить</button>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <h3>Вопросы от НЕзарегистрированных пользователей</h3>
        <table class="table">
            <thead>
            <tr>
                <th>Пользователь</th>
                <th>Вопрос</th>
                <th>Действия</th>
            </tr>
            </thead>

            <tbody>

            @foreach($questions_guests as $item)
                <tr>
                    <td>{{$item->name}} {{$item->email}}</td>
                    <td>{{$item->text}}</td>
                    <td>
                        <button class = "btn btn-success">Ответить</button>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>


</div>
</div>
</div>


<script>
    $(".accept-course").on('click', function(){
       elem = $(this);
       event_id = elem.data('event_id');

        $.get( '/admin/accept/id'+event_id, function(  ) {

        }).done(function(data){
            elem.parent().parent().remove();
            alert("Курс одобрен");
        });

    });



    $(".reject-course").on('click', function(){
        elem = $(this);
        event_id = elem.data('event_id');

        $.get( '/admin/reject/id'+event_id, function(  ) {

        }).done(function(data){
            elem.parent().parent().remove();
            alert("Курс отклонен");
        });
    });


    $(".accept-xhr").on('click', function(){
        elem = $(this);
        xhr_id = elem.data('xhr_id');
        student_id = elem.data('student_id');
        course_id = elem.data('course_id');

        $.get( '/admin/accept_xhr/id'+xhr_id+'_sid'+student_id+'_cid'+course_id, function(  ) {

        }).done(function(data){
            elem.parent().parent().remove();
            alert("Ссылка одобрена");
        });

    });

    $(".reject-xhr").on('click', function(){
        elem = $(this);
        xhr_id = elem.data('xhr_id');
        student_id = elem.data('student_id');
        course_id = elem.data('course_id');

        $.get( '/admin/reject_xhr/id'+xhr_id+'_sid'+student_id+'_cid'+course_id, function(  ) {

        }).done(function(data){
            elem.parent().parent().remove();
            alert("Ссылка отклонена");
        });
    });
</script>
<!-- Bootstrap core JavaScript

</body>
</html>
