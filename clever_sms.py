import json
import os
import time
from lxml import html
from queue import *
import requests
import re
import urllib.parse
import threading
from threading import Thread
import serial


secretkey = 'cleversms'
http_server = 'http://clever-e.com/smssender_' + secretkey

clientQueue = Queue()
port = 'COM5'


def send_sms(recipient, message):
    try:
        phone = serial.Serial(port, 460800, timeout=5)

        try:
            time.sleep(0.5)
            phone.write(b'ATZ\r')
            time.sleep(0.5)
            phone.write(b'AT+CMGF=1\r')
            time.sleep(0.5)
            phone.write(b'AT+CMGS="' + recipient.encode() + b'"\r')
            time.sleep(0.5)
            phone.write(message.encode() + b"\r")
            time.sleep(0.5)
            phone.write(bytes([26]))
            time.sleep(0.5)
        finally:
            print('Success send')
            print("")
            phone.close()
    except Exception as e:
        print(e)


print("")
print("########################################")
print("#          START SMS CENTER            #")
print("########################################")
print("")

try:
    while True:
        try:
            response = requests.get(http_server)
            parsed_json = response.json()
            for val in parsed_json:
                print('Sent to '+val['recipient'])
                send_sms(str(val['recipient']), str(val['message']))

            time.sleep(2)
        except Exception as e:
            print(e)
            time.sleep(2)
            continue

finally:
    s.close()
