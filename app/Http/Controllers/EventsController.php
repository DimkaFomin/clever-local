<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EventsModerated;
use Auth;

class EventsController extends Controller
{
    public function getModeratedTicket($ticket_id)
    {
        $data = EventsModerated::with(['user', 'course'])
                ->where('id', $ticket_id)->first();

        if ($data->user->id == Auth::id())
        return $data;
    }

    public function getAllModeratedTicketsForUser($page)
    {
        $data = EventsModerated::with(['user', 'course'])
            ->where('user_id', Auth::id())->get();

        if ($data->user->id == Auth::id())
            return $data;
    }

    public function getAllModeratedTicketsForAdmin()
    {

    }
}
