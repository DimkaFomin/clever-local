<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SmsSender;

class SmsSenderController extends Controller
{
    public function index($key){
        $secret = "cleversms";
        if ($key == $secret){
            $newSms = SmsSender::where('is_reed', 0)->select('recipient', 'message')->get();
            $updateSms = SmsSender::where('is_reed', 0)->select('recipient', 'message')->update(['is_reed' => 1]);
            return $newSms;
        }
        return abort(404);
    }
}
