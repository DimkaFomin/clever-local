<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 26.07.2018
 * Time: 10:07
 */

namespace App\Http\Controllers;


use App\Support;
use App\User;
use App\Course;
use App\OtherCourses;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\Search_categories;
use Illuminate\Support\Facades\DB;


$global = array();

class GraphSearchController extends Controller
{


    public function index($id, $page)
    {
        global $global;
        if ($id == 1) {
            $center = Search_categories::where('id', $id)
                ->select('category_name', 'id', 'id_parent')
                ->get();

            //dd($center);
            $child = Search_categories::where('id_parent', $id)
                ->select('category_name', 'id', 'id_parent')
                ->get();

            $clever_courses = Course:: where('is_moderated', '1')
                ->select('id', 'name', 'description')
                ->orderBy('id', 'asc')
                ->get()
                ->map(function ($item){
                    $item['source'] = 'our';
                    return $item;
                });


            $other_courses = OtherCourses::select('id', 'name', 'description')
                ->orderBy('id', 'asc')
                ->get()
                ->map(function ($item){
                    $item['source'] = 'other';
                    return $item;
                });

            $all_count = count(Course::all()) + count(OtherCourses::all());


            $all_courses = $clever_courses->merge($other_courses);
            //dd($all_courses);

            if(($page)*5 > count($all_courses)) $show =0;
            else $show = 1;

            $view = view('inside.graphsearch', [
                'center' => $center,
                'child' => $child,
                'count' => $all_count,
                'courses' => $all_courses->take($page*5),
                'next_page' => $page + 1,
                'element_id' => $id,
                'show_button' => $show,
            ]);
            return $view;
        } else {
            $center = Search_categories::where('id', $id)
                ->select('category_name', 'id', 'id_parent')
                ->get();

            //dd($center);
            $child = Search_categories::where('id_parent', $id)
                ->select('category_name', 'id', 'id_parent')
                ->get();

            $id_parent = $center[0]['id_parent'];

            $parent = Search_categories::where('id', $id_parent)
                ->select('category_name', 'id', 'id_parent')
                ->get();

            $child_categories = Search_categories::where('id_parent', $id)
                ->select('id')
                ->get();

            $global = array();
            foreach ($child_categories as $childs) {
                array_push($global, $childs['id']);
            }
            $this->get_childs($global);
            $all_clever = new \Illuminate\Database\Eloquent\Collection();
            $all_other = new \Illuminate\Database\Eloquent\Collection();

            foreach ($global as $id_cat) {
                $clever_courses = Course::join('clever_course_categories', 'courses.id', '=', 'clever_course_categories.id_course')
                    ->join('categories', 'categories.id', '=', 'clever_course_categories.id_category')
                    ->where('categories.id', $id_cat)
                    ->select('name', 'description', 'courses.id')
                    ->get()
                    ->map(function ($item){
                        $item['source'] = 'our';
                        return $item;
                    });
                $all_clever = $all_clever->merge($clever_courses);
            }
            //dd($all_clever);
            foreach ($global as $id_cat) {
                $other_courses = OtherCourses::join('other_course_categories', 'courses_other.id', '=', 'other_course_categories.id_course')
                    ->join('categories', 'categories.id', '=', 'other_course_categories.id_category')
                    ->where('categories.id', $id_cat)
                    ->select('name', 'description', 'courses_other.id')
                    ->get()
                    ->map(function ($item){
                        $item['source'] = 'other';
                        return $item;
                    });
                $all_other = $all_other->merge($other_courses);
            }

            $all_courses = $all_clever->merge($all_other);
            if(($page)*5 > count($all_courses)) $show =0;
            else $show = 1;


            $view = view('inside.graphsearch', [
                'center' => $center,
                'child' => $child,
                'count' => count($all_courses),
                'courses' => $all_courses->take($page*5),
                'next_page' => $page + 1,
                'element_id' => $id,
                'get_back' => $parent,
                'show_button' => $show,
            ]);

            return $view;
        }

    }

    public function get_childs($array)
    {
        global $global;
        $ar = array();
        for ($i = 0; $i < count($array); $i++) {
            $new_list = Search_categories::where('id_parent', $array[$i])
                ->select('id')
                ->get();
            foreach ($new_list as $new_child)
                array_push($ar, $new_child['id']);
        }
        if (count($ar) != 0) {
            $global = array_merge($global, $ar);
            //dd($global);
            $this->get_childs($ar);
        } else {
            return $global;
        }


    }



    private function referralGenerator($email) {
        return md5($email);
    }
}