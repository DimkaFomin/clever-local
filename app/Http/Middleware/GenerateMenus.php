<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class GenerateMenus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {
        \Menu::make('UserBar', function ($menu) {
            if (Auth::id()) {
                $profile = $menu->add('',     ['url'  => 'id'.Auth::id(),  'id' => 'profile']);
                //$profile = $menu->add('', ['url' => Auth::user()->login,  'id' => 'profile']);
            }
            $people = $menu->add('',     ['route'  => 'people',  'id' => 'people']);
            $messages = $menu->add('',     ['route'  => 'messages',  'id' => 'messages']);
            $shop = $menu->add('',     ['route'  => 'shop_manage',  'id' => 'shop']);
            //$stream = $menu->add('',     ['route'  => 'login',  'id' => 'stream']);
            //$setting = $menu->add('',     ['route'  => 'login',  'id' => 'setting']);
            //$blog = $menu->add('',     ['route'  => 'login',  'id' => 'blog']);
            $search = $menu->add('',     ['route'  => 'catalog',  'id' => 'search']);
            //$file = $menu->add('',     ['route'  => 'login',  'id' => 'file']);

            if (Auth::id()) {
                $profile->prepend('<span class="icon home" data-user_id="'.Auth::id().'"></span> ');
            }
            $people->prepend('<span class="icon people"></span> ');

            $messages->prepend('<span class="icon dialog"></span> ');

            // end counting

            $shop->prepend('<span class="icon course"></span> ');
           // $stream->prepend('<span class="icon stream"></span> ');
           // $setting->prepend('<span class="icon setting"></span> ');
           // $blog->prepend('<span class="icon blog"></span> ');
            $search->prepend('<span class="icon search"></span> ');
           // $file->prepend('<span class="icon file"></span> ');
        });


        \Menu::make('GuestBar', function ($menu) {
            $blog = $menu->add('',     ['route'  => 'login',  'id' => 'blog']);
            $search = $menu->add('',     ['route'  => 'catalog',  'id' => 'search']);

            $blog->prepend('<span class="icon blog"></span> ');
            $search->prepend('<span class="icon search"></span> ');
        });

        return $next($request);
    }
}
