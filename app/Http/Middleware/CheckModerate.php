<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Course;

class CheckModerate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $book = Book::whereid($request->course_id)->first();
        if ($book->id_moderated == 0) {
            if ($book->user_id == Auth::id()) {
                return $next($request);
            }
            if ($book->user_id != Auth::id() && !Auth::guest()) {
                return redirect('/login');
            }
            if (Auth::guest()) {
                echo '<script type="text/javascript">alert("3!");</script>';
                return redirect('/catalog');
            }
        }
        return $next($request);


    }


}
