<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 06.09.2018
 * Time: 11:55
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Search_categories extends Model
{
    protected $table = "categories";

    public $timestamps = false;

    public function course(){
        return $this->belongsToMany('App\Course', 'clever_course_categories', 'id_category', 'id_course');
    }
    public function othercourse(){
        return $this->belongsToMany('App\OtherCourse', 'other_course_categories', 'id_category', 'id_course');
    }


}