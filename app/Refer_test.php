<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 26.07.2018
 * Time: 10:05
 */

namespace App;



use Illuminate\Database\Eloquent\Model;

class Refer_test extends Model
{
    protected $table = 'refer';

    public function user()
    {
        return $this->belongsTo('App\User')->select('id', 'first', 'second', 'login');
    }

}