<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OtherCourses extends Model
{
    protected $table = 'courses_other';
    public $timestamps = false;

    public function photo()
    {
        return $this->belongsTo('App\Photo');
    }

    public function categories(){
        return $this->belongsToMany('App\Search_categories', 'other_course_categories', 'id_course', 'id_category');
    }
}
