<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Materials extends Model
{
   protected $table = "course_materials";

    public function mark()
    {
        return $this->hasMany('App\Mark', 'material_id', 'id');
    }
}
