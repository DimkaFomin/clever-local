<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class XhrCourse extends Model
{
    protected $table = 'xhr_check';

    public function course()
    {
        return $this->belongsTo('App\Course');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
