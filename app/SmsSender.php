<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SmsSender extends Model
{
    protected $table = 'sms_sender';
}
