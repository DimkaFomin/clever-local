<?php

use Faker\Generator as Faker;

$factory->define(App\Book::class, function (Faker $faker) {
    return [
        'name'=> $faker->sentence(rand(2, 6), true),
        'discription' => $faker->text(rand(200, 1000)),
        'category_id' => rand(1, 30),
        'vektor_id' => rand(1, 30),
        'difficult_id' => rand(1, 10),
        'learntime' => rand(60, 240),
        'price' => rand(500, 20000),
        'photo_id' => rand(1, 10000),
        'is_moderated' => rand(0, 1),
        'user_id' => rand(1, 50),
        'lang_id' => rand(1, 5),
        'full_description' => $faker->paragraphs(rand(2, 10), true),
        'tags' => $faker->words(rand(5, 10), true),
        'skills' => $faker->sentence(rand(20, 100), true),
        'program' => $faker->sentences(rand(2, 10), true),
        'author' => $faker->name,
        'author_link' => $faker->safeEmailDomain,
        'material_link' => $faker->safeEmailDomain
    ];
});
