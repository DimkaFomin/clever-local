<?php

use Faker\Generator as Faker;

$factory->define(App\User::class, function (Faker $faker) {
    static $password;

    return [
        'first' => $faker->name,
        'second' => $faker->name,
        'third' => $faker->name,
        'login' => $faker->unique()->safeEmail,
        'phone' => rand(1000000, 99999999),
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});
