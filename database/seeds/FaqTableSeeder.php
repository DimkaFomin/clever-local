<?php

use Illuminate\Database\Seeder;

class FaqTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('faq')->insert(['page_id' => 1,  'quest' => 'Как зарегистрироваться на бирже?', 'answer' => 'Вы можете зарегистрироваться на бирже по <a href = "/register">ссылке: </a>']);
    }
}
