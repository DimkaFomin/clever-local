<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statement = "ALTER TABLE users AUTO_INCREMENT = 1000068;";
        DB::unprepared($statement);

        DB::table('users')->insert([
            'login' => 'ihman111@mail.ru',
            'password' =>  '10157881aacbdffa39e15cf8c76b8fde91a2a9e173343b820ef7f63943e270dc',
            'first' => 'Владислав',
            'second' => 'Тэн',
            'sex' => '1',
            'photo_id' => '2',
            'is_admin' => '1',
            'allow_stream' => '1',
            'active' => '1',
            'confirmation_code' => 'aWhtYW4xMTFAbWFpbC5ydQ==',
        ]);

        DB::table('users')->insert([
            'login' => 'ingvar77-1@yandex.ru',
            'password' =>  '0d81684688d4057da4d9f6df64b28154b68afc2f1946a756302613c92fdd4986',
            'first' => 'Игорь',
            'second' => 'Алексеев',
            'third' => 'Вадимович',
            'sex' => '1',
            'photo_id' => '2',
            'is_admin' => '1',
            'allow_stream' => '1',
            'active' => '1',
        ]);

        DB::table('users')->insert([
            'login' => 'vavilova@uniyar.ac.ru',
            'password' =>  '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92',
            'first' => 'Екатерина',
            'second' => 'Вавилова',
            'sex' => '0',
            'photo_id' => '2',
            'is_admin' => '1',
            'allow_stream' => '1',
            'active' => '1',
        ]);

        DB::table('users')->insert([
            'login' => 'marina@uniyar.ac.ru',
            'password' =>  '8d3e0bf685d077784de23e1c217de5c5d8da4c0200d7c86df6ff607d9cbc6959',
            'first' => '???',
            'second' => '???',
            'allow_stream' => '1',
            'active' => '1',
        ]);

        DB::table('users')->insert([
            'login' => 'marinafon@yandex.ru',
            'password' =>  '8d3e0bf685d077784de23e1c217de5c5d8da4c0200d7c86df6ff607d9cbc6959',
            'first' => 'Марина',
            'second' => 'Борисова',
            'third' => 'Владимировна',
            'sex' => '0',
            'photo_id' => '2',
            'allow_stream' => '1',
            'active' => '1',
            'phone' => '797726',
            'skype' => 'marinafon2',
        ]);

        DB::table('users')->insert([
            'login' => 'mary@yars.free.net',
            'password' =>  'd6ca1483c5d0ad8036a9d44b4a7f064116f1787f48f53d3747bf377b83a85a32',
            'first' => 'Марина',
            'second' => 'Захарова',
            'third' => 'Николаевна',
            'sex' => '0',
            'photo_id' => '3',
            'allow_stream' => '1',
            'active' => '1',
        ]);

        DB::table('users')->insert([
            'login' => '79092770375@ya.ru',
            'password' =>  '6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b',
            'first' => 'Алексей',
            'second' => 'Смахтин',
            'allow_stream' => '1',
            'active' => '1',
        ]);

        DB::table('users')->insert([
            'login' => 'master@clever-e.com',
            'password' =>  'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3',
            'first' => 'Rafael',
            'second' => 'Yaikov',
            'photo_id' => '1',
            'allow_stream' => '1',
            'active' => '1',
        ]);

        DB::table('users')->insert([
            'login' => 'man@a-real.ru',
            'password' =>  '9c09023eed98cd9dda54c7bcc3ee054ba5af2179b77e1b1482a032569c3d5e62',
            'first' => '???',
            'second' => '???',
            'allow_stream' => '1',
            'active' => '1',
        ]);

        DB::table('users')->insert([
            'login' => 'mishin.tezra@gmail.com',
            'password' =>  '0af5c3ba790eb08ea3edc32e70aa90a37b3359f696cc8c5b1e280ae4ff59b008',
            'first' => 'Тирион',
            'second' => 'Ланистер',
            'third' => 'Тайвинович',
            'photo_id' => '2',
            'allow_stream' => '1',
            'active' => '1',
        ]);

        DB::table('users')->insert([
            'login' => 'exmarrow@mail.ru',
            'password' =>  '5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5',
            'first' => 'Екатерина',
            'second' => 'Александрова',
            'third' => 'Викторовна',
            'photo_id' => '2',
            'allow_stream' => '1',
            'active' => '1',
            'phone' => '79159760677',
        ]);

        DB::table('users')->insert([
            'login' => 'info@it-park.yar.ru',
            'password' =>  '0e73b0620dbd859a47b2b08f88fc05e0093a2c80454a3185cf408cb37ef73c0c',
            'first' => 'Образовательный центр',
            'second' => '"IT park"',
            'photo_id' => '2',
            'allow_stream' => '1',
            'active' => '1',
            'phone' => '74852797726',
            'site' => 'http://it-park.yar.ru/',
        ]);

        DB::table('users')->insert([
            'login' => 'hitman85@rambler.ru',
            'password' =>  '5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5',
            'first' => '???',
            'second' => '???',
            'allow_stream' => '1',
            'active' => '1',
        ]);

        DB::table('users')->insert([
            'login' => 'lapatria@inbox.ru',
            'password' =>  'ba723435a66e490530c3efdfeac868e06fde6e35dcc43fa8528fb1b2c9411ef5',
            'first' => '???',
            'second' => '???',
            'allow_stream' => '1',
            'active' => '1',
        ]);

        DB::table('users')->insert([
            'login' => 'stud001@clever-e.com',
            'password' =>  'ba723435a66e490530c3efdfeac868e06fde6e35dcc43fa8528fb1b2c9411ef5',
            'first' => '???',
            'second' => '???',
            'allow_stream' => '0',
            'active' => '1',
        ]);

        DB::table('users')->insert([
            'login' => 'stud002@clever-e.com',
            'password' =>  '05239edebe2252a2bbc0cb7a05a15c8d1fa107b13f51540a3c61cd6f0ce4ac44',
            'first' => 'Александр',
            'second' => 'Давыдов',
            'allow_stream' => '0',
            'active' => '1',
            'sex' => 1,
        ]);

         DB::table('users')->insert([
             'login' => 'stud003@clever-e.com',
             'password' =>  'a79a739036fa06af30a63ed9e40c728fa841e8becf6b91a665c06f6ae0ca2ba9',
             'first' => 'Александр',
             'second' => 'Шубин',
             'third' => 'Владимирович',
             'allow_stream' => '0',
             'active' => '1',
             'sex' => 1,
             'phone'=> '79201393215',
         ]);

        DB::table('users')->insert([
            'login' => 'stud004@clever-e.com',
            'password' =>  '5c5f425f26ecf9d9eaffe43ad88ea429bca155b8779c03239f0c0c96cf55d3d6',
            'first' => 'Алексей',
            'second' => 'Иванов',
            'third' => 'Александрович',
            'allow_stream' => '0',
            'active' => '1',
            'sex' => 1,
        ]);

        DB::table('users')->insert([
            'login' => 'stud005@clever-e.com',
            'password' =>  'e2438650d569ba0c5c60d5e23dfef7c9164b2305d2f2d5faa8c70b4f60dcd109',
            'first' => 'Яна',
            'second' => 'Батырева',
            'third' => 'Валериевна',
            'allow_stream' => '0',
            'active' => '1',
            'sex' => 0,
            'phone'=> '79807472350',
        ]);

        DB::table('users')->insert([
            'login' => 'stud006@clever-e.com',
            'password' =>  '609e502e4010a57d21c55eade4a18660b7d73311920e4f97ec9b7aab15b39429',
            'first' => 'Анна',
            'second' => 'Ворникова',
            'third' => 'Ивановна',
            'allow_stream' => '0',
            'active' => '1',
            'sex' => 0,
            'phone'=> '79201000403',
        ]);

        DB::table('users')->insert([
            'login' => 'stud007@clever-e.com',
            'password' =>  '7eef790f131657d503d9dfeba7480118b6aea770c2fd5a5e79cb0873f8e3d9b9',
            'first' => 'Елизавета',
            'second' => 'Ершова',
            'third' => 'Сергеевна',
            'allow_stream' => '0',
            'active' => '1',
            'sex' => 0,
            'phone'=> '79301171872',
        ]);

        DB::table('users')->insert([
            'login' => 'stud008@clever-e.com',
            'password' =>  '1d1354923d4fdb72c87fcc33bea92dcfd31f859e8aa419cc38fd619bd4289e36',
            'first' => 'Ольга',
            'second' => 'Коряковцева',
            'third' => 'Владимировна',
            'allow_stream' => '0',
            'active' => '1',
            'sex' => 0,
            'phone'=> '79201423065',
        ]);

        DB::table('users')->insert([
            'login' => 'stud009@clever-e.com',
            'password' =>  '1a2ffcbac552af5b1b00ac864798421f9f5536f3f291b2dcf680d4a14a93b360',
            'first' => 'Денис',
            'second' => 'Платов',
            'third' => 'Евгеньевич',
            'allow_stream' => '0',
            'active' => '1',
            'sex' => 1,
            'phone'=> '79206503175',
        ]);

        DB::table('users')->insert([
            'login' => 'stud010@clever-e.com',
            'password' =>  '54007241b1666e984023d8221cfac690844b095b505e2577b220d632ca496f7e',
            'first' => 'Анастасия',
            'second' => 'Славгородская',
            'third' => 'Сергеевна',
            'allow_stream' => '0',
            'active' => '1',
            'sex' => 0,
            'phone'=> '79807494335',
        ]);

        DB::table('users')->insert([
            'login' => 'stud011@clever-e.com',
            'password' =>  'ec52858c10476941c9b944ed8466290ada3f1bc83ff907ec50c1ef9073c1e3e9',
            'first' => 'Елена',
            'second' => 'Косимова',
            'third' => 'Николаевна',
            'allow_stream' => '0',
            'active' => '1',
            'sex' => 0,
        ]);

        DB::table('users')->insert([
            'login' => 'stud012@clever-e.com',
            'password' =>  'b862b732545f57434236791756cc40b494489061c9f82dad06ed168f3921201a',
            'first' => 'Владимир',
            'second' => 'Сорокин',
            'third' => 'Игоревич',
            'allow_stream' => '0',
            'active' => '1',
            'sex' => 1,
            'phone'=> '79301127387',
        ]);

        DB::table('users')->insert([
            'login' => 'stud013@clever-e.com',
            'password' =>  'e01f40855cd01791ecb86351c6338f4530a18cb7a1d98c5cf6a1dc3ab00fbba4',
            'first' => 'Татьяна',
            'second' => 'Христолюбова',
            'third' => 'Алексеевна',
            'allow_stream' => '0',
            'active' => '1',
            'sex' => 0,
            'phone' => '79051336733',
        ]);

        DB::table('users')->insert([
            'login' => 'stud014@clever-e.com',
            'password' =>  '77dbe6bcc0da2ddf6295bec575ffa19a156f1acf2c54af68d5ac16ad4784b06a',
            'first' => 'Светлана',
            'second' => 'Багрова',
            'third' => 'Алексеевна',
            'allow_stream' => '0',
            'active' => '1',
            'sex' => 0,
        ]);

        DB::table('users')->insert([
            'login' => 'stud015@clever-e.com',
            'password' =>  '47c805556f9b08ce4f0efbdd58ef11440b6711978caf739ebaaac3c05c5de2af',
            'first' => 'Ксения',
            'second' => 'Хмелева',
            'third' => 'Александровна',
            'allow_stream' => '0',
            'active' => '1',
            'sex' => 0,
        ]);

        DB::table('users')->insert([
            'login' => 'stud016@clever-e.com',
            'password' =>  '9d3f12324eb6600122417420ce1c91dc72dd2e0f2040da138d4f839d05cc1237',
            'first' => 'Мария',
            'second' => 'Мария',
            'third' => 'Александровна',
            'allow_stream' => '0',
            'active' => '1',
            'sex' => 0,
            'phone'=> '79109697480',
        ]);

        DB::table('users')->insert([
            'login' => 'stud017@clever-e.com',
            'password' =>  'c880392d7caca7d8ba61d4a6bdc5d1cc4bfcb483fc5fe8103c4d455a248fc93f',
            'first' => 'Екатерина',
            'second' => 'Яблокова',
            'third' => 'Викторовна',
            'allow_stream' => '0',
            'active' => '1',
            'sex' => 0,
            'phone'=> '79201143355',
        ]);

        DB::table('users')->insert([
            'login' => 'stud018@clever-e.com',
            'password' =>  '817f674e70abdc911551797dc8a245f7111eadab4257c925868f6583fcae0e86',
            'first' => 'Ольга',
            'second' => 'Гущина',
            'allow_stream' => '0',
            'active' => '1',
            'sex' => 0,
        ]);

        DB::table('users')->insert([
            'login' => 'stud019@clever-e.com',
            'password' =>  '7ca860dd3d9fc1efd3c8aa563b55e9cc25ccf69da30a7c0ef58dffc79d4238a3',
            'first' => 'Ольга',
            'second' => 'Яблокова',
            'third' => 'Викторовна',
            'allow_stream' => '0',
            'active' => '1',
            'sex' => 0
        ]);

        DB::table('users')->insert([
            'login' => 'stud020@clever-e.com',
            'password' =>  '1926849b4cb24e7858d43d726ea7dc1e8c4023982c156937d2cf8b2a313bc291',
            'first' => 'Роман',
            'second' => 'Артемьев',
            'third' => 'Сергеевич',
            'allow_stream' => '0',
            'active' => '1',
            'sex' => 1,
            'phone'=> '79807459808',
        ]);

        DB::table('users')->insert([
            'login' => 'stud021@clever-e.com',
            'password' =>  '660981ff3e41eabbd5e652146ff4a4ab722abda12677076b95e539a5ed88cd96',
            'first' => 'Руслан',
            'second' => 'Исмаилов',
            'allow_stream' => '0',
            'active' => '1',
            'sex' => 1,
        ]);

        DB::table('users')->insert([
            'login' => 'stud022@clever-e.com',
            'password' =>  '35e49fffce57256e17d2d0b9486726b1b176afd838adf02f1713814c278fc1a9',
            'first' => 'Виктор',
            'second' => 'Горшков',
            'third' => 'Павлович',
            'allow_stream' => '0',
            'active' => '1',
            'sex' => 1,
        ]);
        DB::table('users')->insert([
            'login' => 'stud023@clever-e.com',
            'password' =>  '3f8c859fa53762c335ae1135c4773c8177df8ced4fba3d5909bf4ee1b87784f1',
            'first' => 'Сергей',
            'second' => 'Антоничев',
            'third' => 'Александрович',
            'allow_stream' => '0',
            'active' => '1',
            'sex' => 1,
            'phone'=> '79159820645',
        ]);

        DB::table('users')->insert([
            'login' => 'stud024@clever-e.com',
            'password' =>  '8ca9681f7e153064b508de0f344add5a5cf2926481f23c62c0015e539832d424',
            'first' => 'Василий',
            'second' => 'Горшенин',
            'third' => 'Александрович',
            'allow_stream' => '0',
            'active' => '1',
            'sex' => 1,
        ]);

        DB::table('users')->insert([
            'login' => 'stud025@clever-e.com',
            'password' =>  '3f8c859fa53762c335ae1135c4773c8177df8ced4fba3d5909bf4ee1b87784f1',
            'first' => 'Илья',
            'second' => 'Гусев',
            'third' => 'Владимирович',
            'allow_stream' => '0',
            'active' => '1',
            'sex' => 1,
            'phone'=> '79056371070',
        ]);

        DB::table('users')->insert([
            'login' => 'stud026@clever-e.com',
            'password' =>  '5721458a04031d340369078b71d02fd36d0c608cdd17e56cf482594f4f592e0d',
            'first' => 'Анастасия',
            'second' => 'Баринова',
            'third' => 'Александровна',
            'allow_stream' => '0',
            'active' => '1',
            'sex' => 0,
            'phone'=> '79301192257',
        ]);

        DB::table('users')->insert([
            'login' => 'stud027@clever-e.com',
            'password' =>  'cb8936d2c9eade078162aa2df73c0ebb0999ca6c5743094ab016ffc31ee3d5e4',
            'first' => 'Ирина',
            'second' => 'Яблокова',
            'third' => 'Викторовна',
            'allow_stream' => '0',
            'active' => '1',
            'sex' => 0,
        ]);

        DB::table('users')->insert([
            'login' => 'stud028@clever-e.com',
            'password' =>  '5f38d2df1c99939c23a8bf0f566c81dc2c059805cbb50a65e62d44c4c9dc228e',
            'first' => 'Ольга',
            'second' => 'Трубникова',
            'third' => 'Анатольевна',
            'allow_stream' => '0',
            'active' => '1',
            'sex' => 0,
        ]);

        DB::table('users')->insert([
            'login' => 'stud029@clever-e.com',
            'password' =>  'f7875c92ec2f0e201578ceb288306d61c53a85a4947e94f71eff2976e7a7a494',
            'first' => 'Евгений',
            'second' => 'Фасхутдинов',
            'third' => 'Исмаилович',
            'allow_stream' => '0',
            'active' => '1',
            'sex' => 1,
            'phone'=> '79109616703',
        ]);

        DB::table('users')->insert([
            'login' => 'stud030@clever-e.com',
            'password' =>  'ba1e0b58b971368533b87c3756b43520e39e69eaea1ebc1cb8f576b344918d06',
            'first' => 'Алексей',
            'second' => 'Смирнов',
            'third' => 'Владимирович',
            'allow_stream' => '0',
            'active' => '1',
            'sex' => 1,
        ]);

        DB::table('users')->insert([
            'login' => 'teach001@clever-e.com',
            'password' =>  '97249142d78d62209d4a0e8e1b2ce1be26f86f2b44780649473d68e55e4f7ec7',
            'first' => '???',
            'second' => '???',
            'allow_stream' => '0',
            'active' => '1',
        ]);

        DB::table('users')->insert([
            'login' => 'teach002@clever-e.com',
            'password' =>  'f66636f53f3cd96e7060476bdb431b36f6c0dace77ff086ec0d6302713f8d4b8',
            'first' => 'Елена',
            'second' => 'Шалыгина',
            'third' => 'Евгеньевна',
            'allow_stream' => '0',
            'active' => '1',
            'sex' => 0,
        ]);

        DB::table('users')->insert([
            'login' => 'teach003@clever-e.com',
            'password' =>  'b14ed1c39be790dfedf62b12c10050f2b2b546e70fb452b30d82ebb4a79882ed',
            'first' => 'Сергей',
            'second' => 'Панфилов',
            'allow_stream' => '0',
            'active' => '1',
            'sex' => 1,
        ]);

        DB::table('users')->insert([
            'login' => 'teach004test@clever-e.com',
            'password' =>  '22bd3cfe624666429af951d7e5edeaf9cd5e097913ce7a4e939649a0e38541be',
            'first' => '???',
            'second' => '???',
            'allow_stream' => '0',
            'active' => '1',
        ]);

        DB::table('users')->insert([
            'login' => 'teach005test@clever-e.com',
            'password' =>  '4d844691955dd2564b06be9e2a730aa026982c0c929078c7458e367ada1654f9',
            'first' => 'Тестер',
            'second' => '???',
            'allow_stream' => '0',
            'active' => '1',
            'sex' => 1,
        ]);

        DB::table('users')->insert([
            'login' => 'mishin911@gmail.com',
            'password' =>  '32eeebd1480d5e8bb6b10bbc37d2294a130f962b439295a09bf9a752f2fca0f5',
            'first' => '???',
            'second' => '???',
            'allow_stream' => '1',
            'active' => '1',
        ]);

        DB::table('users')->insert([
            'login' => 'mishin@uniyar.ac.ru',
            'password' =>  '7a35a2e068d038c41d79f45a4f694f719f62fd6415fb733007e6cd9eecb3de4d',
            'first' => '???',
            'second' => '???',
            'allow_stream' => '1',
            'active' => '1',
        ]);

        DB::table('users')->insert([
            'login' => 'aseev79@uniyar.ac.ru',
            'password' =>  'c542798aeb280b1257e387d011092366a2ece79f0af5287c3cffcadbe83d2952',
            'first' => '???',
            'second' => '???',
            'allow_stream' => '1',
            'active' => '1',
        ]);

        DB::table('users')->insert([
            'login' => 'yournatalita@gmail.com',
            'password' =>  '64d27345da86f9c656767cffa1874f6aff2da213b7de10216bd21d4a945278ed',
            'first' => 'Наталья',
            'second' => 'Егорова',
            'third' => 'Алексеевна',
            'allow_stream' => '1',
            'active' => '1',
            'sex' => 0,
        ]);

        DB::table('users')->insert([
            'login' => 'aiv@yars.free.net',
            'password' =>  '0d81684688d4057da4d9f6df64b28154b68afc2f1946a756302613c92fdd4986',
            'first' => 'Игорь, но другой',
            'second' => 'Тоже Алексеев',
            'third' => 'для проверки',
            'allow_stream' => '1',
            'active' => '1',
            'sex' => 1,
        ]);

        DB::table('users')->insert([
            'login' => 'clever_main@clever-e.com',
            'password' =>  'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3',
            'first' => 'Blog',
            'second' => 'CLEVER-E',
            'third' => 'Александрович',
            'allow_stream' => '1',
            'photo_id' => '1',
            'active' => '1',
            'sex' => 1,
        ]);
    }
}
