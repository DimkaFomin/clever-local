<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('courses', function (Blueprint $table) {
            $table->string('discription', 1023)->nullable()->change();
            $table->integer('category_id')->nullable()->change();
            $table->integer('vector_id')->nullable()->change();
            $table->integer('difficult_id')->nullable()->change();
            $table->integer('learntime')->nullable()->change();
            $table->integer('photo_id')->nullable()->change();
            $table->integer('is_moderated')->default(0)->change();
            $table->integer('lang_id')->nullable()->change();
            $table->text('full_description')->nullable()->change();
            $table->text('tags')->nullable()->change();
            $table->text('skills')->nullable()->change();
            $table->text('program')->nullable()->change();
            $table->string('author', 255)->nullable()->change();
            $table->string('author_link', 255)->nullable()->change();
            $table->string('material_link', 255)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('courses', function (Blueprint $table) {
        $table->string('discription', 1023)->change();
        $table->integer('category_id')->change();
        $table->integer('vector_id')->change();
        $table->integer('difficult_id')->change();
        $table->integer('learntime')->change();
        $table->integer('photo_id')->change();
        $table->integer('is_moderated')->change();
        $table->integer('lang_id')->change();
        $table->text('full_description')->change();
        $table->text('tags')->change();
        $table->text('skills')->change();
        $table->text('program')->change();
        $table->string('author', 255)->change();
        $table->string('author_link', 255)->change();
        $table->string('material_link', 255)->change();
        });
    }
}
