<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsModeratedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events_moderated', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('accept')->default(0); //1 - accepted //2 - rejected
            $table->integer('course_id');
            $table->string('comment', 1023)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events_moderated');
    }
}
