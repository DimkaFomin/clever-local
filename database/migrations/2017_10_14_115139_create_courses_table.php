<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 511);
            $table->text('discription');
            $table->integer('category_id');
            $table->integer('vektor_id');
            $table->integer('difficult_id');
            $table->integer('learntime');
            $table->integer('price');
            $table->integer('photo_id');
            $table->integer('is_moderated');
            $table->integer('user_id');
            $table->integer('lang_id');
            $table->text('full_description');
            $table->text('tags');
            $table->text('skills');
            $table->text('program');
            $table->string('author', 255);
            $table->string('author_link', 255);
            $table->string('material_link', 255);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
