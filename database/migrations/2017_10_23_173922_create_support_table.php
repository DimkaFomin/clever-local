<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('support', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255)->nullable();
            $table->string('email', 255)->nullable();
            $table->string('text', 2047);
            $table->integer('user_id')->default(0);
            $table->string('answer', 2047)->nullable();
            $table->integer('first_msg_id')->default(0);
            $table->integer('admin_id')->default(0);
            $table->integer('question_status')->default(0); //0 - new, 1 - admin answer exist
            $table->string('md5', 32);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('support');
    }
}
