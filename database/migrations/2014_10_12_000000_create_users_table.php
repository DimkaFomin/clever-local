<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('users')) {
            Schema::create('users', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('login',255)->unique();
                $table->string('password',255);
                $table->string('first',255);
                $table->string('second',255);
                $table->string('third',255)->nullable();
                $table->date('birthday',255)->nullable();
                $table->integer('sex')->nullable();
                $table->integer('photo_id')->nullable();
                $table->integer('country_id')->nullable();
                $table->integer('city_id')->nullable();
                $table->integer('work_place_id')->nullable();
                $table->integer('job_id')->nullable();
                $table->integer('edu_place_id')->nullable();
                $table->integer('edu_grade_id')->nullable();
                $table->string('phone',255)->nullable()->unique();
                $table->string('skype',255)->nullable()->unique();
                $table->string('vk',255)->nullable()->unique();
                $table->string('fb',255)->nullable()->unique();
                $table->string('site',255)->nullable();
                $table->integer('is_admin')->nullable();
                $table->integer('allow_stream')->nullable();
                $table->integer('active')->default(0);
                $table->string('confirmation_code')->nullable();
                $table->rememberToken();
                $table->timestamps();

                $table->index('photo_id');
                $table->index('country_id');
                $table->index('city_id');
                $table->index('work_place_id');
                $table->index('job_id');
                $table->index('edu_place_id');
                $table->index('edu_grade_id');
            });
        }

        if (!Schema::hasColumn('users', 'remember_token')) {
            Schema::table('users', function (Blueprint $table) {
                $table->rememberToken();
            });
        }
        if (!Schema::hasColumn('users', 'created_at')) {
            Schema::table('users', function (Blueprint $table) {
                $table->timestamps();
            });
        }
        if (!Schema::hasColumn('users', 'confirmation_code')) {
            Schema::table('users', function (Blueprint $table) {
                $table->confirmation_code()->nullable();
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
