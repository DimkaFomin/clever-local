/* left slide bar slide mobile */
var slideout = new Slideout({
    'panel': document.getElementById('panel'),
    'menu': document.getElementById('menu'),
    'padding': 81,
    'tolerance': 70
});

var fixed = document.querySelector('.navbar-fixed-top');

slideout.on('translate', function(translated) {
    fixed.style.transform = 'translateX(' + translated + 'px)';
});

slideout.on('beforeopen', function () {
    fixed.style.transition = 'transform 300ms ease';
    fixed.style.transform = 'translateX(81px)';
});

slideout.on('beforeclose', function () {
    fixed.style.transition = 'transform 300ms ease';
    fixed.style.transform = 'translateX(0px)';
});

slideout.on('open', function () {
    fixed.style.transition = '';
});

slideout.on('close', function () {
    fixed.style.transition = '';
});
/* end left slide bar */

/*ajax to render page*/

    /* render register */
    $('body').on('click', '.register', function (e) {
        e.preventDefault();
        var link = $(this).attr('href');
        $.get(link, function(response){
             $('#panel').html(response.content);
            history.pushState(null, null, link);
            document.title = response.title;

        });
    });
    /* end redner register*/

    /* render login */
    $('body').on('click', '.login', function (e) {
        e.preventDefault();
        var link = $(this).attr('href');
        $.get(link, function(response){
           $('#panel').html(response.content);
            history.pushState(null, null, link);
            document.title = response.title;
        });
    });
    /* end redner login*/

    /* render verify send */
    $('body').on('submit', '#register-form', function (e) {
        e.preventDefault();

        var link = $(this).attr('action');
        var _token = $("input[name='_token']").val();
        var first = $("#first").val();
        var second = $("#second").val();
        var login = $("#login").val();
        var password = $("#password").val();
        var password_confirmation = $("#password-confirm").val();

        $.post(link, {_token:_token, first:first, second:second, login:login, password:password, password_confirmation:password_confirmation, remember:'on'},function(response){
           $('#panel').html(response.content);;
            history.pushState(null, null, link);
        });
    });
    /* end verify send*/

    /* render login success */
    // $('body').on('submit', '#login-form', function (e) {
    //     e.preventDefault();
    //     var link = $(this).attr('action');
    //
    //     var _token = $("input[name='_token']").val();
    //     var login = $("#login").val();
    //     var password = $("#password").val();
    //
    //     $.post(link, {_token:_token, login:login, password:password},function(response){
    //        $('#panel').html(response.content);;
    //         history.pushState(null, null, link);
    //     });
    // });
    /* end login success */

    /* render userpage */
    $('body').on('click', '#profile_head a', function (e) {
        e.preventDefault();
        var link = $(this).attr('href');
        $.get(link, function(response){
           $('#panel').html(response.content);;
            history.pushState(null, null, link);
            document.title = response.title;
            upload_avatar();
        });
    });
    $('body').on('click', '#profile a', function (e) {
        e.preventDefault();
        var link = $(this).attr('href');
        $.get(link, function(response){
           $('#panel').html(response.content);;
            history.pushState(null, null, link);
            document.title = response.title;
            upload_avatar();
        });
    });

    $('body').on('click', '.followbox a', function (e) {
        e.preventDefault();
        var link = $(this).attr('href');
        $.get(link, function(response){
           $('#panel').html(response.content);;
            history.pushState(null, null, link);
            document.title = response.title;
            upload_avatar();
        });
    });

    $('body').on('click', '#cancel_course', function (e) {
        e.preventDefault();
        var link = $(this).attr('action');
        $.get(link, function(response){
           $('#panel').html(response.content);;
            history.pushState(null, null, link);
            document.title = response.title;
            upload_avatar();
        });
    });

    $('body').on('click', '.close-container a', function (e) {
        e.preventDefault();
        $('.popover').remove();
        var link = $(this).attr('href');
        $.get(link, function(response){
            $('#panel').html(response.content);
            history.pushState(null, null, link);
            document.title = response.title;
            upload_avatar();
            MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
        });
    });

    $('body').on('click', '.profile a', function (e) {
        e.preventDefault();
        var link = $(this).attr('href');
        $.get(link, function(response){
            $('#panel').html(response.content);;
            history.pushState(null, null, link);
            document.title = response.title;
            upload_avatar();
        });
    });
    /* end redner userpage*/

    /* render user setting */
    $('body').on('click', '#profile_setting a', function (e) {
        e.preventDefault();
        var link = $(this).attr('href');
        $.get(link, function(response){
           $('#panel').html(response.content);;
            history.pushState(null, null, link);
            document.title = response.title;
        });
    });
    /* config validate form */
    $('body').on('submit', '#config-user-form', function (e) {
        e.preventDefault();

        var link = $(this).attr('action');
        var _token = $("input[name='_token']").val();

        var third = $("input[name='third_name']").val();
        var sex = $("select[name='sex_name'] option:selected").data('id');
        var birthday = $("input[name='birthday_name']").val();
        var country = $("select[name='country_name'] option:selected").data('id');
        var region = $("select[name='region_name'] option:selected").data('id');
        var city = $("select[name='city_name'] option:selected").data('id');
        var work = $("input[name='work_place_name']").val();
        var job = $("input[name='job_name']").val();
        var phone = $("input[name='phone_name']").val();
        var skype = $("input[name='skype_name']").val();
        var vk = $("input[name='vk_name']").val();
        var fb = $("input[name='fb_name']").val();
        var site = $("input[name='site_name']").val();

        if (third == "") third = undefined;
        if (sex == 0) sex = undefined;
        if (birthday == "") birthday = undefined;
        if (country == 0) country = undefined;
        if (region == 0) region = undefined;
        if (city == 0) city = undefined;
        if (work == "") work = undefined;
        if (job == "") job = undefined;
        if (phone == "") phone = undefined;
        if (skype == "") skype = undefined;
        if (vk == "") vk = undefined;
        if (fb == "") fb = undefined;
        if (site == "") site = undefined;

        $.post(link, {
            _token: _token,
            third: third,
            sex: sex,
            birthday: birthday,
            country: country,
            region: region,
            city: city,
            work: work,
            job: job,
            phone: phone,
            skype: skype,
            vk: vk,
            fb: fb,
            site: site
        }, function (response) {
            alert("Данные успешно сохранены");
            //$('#panel').html(response.content);;
            // history.pushState(null, null, link);
        });


    });
    /* end validate form */

    /* follow unfollow button */

    /* end follow unfollow button */
    $('body').on('click', '.follow-btn .follow', function (e) {
        e.preventDefault();

        var link = $(this).attr('action');
        var _token = $("input[name='_token']").val();

        var user_id = $('.follow-btn').data('id');
        var auth_id = $('.follow-btn').data('auth');
        var photo_id = $('.follow-btn').data('img');
        var name = $('.follow-btn').data('name');
        var photo_path = $('.user-logo').attr('src').replace('icon/','');
        var elem =$(this);
        $.post(link, {
            _token: _token,
            user_id: user_id
        }, function (response) {
            var content = '<button action="/id'+user_id+'/unfollow" type="button" class="btn-default btn btn-left btn-block unfollow"><b>Отписаться</b></button>';
            elem.parent().append(content);
            content = '<div class="col-md-12 col-sm-12 col-xs-12  box followbox">' +
                        '<div class="col-md-12">' +
                            '<h2>Подписчики <span class="fcount">1</span></h2>' +
                            '</div>' +
                        '<div class="col-md-12">' +
                            '<ul><li>' +
                                '<a href="/id'+auth_id+'">';
            if (photo_id > 0) {
                content += '<div class = "user-avatar-sm">' +
                    '<img class="user-avatar-pic" src="'+photo_path+'">' +
                '</div>';
            }
            else {
                content += '<div class = "user-no-avatar-sm">' +
                    '<img class="user-avatar-pic" src="/img/no_avatar_small.png">' +
                    '</div>';
            }
            content += '<p>'+name+'</p> </a></li></ul></div></div>';
            elem.parent().parent().parent().append(content);
            elem.remove();
            //$('#panel').html(response.content);;
            // history.pushState(null, null, link);
        });


    });

    $('body').on('click', '.follow-btn .unfollow', function (e) {
        e.preventDefault();

        var link = $(this).attr('action');
        var _token = $("input[name='_token']").val();

        var user_id = $(this).data('id');
        var elem = $(this);

        $.post(link, {
            _token: _token,
            user_id: user_id
        }, function (response) {
            var content = '<button action="/id'+user_id+'/follow" type="button" class="btn-success btn btn-left btn-block follow" <b>Подписаться</b></button>';
            elem.parent().append(content);
            elem.remove();
            $('.followbox').remove();
            //$('#panel').html(response);
            //history.pushState(null, null, link);
        });


    });
    /* end render user setting*/

    /* render searchpage */
    $('body').on('click', '#search a', function (e) {
        e.preventDefault();
        var link = $(this).attr('href');
        $.get(link, function(response){
           $('#panel').html(response.content);;
            history.pushState(null, null, link);
            document.title = response.title;
            $('.selectpicker').selectpicker({
                size: 7
            });
            //$.getScript( "lib/selectpicker/bootstrap-select.min.js" )
        });
    });
    /* end redner searchpage*/

    /* render create course */
    $('body').on('click', '.create-new-container', function (e) {
        var link = "/course/create";
        $.get(link, function(response){
            $('#modals').html(response.modal);
            $('#panel').html(response.content);
            history.pushState(null, null, link);
            document.title = response.title;
            $('.selectpicker').selectpicker({
                size: 7
            });
            //$.getScript( "../lib/selectpicker/bootstrap-select.min.js" )
            //$.getScript( "../js/create-courses.js" )
        });
    });


    // $("input[name=free]").click( function(){
    //     if( $(this).is(':checked') ) alert("checked");
    // });
    $("input[name=difficult_checkbox]").click( function(){
        var el = $("#difficulty");
        if (this.checked) {
            el.attr('disabled', 'true');
        } else {
            el.removeAttr('disabled');
        }
    });

    $("select[name=type_name]").change(function(){
        var el = $("#offline");
        var xhr = $("#xhr_course");
        if($(this).find("option:selected").data('id') == 0)
        {

            el.css('visibility', 'hidden');
            el.slideUp();

            xhr.css('visibility', 'visible');
            xhr.slideToggle();
        }
        else{
            xhr.removeAttr('visibility');
            xhr.css('visibility', 'visible');
            xhr.slideUp();

            el.removeAttr('visibility');
            el.css('visibility', 'visible');
            el.slideToggle();
        }
    });


    $('body').on('submit', '#create-form', function (e) {
        e.preventDefault();

        var type = $("select[name='type_name'] option:selected").data('id');
        var discription = $("#discription").val();
        var full_description = $("#full_description").val();
        var material_link = $("#xhr_link").val();
        var link = $(this).attr('action');
        var course_length_hours = $("#course_length_hours").val();
        var course_length_hours = $("#course_length_hours").val();
        var max_number_students = $("#max_number_students").val();
        //var start_date = $("#datetimepicker1").data("DateTimePicker").date();
        var start_date = $("#datetimepicker1").datetimepicker({datetimeFormat: 'DD/MM/YYYY - HH:mm'}).val();
        alert(start_date);

        if(discription == "")
            discription = undefined;
        if(full_description == "")
            full_description = undefined;
        if(material_link == "")
            material_link = undefined;

        if (type == 0){
            /* validate form */
            if($('input[name=xhr_course]').is(':checked') && $('#xhr_link').val().length == 0){
                $('#create-form .help-block-xhr').text('Укажите ссылку на материалы курса');
                $('#create-form .help-block-xhr').removeClass('hidden');
                return false;
            }
            if ($('input[name=free]').is(':checked') && $('input[name=price]').val().length > 0) {
                $('#create-form .help-block').text('Введите цену или укажите опцию "Бесплатный курс"');
                $('#create-form .help-block').removeClass('hidden');
                return false;
            }else if ($('input[name=free]').is(':checked') &&  $('input[name=price]').val().length == 0) {

                var name = $("input[name='course_name']").val();
                var price = 0;
                var _token = $("input[name='_token']").val();


            }else  if ($('input[name=free]').prop('checked', false) &&  $('input[name=price]').val().length > 0) {

                var name = $("input[name='course_name']").val();
                var price = $("input[name='price']").val();
                var _token = $("input[name='_token']").val();

                $.post(link,
                    {
                        _token:_token,
                        name:name,
                        price:price,
                        type:type,
                        discription:discription,
                        full_description:full_description,
                        material_link:material_link
                    }, function (response) {
                        $('#panel').html(response.content);
                        history.pushState(null, null, link);
                        //window.location.href = '/';
                        //window.location.reload;
                    });

            }else if ($('input[name=free]').prop('checked', false) &&  $('input[name=price]').val().length == 0) {
                $('#create-form .help-block').text('Введите цену или укажите опцию "Бесплатный курс"');
                $('#create-form .help-block').removeClass('hidden');
                return false;
                /* end validate form */
        }

        }
        else{

        }


    });
    /* end redner create course*/

    /* render edit course */
    $('body').on('click', '.go_course [name="go_course"]', function (e) {
        e.preventDefault();
        var link = $(this).attr('action');
        $.get(link, function(response){
            $('#modals').html(response.modal);
            $('#panel').html(response.content);
            history.pushState(null, null, link);
            document.title = response.title;
            $('[data-toggle="popover"]').popover({
                container: 'body'
            });
            var d = new Date();
            $('.course-avatar').attr('src',$('.course-avatar').attr('src')+'?'+d.getTime());
            upload_course_avatar();
            $('.lesson-content').addClass('hide_block');
        });
    });

    $('body').on('click', '.goto_edit_course', function (e) {
        e.preventDefault();
        var link = $(this).attr('action');
        var d = new Date();
        $.get(link, function(response){
            $('#modals').html(response.modal);
            $('#panel').html(response.content);
            history.pushState(null, null, link);
            document.title = response.title;
            $('[data-toggle="popover"]').popover({
                container: 'body'
            });
            var d = new Date();
            $('.course-avatar').attr('src',$('.course-avatar').attr('src')+'?'+d.getTime());
            upload_course_avatar();
            $('.lesson-content').addClass('hide_block');
        });
    });

    $('body').on('click', '.editor-container a', function (e) {
        e.preventDefault();
        var link = $(this).attr('href');
        $.get(link, function(response){
            $('#modals').html(response.modal);
            $('#panel').html(response.content);
            history.pushState(null, null, link);
            document.title = response.title;
            $('[data-toggle="popover"]').popover({
                container: 'body'
            });
            var d = new Date();
            $('.course-avatar').attr('src',$('.course-avatar').attr('src')+'?'+d.getTime());
            upload_course_avatar();
            $('.lesson-content').addClass('hide_block');
        });
    });
    /* end redner  edit course*/

    /* render config course */
    $('body').on('click', '.edit-container a', function (e) {
        e.preventDefault();
        $('.popover').remove();
        var link = $(this).attr('href');
        $.get(link, function(response){
           $('#panel').html(response.content);
            history.pushState(null, null, link);
            document.title = response.title;
            $('.selectpicker').selectpicker({
                size: 7
            });
            //$.getScript( "/../lib/selectpicker/bootstrap-select.min.js" )
            $('[data-toggle="popover"]').popover({
                container: 'body'
            });

        });
    });

    $('body').on('click', '.goto_config_course', function (e) {
        e.preventDefault();
        $('.popover').remove();
        var link = $(this).attr('action');
        $.get(link, function(response){
           $('#panel').html(response.content);;
            history.pushState(null, null, link);
            document.title = response.title;
            $('.selectpicker').selectpicker({
                size: 7
            });
            //$.getScript( "/../lib/selectpicker/bootstrap-select.min.js" )
            $('[data-toggle="popover"]').popover({
                container: 'body'
            });

        });
    });
    /* config validate form */
    $('body').on('submit', '#config-course-form', function (e) {
        e.preventDefault();
        $('.popover').remove();
        var link = $(this).attr('action');

        var category_id = $("select[name='category_name'] option:selected").data('id');
        var vector_id = $("select[name='vector_name'] option:selected").data('id');
        var difficult_id = $("select[name='difficult']")[0].selectedIndex;
        var discription = $("#discription").val();
        var full_description = $("#full_description").val();
        if (category_id == 0)
            category_id = undefined
        if (vector_id == 0)
            category_id = undefined;
        if (difficult_id == 0)
            difficult_id = undefined;
        if (discription == "")
            discription = undefined;
        if (full_description == "")
            full_description = undefined;
        if ($('input[name=free]').is(':checked') && $('input[name=price]').val().length > 0) {
            $('#create-form .help-block').text('Введите цену или укажите опцию "Бесплатный курс"');
            $('#create-form .help-block').removeClass('hidden');
            return false;
        } else if ($('input[name=free]').is(':checked') && $('input[name=price]').val().length == 0) {

            var name = $("input[name='course_name']").val();
            var price = 0;
            var _token = $("input[name='_token']").val();

            $.post(link, {
                _token: _token,
                name: name,
                price: price,
                category: category_id,
                vector: vector_id,
                difficult: difficult_id,
                discription: discription,
                full_description: full_description
            }, function (response) {
               $('#panel').html(response.content);
                history.pushState(null, null, link);
            });

        } else if ($('input[name=free]').prop('checked', false) && $('input[name=price]').val().length > 0) {

            var name = $("input[name='course_name']").val();
            var price = $("input[name='price']").val();
            var _token = $("input[name='_token']").val();

            $.post(link, {
                _token: _token,
                name: name,
                price: price,
                category: category_id,
                vector: vector_id,
                difficult: difficult_id,
                discription: discription,
                full_description: full_description
            }, function (response) {
               $('#panel').html(response.content);;
                history.pushState(null, null, link);
            });

        } else if ($('input[name=free]').prop('checked', false) && $('input[name=price]').val().length == 0) {
            $('#create-form .help-block').text('Введите цену или укажите опцию "Бесплатный курс"');
            $('#create-form .help-block').removeClass('hidden');
            return false;
        }
    });
    /* end validate form */

    /* end config course*/

    /* render card course */
    $('body').on('click', '.go_course [name="go_card"]', function (e) {
        e.preventDefault();
        var link = $(this).attr('action');
        $.get(link, function(response){
           $('#panel').html(response.content);;
            history.pushState(null, null, link);
            document.title = response.title;
            $('[data-toggle="popover"]').popover({
                container: 'body'
            });
            var d = new Date();
            $('.course-avatar').attr('src',$('.course-avatar').attr('src')+'?'+d.getTime());
            upload_course_avatar();
            $('.lesson-content').addClass('hide_block');
        });
    });

    $('body').on('click', '.to_course .to_card', function (e) {
        e.preventDefault();
        var link = $(this).attr('href');
        $.get(link, function(response){
           $('#panel').html(response.content);;
            history.pushState(null, null, link);
            document.title = response.title;
            $('[data-toggle="popover"]').popover({
                container: 'body'
            });
            var d = new Date();
            $('.course-avatar').attr('src',$('.course-avatar').attr('src')+'?'+d.getTime());
            upload_course_avatar();
            $('.lesson-content').addClass('hide_block');
        });
    });
    /*end rednder card course */

    /*render go to source from catalog*/

    $('body').on('click', '.source a', function (e) {
        e.preventDefault();
        var link = $(this).attr('href');
        $.get(link, function(response){
            $('#panel').html(response.content);;
            history.pushState(null, null, link);
            document.title = response.title;
            $('[data-toggle="popover"]').popover({
                container: 'body'
            });
            var d = new Date();
            $('.course-avatar').attr('src',$('.course-avatar').attr('src')+'?'+d.getTime());
            upload_course_avatar();
            $('.lesson-content').addClass('hide_block');
        });
    });

    /* end render to home from catalog*/

    /* render buy course*/
    $('body').on('click', '.buy-course a', function (e) {
        e.preventDefault();
        var link = $(this).attr('href');
        var course_id = $(this).data('id');
        $.get(link, function(response){
            $('#panel').html(response.content);
            history.pushState(null, null, '/course/id'+course_id+'/materials');
            document.title = response.title;
        });
    });
    /*end rednder buy course */

    /* render material course */
    $('body').on('click', '.go_material', function (e) {
        e.preventDefault();
        var link = $(this).attr('action');
        $.get(link, function(response){
            $('#panel').html(response.content);;
            history.pushState(null, null, link);
            document.title = response.title;
            MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
        });
    });

    $('body').on('click', '.goto_material', function (e) {
        e.preventDefault();
        var link = $(this).attr('action');
        $.get(link, function(response){
            $('#panel').html(response.content);;
            history.pushState(null, null, link);
            document.title = response.title;
            MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
        });
    });

    /* end render material course */

    /* render people page */
    $('body').on('click', '#people a', function (e) {
        e.preventDefault();
        var link = $(this).attr('href');
        $.get(link, function(response){
           $('#panel').html(response.content);;
            history.pushState(null, null, link);
            document.title = "Контакты | Clever-e";
            //$.getScript( "/js/inside/people.js" );
            scroll_search();
            live_search();
        });
    });
    /* end render people page */


    // /* render sell course */
    // $('body').on('click', '.go_sell', function (e) {
    //     e.preventDefault();
    //     var link = $(this).attr('action');
    //     $.get(link, function(response){
    //         $('#panel').html(response.content);
    //         history.pushState(null, null, link);
    //         document.title = response.title;
    //         typeahead_page();
    //     });
    // });
    // /* end render sell course */

    /* render messeges  */
    $('body').on('click', '#messages a', function (e) {
        e.preventDefault();
        var link = $(this).attr('href');
        $.get(link, function(response){
           $('#panel').html(response.content);;
            history.pushState(null, null, link);
            document.title = "Чат | Clever-e";
            // $.getScript( "/js/inside/messenger.js" );
        });
    });
    /* end redner messeges*/

    /* render create book */
    $('body').on('click', '.create-book', function (e) {
        e.preventDefault();
        $('#books_modal').modal('hide');
        $('.modal-backdrop').hide();
        var link = $(this).attr('action');
        var course_id = $(this).data('course_id');
        $.get(link, function(response){
            $('#panel').html(response.content);
            $('#modals').html(response.modal);
            history.pushState(null, null, '/book/edit/cid_'+course_id+'/id'+response.id);
            document.title = response.title;
        });
    });
    /* end render create book */

    /* render shop  */
    $('body').on('click', '#shop a', function (e) {
        e.preventDefault();
        var link = $(this).attr('href');
        $.get(link, function(response){
            $('#panel').html(response.content);
            $('#modals').html(response.modal);
            history.pushState(null, null, link);
            document.title = response.title;
            // $.getScript( "/js/inside/messenger.js" );
        });
    });
    /* end render shop */

    /* render book  */
    $('body').on('click', '.to-book-editor', function (e) {
        e.preventDefault();
        var link = $(this).attr('action');
        $('.modal-backdrop').hide();
        $.get(link, function(response){
            $('#panel').html(response.content);
            $('#modals').html(response.modal);
            history.pushState(null, null, link);
            document.title = response.title;
            // $.getScript( "/js/inside/messenger.js" );
        });
    });
    /* end render book */

    /* render testeditor  */
    $('body').on('click', '.to-test-editor', function (e) {
        e.preventDefault();
        var link = $(this).attr('action');
        $('.modal-backdrop').hide();
        $.get(link, function(response){
            $('#panel').html(response.content);
            $('#modals').html(response.modal);
            history.pushState(null, null, link);
            document.title = response.title;
            // $.getScript( "/js/inside/messenger.js" );
        });
    });
    /* end render testeditor */

    /* render create test */
    $('body').on('click', '.create-test', function (e) {
        e.preventDefault();
        $('#tests_modal').modal('hide');
        $('.modal-backdrop').hide();
        var link = $(this).attr('action');
        var course_id = $(this).data('course_id');
        $.get(link, function(response){
            $('#panel').html(response.content);
            $('#modals').html(response.modal);
            history.pushState(null, null, '/quiz/edit/id'+response.id);
            document.title = response.title;
        });
    });
    /* end render create test */

    /* render start test */
    $('body').on('click', '.start-quiz', function (e) {
        e.preventDefault();
        var material_id = $(this).data('mid');
        var _token = $("input[name='_token']").val();
        $.post('/quiz/refresh', {
            _token: _token,
            material_id: material_id,
            type: 'POST'
        },function(response){

        });
        var link = $(this).attr('action');
        var course_id = $(this).data('course_id');
        $.get(link, function(response){
            $('#panel').html(response.content);
            $('#modals').html(response.modal);
            history.pushState(null, null, link);
            document.title = response.title;
        });
    });
    /* end start test */

    /* render result test */
    $('body').on('click', '.result-quiz', function (e) {
        e.preventDefault();
        var link = $(this).attr('action');
        var course_id = $(this).data('course_id');
        $.get(link, function(response){
            $('#panel').html(response.content);
            $('#modals').html(response.modal);
            history.pushState(null, null, link);
            document.title = response.title;
        });
    });
    /* end result test */

    /* render create task */
    $('body').on('click', '.create-task', function (e) {
        e.preventDefault();
        $('#tests_modal').modal('hide');
        $('.modal-backdrop').hide();
        var link = $(this).attr('action');
        var course_id = $(this).data('course_id');
        $.get(link, function(response){
            $('#panel').html(response.content);
            $('#modals').html(response.modal);
            history.pushState(null, null, '/task/edit/cid_'+course_id+'/id'+response.id);
            document.title = response.title;
        });
    });
    /* end render create task */

    /* render complete task */
    $('body').on('click', '.complete-task', function (e) {
        e.preventDefault();
        var elem = $(this);
        var link = $(this).attr('action');
        var material_id = $(this).data('mid');
        var task_text = CKEDITOR.instances['task_text_'+material_id+''].getData();
        var _token = $("input[name='_token']").val();
        $.post(link, {
            _token: _token,
            task_text: task_text,
            material_id: material_id,
            type: 'POST'
        },function(response){
            $('#task_div_' + material_id).html(CKEDITOR.instances['task_text_'+material_id+''].getData());
            $('#task_div_' + material_id).show();
            CKEDITOR.instances['task_text_'+material_id+''].destroy(true);
            $('#task_text_' + material_id).hide();
            elem.hide();

        });
    });
    /* end render complete task */

    /* render result user */
    $('body').on('click', '.go_my_results', function (e) {
        e.preventDefault();
        var link = $(this).attr('action');
        $.get(link, function(response){
            $('#panel').html(response.content);
            $('#modals').html(response.modal);
            history.pushState(null, null, link);
            document.title = response.title;
        });
    });
    /* end result user */

    /* render more result student */
    $('body').on('click', '.my_result_more', function (e) {
        e.preventDefault();
        var link = $(this).attr('action');
        $.get(link, function(response){
            $('#panel').html(response.content);
            $('#modals').html(response.modal);
            history.pushState(null, null, link);
            document.title = response.title;
        });
    });
    /* end more result student */

    /* render result student */
    $('body').on('click', '.go_student_results', function (e) {
        e.preventDefault();
        var link = $(this).attr('action');
        var callback = window.location.pathname;
        $.get(link, function(response){
            $('#callbackurl').data('url',callback);
            $('#panel').html(response.content);
            $('#modals').html(response.modal);
            history.pushState(null, null, link);
            document.title = response.title;
            $('.modal-backdrop').hide();
        });
    });
    /* end result student */
/*end ajax to render page*/

/*cosmetick on page*/
    /* change name on collapse user info */
    $('body').on('click', '.collapse-user-info', function (e) {
        if(!$('#full-info').hasClass('in')){
            $('.collapse-user-info').text('Свернуть');
        }else{
            $('.collapse-user-info').text('Развернуть');
        }
    });
    /* end change name on collapse user info */

    /* change tab color on user courses teacher click */
    $('body').on('click', '.teacher', function (e) {
       $(".user-course .tab-content").css('border', '3px solid #e57335');
       $(".student").css('opacity', '0.5');
       $(".teacher").css('opacity', '1');
    });
    /* end change tab color on user courses teacher click*/

    /* change tab color on user courses student click */
    $('body').on('click', '.student', function (e) {
        $(".user-course .tab-content").css('border', '3px solid #58baad');
        $(".teacher").css('opacity', '0.5');
        $(".student").css('opacity', '1');
    });
    /* end change tab color on user courses student click*/


    /* upload avatar*/
    $('body').on('click', '.user-avatar-upload', function (e) {
        e.preventDefault();
        $("#upload-avatar:hidden").trigger('click');
    });

    function upload_avatar() {
        $("input[name='avatar']").change(function(e) {
            //this.form.submit();
            e.preventDefault();

            var link = $(this).parent().attr('action');
            var _token = $("input[name='_token']").val();
            var formatdata = new FormData($('#user-avatar-upload-form')[0]);
            var d = new Date();
            $.ajax({
                url: link,
                data: formatdata,
                processData: false,
                contentType: false,
                type: 'POST',
                success: function(response){
                    $('.user-avatar').attr('src','/storage/avatars/'+response+'?'+d.getTime());
                    $('.user-logo').attr('src','/storage/avatars/icon/'+response+'?'+d.getTime());
                }
            });

        });
    }
    /* end upload avatar*/

    /* upload course avatar*/
    $('body').on('click', '.course-avatar-upload', function (e) {
        e.preventDefault();
        $("#upload-avatar:hidden").trigger('click');
    });

    function upload_course_avatar() {
        $("input[name='avatar']").change(function(e) {
            //this.form.submit();
            e.preventDefault();

            var link = $(this).parent().attr('action');
            var _token = $("input[name='_token']").val();
            var formatdata = new FormData($('#course-avatar-upload-form')[0]);

            var d = new Date();
            $.ajax({
                url: link,
                data: formatdata,
                processData: false,
                contentType: false,
                type: 'POST',
                success: function(response){
                    $('.course-avatar').attr('src','/storage/course_avatars/'+response+'?'+d.getTime());
                }
            });

        });
    }
    /* end upload course avatar*/

    /* render alert xhr */
    $(document).ready(function () {
        if ($('body .has-create-api').length > 0) {
            $(".xhr_course").click(function (e) {
                if ($('.xhr_course').is(':checked')) {
                    $('#xhrModal').modal();
                    $('#xhr_link').parent().parent().parent().removeClass('hidden');

                }
                else
                    $('#xhr_link').parent().parent().parent().addClass('hidden');
            });
        }
    })
    /* end render alert xhr */

    /*modal to send xhr*/
    $("body").on("click", "#sendXhr", function (e) {
        var elem = $(this);

        var user_id = elem.data('user_id');
        var user_name = elem.data('user_name');
        var course_id = elem.data('course_id');
        var course_name = elem.data('course_name');
        var link = elem.data('link');
        $("#xhrConfirmation #xhrOk").attr("action", link);
        $("#xhrConfirmation #user").data("id", user_id);
        $("#xhrConfirmation #user").val(user_name);
        $("#xhrConfirmation #course").data("id", course_id);
        $("#xhrConfirmation #course").val(course_name);
        $("#xhrConfirmation .error").addClass('hidden');
        // e.preventDefault();
    });

    $("body").on("click", "#xhrOk", function (e) {
        e.preventDefault();
        var link = $(this).attr('action');
        var _token = $("input[name='_token']").val();
        var user_id = $("#xhrConfirmation #user").data("id");
        var course_id = $("#xhrConfirmation #course").data("id");
        var login = $("#xhrConfirmation #login").val();
        var password = $("#xhrConfirmation #password").val();
        var comment = $("#xhrConfirmation #comment").val();
        var elem = $(".sendXhrClass[data-user_id='" + user_id + "']");
        if((login.length > 0 && password.length >0) || comment.length >0){
            $.post(link, {
                _token: _token,
                user_id: user_id,
                course_id: course_id,
                login: login,
                password: password,
                comment: comment,
                type: 'POST'
            },function(response){
                if (response != 0)
                    alert("Данные успешно отправлены. Они будут доступны для студента после проверки модератором.");
                $('#xhrConfirmation').modal('toggle');
                console.log(elem);
                elem.removeClass('btn-danger');
                elem.removeClass('btn-success');
                elem.addClass('btn-default');
                elem.addClass('disabled');
                elem.data('link', '');
                elem.text('Ожидает проверки');


            });
        }else{
            $("#xhrConfirmation .error").removeClass('hidden');
        }
    });
    /*end modal to send xhr*/
/*end cosmetick on page*/

/* back buton browser fix */
    $(window).bind('popstate', function() {
        $.ajax({url:window.location,success: function(response){
            $('#panel').html(response.content);
            $('#modals').html(response.modal);
            document.title = response.title;
            if(window.location.pathname.indexOf("/catalog") >= 0){
                $('.selectpicker').selectpicker({
                    size: 7
                });
               // $.getScript( "/lib/selectpicker/bootstrap-select.min.js" );
            }
            if(window.location.pathname.indexOf("/course/create") >= 0 ){
                $('.selectpicker').selectpicker({
                    size: 7
                });
                //$.getScript( "../lib/selectpicker/bootstrap-select.min.js" );
                $.getScript( "../js/create-courses.js" );
            }
            if(window.location.pathname.indexOf("/course/config") >= 0 ){
                $('.selectpicker').selectpicker({
                    size: 7
                });
                //$.getScript( "/../lib/selectpicker/bootstrap-select.min.js" );
            }
            if(window.location.pathname.indexOf("/people") >= 0 ){
                // $.getScript( "/js/inside/people.js" );
                scroll_search();
                live_search();
            }

            if(window.location.pathname.indexOf("/messages") >= 0 ){
                // typeahead_page();
                // $.getScript( "/js/inside/messenger.js" );
            }
            
            // var regex = new RegExp("\\Scatalog\/\\S*\/c_\\d*\/v_\\d*\/from_\\d*\/\\S*\/order_\\d*\/page_\\d*");
            // if(regex.test(window.location.pathname) ){
            //     alert(1);
            //     $.get( window.location.pathname, function(  ) {
            //
            //     }).done(function(data){
            //
            //     })
            // }
        }});
    });
/* end back buton browser fix */

/*preview course */
    $("body").on("click", ".lesson-container-preview h3", function (e) {
        var elem = $(this);
        var lesson_id = elem.data("lesson_id");
        var book_id = $('.lesson_menu-container').data('book_id');
        var _token = $("input[name='_token']").val();
        $.post("/book/preview/id" + book_id + "/getlesson", {
            _token: _token,
            lesson_id: lesson_id
        }, function (response) {
            $('.lesson-content-preview').removeClass('hide_block');
            $('.lesson-content-preview .input-name-preview').empty();
            $('.textarea').empty();
            $('.lesson-content-preview .input-name-preview').append(response.name);
            $('.lesson-content-preview .input-name-preview').data('lesson_id', lesson_id);

            $('.textarea').append(response.text);
            MathJax.Hub.Queue(["Typeset",MathJax.Hub]);

            $('.textarea').scrollTop(0);
        });
        e.preventDefault();
    });

    $("body").on("click", ".textarea a", function (e) {
        var link =$(this).attr('href');
        if(link.indexOf("?lid=") >=0 && link.indexOf("&uid=")>=0 ){
            e.preventDefault();

            var lesson_id =  link.substring(link.lastIndexOf("?")+5,link.lastIndexOf("&"));
            var link_id =  link.substring(link.lastIndexOf("&")+5,link.length);
            var book_id = $('.lesson_menu-container').data('book_id');
            var _token = $("input[name='_token']").val();
            $.post("/book/preview/id" + book_id + "/getlesson", {
                _token: _token,
                lesson_id: lesson_id
            }, function (response) {
                $('.lesson-content-preview').removeClass('hide_block');
                $('.lesson-content-preview .input-name-preview').empty();
                $('.textarea').empty();
                $('.lesson-content-preview .input-name-preview').append(response.name);
                $('.lesson-content-preview .input-name-preview').data('lesson_id', lesson_id);

                $('.textarea').append(response.text);
                MathJax.Hub.Queue(["Typeset",MathJax.Hub]);

                var scroll =$("a[name='#lesson_id_" + lesson_id + "_uid_" + link_id + "']").offset().top;
                $('.textarea').scrollTop(scroll);
            });

        }
    });
/*end course preview */

/* material link */
    $("body").on("click", ".panel-group a", function (e) {
        var link =$(this).attr('href');
        if(link.indexOf("?lid=") >=0 && link.indexOf("&uid=")>=0 ){
            e.preventDefault();
            var lesson_id =  link.substring(link.lastIndexOf("?")+5,link.lastIndexOf("&"));
            var link_id =  link.substring(link.lastIndexOf("&")+5,link.length);
            var find_link = $('.panel-group').find($("a[name='#lesson_id_"+lesson_id+"_uid_"+link_id+"']"));
            var finder = find_link.parent().parent().parent().parent().parent();

            var current = $(this).parent().parent().parent().parent().parent();

            current.collapse('toggle') ;
            finder.collapse('toggle') ;

            var scroll =$("a[name='#lesson_id_" + lesson_id + "_uid_" + link_id + "']").offset().top;
            console.log(scroll);
            $(window).scrollTop(scroll);
        }
    });

/* end material link */

// user-monitoring

