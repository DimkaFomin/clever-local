$(document).ready(function() {
    if ($('body .has-people-api').length > 0) {
        scroll_search();
        live_search();
    }

});


function scroll_search() {
    $(window).scroll(function () {
        var page = $('.endless-pagination').data('next-page');

        if (page !== null) {
            clearTimeout($.data(this, "scrollCheck"));

            $.data(this, "scrollCheck", setTimeout(function () {
                var scroll_position_user_load = $(window).height() +
                    $(window).scrollTop() + 100;

                if (scroll_position_user_load >= $(document).height()) {
                    var squery = $('.user-search-input[type="search"]').val();

                    if (squery.length > 0) {
                        $.get(page, function (data) {
                            $('.users').append(data.users);
                            $('.endless-pagination').data('next-page', data.next_page);
                        });
                    } else {
                        $.get(page, function (data) {
                            $('.users').append(data.users);
                            $('.endless-pagination').data('next-page', data.next_page);
                        });
                    }

                }
            }, 100))
        }
    });
};
function live_search() {
    $('.user-search-input[type="search"]').on('keyup', function (e) {
        var squery = $(this).val();
        if (squery == '') {
            squery = "getmyallpeople";
        }

            $.get('/people_' + squery, function (data) {
                $('.users').empty();
                $('.users').append(data.users);
                $('.endless-pagination').data('next-page', data.next_page);
            });


    });
}



// $('#search_wrap .user-search-btn').on('click', function() {
//     var squery = $('#search_wrap .user-search-input').val();
//     $.ajax({
//         method: 'POST',
//         url: url,
//         data: {_token: token, query: squery}
//     }).done(function(data) {
//         $.each(data, function(key, value) {
//             var avatar = value.photo_id > 0?
//             '/uploads/avatars/'+value.id+'.'+value.photo.type
//             : '/img/no_avatar.png';
//             $('.users').empty().append(
//                 '<div class="profile col-lg-3 col-sm-6 text-center">'+
//                 '<a href="/id'+value.id+'">'+
//                 '<img class="img-circle img-responsive img-center" src="'+avatar+'">'+
//                 '</a><h5>'+value.first+' '+value.second+'</h5>'+
//                 '</div>'
//             );
//         });
//     });
// });
