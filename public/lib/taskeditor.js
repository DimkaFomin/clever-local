$(document).ready(function () {
    $('body').on('click', '.save_task', function () {
        for (name in CKEDITOR.instances) {
            CKEDITOR.instances[name].getData();
            var elem = parent.CKEDITOR.instances[name].id;
            $('.' + elem).prev().html(CKEDITOR.instances[name].getData());
            $('.' + elem).prev().prev().html(CKEDITOR.instances[name].getData());
        }
        var task = $(".task").serializeArray();
        console.log(task['task_name']);
        var link = $(this).data('link');
        var _token = $("input[name='_token']").val();
        $.post(link, {
            _token: _token,
            task: task
        }, function (response) {
            alert('Задание сохранено');
        });

    });
});
