CKEDITOR.plugins.add('savecourse', {
    init : function(editor) {

        editor.addCommand( 'savecourse', {

            exec: function( editor ) {
                if($('.cke_button__maximize').hasClass('cke_button_on')){
                    //$('.cke_button__maximize').click();
                }
                var book_id = $('.add-lesson-container').data('book_id');
                var lesson_id = $(".lesson-content .input-name").data("lesson_id");
                var title = $(".lesson-content .input-name").val() != "" ? $(".lesson-content .input-name").val() : "Без имени";
                var text = CKEDITOR.instances.mytextarea.getData() != "" ? CKEDITOR.instances.mytextarea.getData() : "Нет сожержимого";
                var save_id = $("#page-content-wrapper").data("save_id");
                var _token = $("input[name='_token']").val();

                $.post("/book/edit/id"+book_id+"/setlesson", {
                    _token: _token,
                    lesson_id: lesson_id,
                    name: title,
                    text: text
                }, function (response) {
                    $('.accordion').find("[data-lesson_id='"+lesson_id+"']").text(title);
                    $('.accordion').find("[data-lesson_id='"+lesson_id+"']").data('title',title);
                    $('.save-trigger').data("save-trigger", 0);
                    CKEDITOR.instances.mytextarea.getCommand('savecourse').setState(CKEDITOR.TRISTATE_DISABLED);
                });


               // $(".lesson-block .lesson-save").click();
            },
            startDisabled: true
        });

        editor.ui.addButton('SaveCourse', {
            label : 'Сохранить урок',
            command : 'savecourse',
            icon : this.path + 'icons/save.png',
            toolbar: 'others1,0'
        });

        CKEDITOR.dialog.add('slide', this.path + 'dialogs/savecourse.js');
    }
});