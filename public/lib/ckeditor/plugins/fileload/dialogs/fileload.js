CKEDITOR.dialog.add( 'fileload', function( editor ) {
    return {
        title:          'Вставить файл',
        resizable:      CKEDITOR.DIALOG_RESIZE_NONE,
        minWidth:       500,
        minHeight:      400,
        className: "file_over",
        onOk : function() {

            $('.file_here input[type=checkbox]').each(function () {
                if(this.checked){
                    var ext = this.nextSibling.innerHTML;
                    ext = ext.split('.').pop().toLowerCase();
                    if(ext == 'jpg' || ext == 'png' || ext == 'jpeg'|| ext == 'gif'){
                        editor.insertHtml('<p><img src ="' + this.value + '" style="width:600px;"></p></br>');

                    }else {
                        editor.insertHtml('<p><a href ="' + this.value + '" target="_blank">' + this.nextSibling.innerHTML + '</a></p></br>');
                    }
                    $(this).prop('checked', false);
                }
            });


        },
        contents: [
            {
                id : 'tab1',

                elements: [
                    {
                        type:           'button',
                        label:          'Добавить  файл',
                        id:             'get_file',
                        onClick: function() {
                            $("#upload-file:hidden").trigger('click');
                        }
                    },
                    {
                        type : 'html',
                        html :  '<form enctype="multipart/form-data" id="user-file-upload-form" action="/upload/files" method="POST">'+
                                '<input type="file" name="fileload" class ="hidden" id="upload-file"><input type="hidden" id="token2" name="_token" value=""></form>'
                    },
                    {
                        type : 'html',
                        html : '<div class="file_here" id="file_here"></div>'
                    }
                ]
            }
        ]
    };
} );

CKEDITOR.on('dialogDefinition', function (ev) {
    var dialogName = ev.data.name;
    var dialogDefinition = ev.data.definition;
    var dialog = ev.data.definition.dialog;

    dialog.on('show', function () {
    /*add file in editor */
        $("#token2").val($("input[name='_token']").val());

        $("input[name='fileload']").change(function(e) {
            //this.form.submit();
            e.preventDefault();
            var link = '/files/upload';
            var formatdata = new FormData($('#user-file-upload-form')[0]);
            var d = new Date();
            $.ajax({
                url: link,
                data: formatdata,
                processData: false,
                contentType: false,
                type: 'POST',
                success: function(response){
                    response.forEach(function(file){
                    if (file != 0 || file != 1) {
                        var parent = new CKEDITOR.dom.element(document.getElementById('new_file'));

                        var id = parseInt($('.file_check:last').attr('id').match(/\d+/), 10);
                        id++;
                        var check = new CKEDITOR.dom.element("INPUT");
                        check.setAttribute("type", "checkbox");
                        check.addClass('file_check');
                        check.addClass('nolink');
                        check.setValue('/storage/user_' + user_id + '/' + file.type + '/' + file.hash + '.' + file.type);
                        check.setAttribute('name', 'file' + id);
                        check.setAttribute('id', 'file' + id);

                        var file_body = new CKEDITOR.dom.element('div');
                        file_body.addClass('file_body');

                        var file_name = new CKEDITOR.dom.element('div');
                        file_name.addClass('file_name');
                        file_name.setText(file.name);

                        var file_label = new CKEDITOR.dom.element('label');
                        file_label.setAttribute('for', 'file' + id)

                        file_body.append(check);
                        file_body.append(file_name);
                        file_label.append(file_body);
                        parent.append(file_label);

                        parent.removeAttribute("id");
                        parent = $('.file_here');

                        parent.prepend("<div id='new_file'></div>");
                    }

                });
                }
            });
            e.preventDefault();

        });
    /*end add file in editor */

    if (dialogName == 'fileload') {
        var main = dialogDefinition.getContents('tab1');

        var data = null;
        var _token = $("input[name='_token']").val();
        var user_id = $('#profile .home').data('user_id');
        if (user_id == null){
            user_id = $('.lesson-content').data('user_id');
        }

        $.post('/files/getall', {_token:_token},function(response){
            document.getElementById('file_here').innerHTML="";
            var parent = new CKEDITOR.dom.element(document.getElementById('file_here'));
            var new_file = new CKEDITOR.dom.element('div');
            new_file.setAttribute('id', 'new_file');
            parent.append(new_file);
            var id = 0;

            response.forEach(function(file){
                var check = new CKEDITOR.dom.element("INPUT");
                    check.setAttribute("type", "checkbox");
                    check.addClass('file_check');
                    check.setValue('/storage/user_'+user_id+'/'+file.type+'/'+file.hash+'.'+file.type);
                    check.setAttribute('name', 'file' + id);
                    check.setAttribute('id', 'file' + id);

                    var file_body = new CKEDITOR.dom.element('div');
                    file_body.addClass('file_body');

                    var file_name = new CKEDITOR.dom.element('div');
                    file_name.addClass('file_name');
                    file_name.setText(file.name);

                    var file_label = new CKEDITOR.dom.element('label');
                    file_label.setAttribute('for', 'file' + id);

                    file_body.append(check);
                    file_body.append(file_name);
                    file_label.append(file_body);
                    parent.append(file_label);
                    id++;
            });
        });
    }
});

    dialog.on('hide', function () {
        $("input[name='fileload']").unbind('change');
    });
});
