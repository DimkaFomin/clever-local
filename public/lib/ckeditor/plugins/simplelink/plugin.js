CKEDITOR.plugins.add( 'simplelink', {
    icons: 'simplelink',
    init: function( editor ) {
        editor.addCommand( 'simplelink', new CKEDITOR.dialogCommand( 'simplelinkDialog' ) );
        editor.ui.addButton( 'simplelink', {
            label: 'Добавить ссылку',
            icons: 'simplelink',
            command: 'simplelink',
            toolbar: 'links'
        });

        CKEDITOR.dialog.add( 'simplelinkDialog', this.path + 'dialogs/simplelink.js' );
    }
});