CKEDITOR.dialog.add( 'courselink', function( editor ) {
    return {
        title:          'Вставить ссылку на метриал',
        resizable:      CKEDITOR.DIALOG_RESIZE_NONE,
        minWidth:       800,
        minHeight:      500,
        className: "course_over",
        onOk : function() {
                if (window.getSelection) {
                    var txt = window.getSelection();
                } else if (document.getSelection) {
                    txt = document.getSelection();
                } else if (document.selection) {
                    txt = document.selection.createRange().text;
                }
                var lesson_id = $('#course_name').data("lesson_id");
                var book_id = $('.add-lesson-container').data('book_id');
                var title = $('#course_name').text();
                var _token = $("input[name='_token']").val();

                var selected = CKEDITOR.instances.mytextarea.getSelection().getNative();
                var range = txt.getRangeAt(0);
                var genTag = document.createElement("a");
                var el = document.getElementById("course_source")
                if (el.innerHTML.indexOf("#slide") !== -1) {
                    var startOffset = 0;
                    var preCaretRange = range.cloneRange();
                    preCaretRange.selectNodeContents(el);
                    preCaretRange.setEnd(range.startContainer, range.startOffset);
                    find_slid = preCaretRange.toString();
                    var target = "#slide";
                    var slideCount = 0;
                    var pos = -1;
                    while ((pos = find_slid.indexOf(target, pos + 1)) != -1) {
                        slideCount++;
                    }
                    genTag.name="#lesson_id_"+lesson_id+"_uid_"+txt+"_slide_"+slideCount;
                    editor.insertHtml('<a href ="?lid='+lesson_id+'&uid='+txt+'&sid='+slideCount+'" target="_blank">' + selected + '</a>');
                }else{
                    genTag.name="#lesson_id_"+lesson_id+"_uid_"+txt;
                    editor.insertHtml('<a href ="?lid='+lesson_id+'&uid='+txt+'" target="_blank">' + selected + '</a>');
                }


                var documentFragment = range.extractContents();
                range.insertNode(documentFragment);
                range.insertNode(genTag);
                var content = $('#course_source').html();

                $.post("/book/edit/id"+book_id+"/setlesson", {
                    _token: _token,
                    lesson_id: lesson_id,
                    name: title,
                    text: content
                }, function (parent_id) {
                    CKEDITOR.instances.mytextarea.getCommand('savecourse').setState(CKEDITOR.TRISTATE_OFF);
                });
        },
        contents: [
            {
                id : 'tab1',

                elements: [
                    {
                        type : 'html',
                        html : '<div class="course_over row" id="course_over">' +
                        '<div class="col-md-3 fix_float" id="course_menu"></div>'+
                        '<div class="col-md-9 fix_float" id="course_text">' +
                        '<div class="row">' +
                        '<div class="col-md-12 course_name" id="course_name"></div>' +
                        '<div class="col-md-12 course_source" id="course_source"></div>' +
                        '</div>'+
                        '</div>'+
                        '</div>'
                    }
                ]
            }
        ]
    };
} );

CKEDITOR.on('dialogDefinition', function (ev) {
    var dialogName = ev.data.name;
    var dialogDefinition = ev.data.definition;
    var dialog = ev.data.definition.dialog;
    var selected = CKEDITOR.instances.mytextarea.getSelection().getNative();

    dialog.on('show', function () {
        if (dialogName == 'courselink') {

            var main = dialogDefinition.getContents('tab1');
            var _token = $("input[name='_token']").val();

            var data = null;
            var book_id = $('.add-lesson-container').data('book_id');
            $.post("/book/edit/id"+book_id+"/getalllesson", { _token: _token},function (lessons) {

                document.getElementById('course_menu').innerHTML="";

                var parent = new CKEDITOR.dom.element(document.getElementById('course_menu'));
                parent.addClass('course_menu');
                lessons.forEach(function (lesson) {
                    if (lesson.parent_id == 0) {
                        var lesson_name = new CKEDITOR.dom.element('div');
                        lesson_name.addClass('l_name');
                        lesson_name.addClass('l_click');
                        lesson_name.setAttribute("data-lesson_id",lesson.id);
                        lesson_name.setText(lesson.name);
                        parent.append(lesson_name);
                        lessons.forEach(function (lesson_second) {
                            if (lesson_second.parent_id == lesson.id) {
                                var lesson_child = new CKEDITOR.dom.element('div');
                                lesson_child.addClass('l_child');
                                lesson_child.addClass('l_click');
                                lesson_child.setAttribute("data-lesson_id", lesson_second.id);
                                lesson_child.setText(lesson_second.name);
                                parent.append(lesson_child);
                            }
                        });

                    }
                });

                $('.l_click').click(function() {
                    var lesson_id = $(this).data("lesson_id");
                    $.post("/book/edit/id"+book_id+"/getlesson",
                        {
                            lesson_id:lesson_id,
                            _token: _token
                        },function (lesson) {
                        $('#course_name').text(lesson.name);
                        $('#course_name').data("lesson_id",lesson_id);
                        $('#course_source').html(lesson.text).text();
                    });
                });

                $('.l_click:first').click();

            });
        }

    });

});

