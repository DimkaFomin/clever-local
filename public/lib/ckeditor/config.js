/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */
CKEDITOR.editorConfig = function( config ) {
    config.toolbarGroups = [
        { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
        { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
        { name: 'links', groups: [ 'links' ] },
        { name: 'insert', groups: [ 'insert' ] },
        { name: 'forms', groups: [ 'forms' ] },

        { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
        //'/',
        { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
        { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
        { name: 'styles', groups: [ 'styles' ] },
        { name: 'colors', groups: [ 'colors' ] },
        { name: 'about', groups: [ 'about' ] },
        '/',
        { name: 'others', groups: [ 'others' ] },
        { name: 'others1', groups: [ 'others1' ] },
        { name: 'tools', groups: [ 'tools' ] }

    ];

    config.removeButtons = 'Scayt,Unlink,Anchor,Source,Strike,RemoveFormat,Outdent,Indent,About,Format,Styles';//Link,Unlink

    config.removePlugins    = 'elementspath,resize,about,save,eqneditor,specialchar';

/*
    config.toolbarGroups = [
        { name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
        { name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
        { name: 'links' },
        { name: 'insert' },
        { name: 'forms' },
        { name: 'tools' },
        { name: 'document',	   groups: [ 'mode', 'document', 'doctools' ] },
        { name: 'others' },
        '/',
        { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
        { name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
        { name: 'styles' },
        { name: 'colors' }
    ];
*/

    // Remove some buttons provided by the standard plugins, which are
	// not needed in the Standard(s) toolbar.

	// Set the most common block elements.
	config.format_tags = 'p;h1;h2;h3;pre';
    config.allowedContent = true;

	// Simplify the dialog windows.
	//config.removeDialogTabs = 'image:advanced;link:advanced';

    //config.extraPlugins = 'imageresize';
   // config.extraPlugins = 'image2';
    config.extraPlugins = 'slide,image,justify,fileload,savecourse,autocorrect,pastefromword,symbol,font,mathjax,courselink';
    //config.extraPlugins = 'html5video';
    //config.extraPlugins = 'iframedialog';
    //config.extraPlugins = 'iframe';
    /*config.extraPlugins = 'embed';
    config.extraPlugins = 'embedbase';
    //config.extraPlugins = 'autoembed';
    config.extraPlugins = 'notification';
    config.extraPlugins = 'notificationaggregator';*/
    //config.extraPlugins = 'youtube';//работета с youtube
    config.resize_enabled = false;
    config.disableNativeSpellChecker = false;

    config.mathJaxLib = '//cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0/MathJax.js?config=TeX-AMS_HTML';
    config.mathJaxClass = 'math-tex';
};