
var app = require('express'),
    server = require('http').Server(app),
    io = require('socket.io')(server, {
        origins: 'clever-e.com:*'
        // origins: 'clever:*'
    }),
    request = require('request');

var model = require('../messenger/model.js');

server.listen(8080);
console.log('Server listening at port 8080');

io.attach(server, {
  pingInterval: 10000,
  pingTimeout: 5000,
  cookie: false
});

var peers = {};
var msgcount = {};

io.use((socket, next) => {

    request.get({
        url: 'http://clever-e.com/ws/check-auth',
        // url: 'http://clever/ws/check-auth',
        headers: {cookie: socket.request.headers.cookie},
        json: true
    }, function(error, response, json) {

        console.log(json.first, json.second, socket.id);

        socket.user = json;
        if (peers[json.id] == undefined) peers[json.id] = [];
        peers[json.id].push(socket);

        return json.auth ? next() : next(new Error('Authentication error'));
    });
});



io.sockets.on('connection', function(socket) {

    socket.on('disconnect', function() {
        if (!socket.user) return;
        var sockets = peers[socket.user.id];
        for (var i = 0; i < sockets.length; i++) {
            if (sockets[i].id == socket.id) {
                sockets.splice(i, 1);
            }
        }
    });

    // ---------------[ chat ]-----------------
    socket.on('message', function(data) {
        var initiator = socket.user.id;
        var thread_id = data.thread_id;

        model.getUserThreadAll(thread_id, function(users) {
            var mcount = undefined, // message count
                tcount = undefined; // thread count

            users.forEach(function(user) {
                // counting
                if (user.id != initiator) {
                    if (msgcount[user.id] == undefined) {
                        msgcount[user.id] = {};
                        msgcount[user.id][data.thread_id] = 0;
                    } else if (msgcount[user.id][data.thread_id] == undefined){
                        msgcount[user.id][data.thread_id] = 0;
                    }

                    msgcount[user.id][data.thread_id]++;
                    mcount = msgcount[user.id][data.thread_id];
                    tcount =  Object.keys(msgcount[user.id]).length;
                }

                // send message and/or set counting
                if (user.id in peers) {
                    var sockets = peers[user.id];

                    for (i in sockets) {
                        console.log('send', sockets[i].id, user.id);
                        // [1]: send message to all users of thread
                        io.sockets.sockets[sockets[i].id].emit('message', {
                            thread_id: data.thread_id,
                            message: data.message
                        });
                        // [2]: counting the number of messages
                        if (sockets[i].user.id != initiator) {
                            io.sockets.sockets[sockets[i].id].emit('unread', {
                                thread_id: data.thread_id,
                                mcount: mcount,
                                tcount: tcount
                            });
                        }
                    }
                }
            })
        });
    });

    // socket.on('get markers', function() {
    //     io.sockets.sockets[socket.id].emit('markers', {
    //         markers: msgcount[socket.user.id]
    //     });
    // });

    socket.on('viewed', function(data) {
        if (socket.user.id in msgcount)
            if  (data.thread_id in msgcount[socket.user.id]) {
                delete msgcount[socket.user.id][data.thread_id];
                model.setUserLastRead(data.thread_id, socket.user.id);

                model.getUserThreadAll(data.thread_id, function(users) {
                    users.forEach(function(user) {

                        if (user.id in peers) {
                            var sockets = peers[user.id];
                            for (i in sockets) {
                                io.sockets.sockets[sockets[i].id].emit('read', {
                                    thread_id: data.thread_id,
                                });
                            }
                        }

                    });
                });
            }
    });

});
